﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Public_Const;
using System.Configuration;
using System.Security.Cryptography;
using System.Xml;
using System.IO;
using JSON_Interface;
using ICSharpCode;
using ICSharpCode.SharpZipLib;
using ICSharpCode.SharpZipLib.Core;
using ICSharpCode.SharpZipLib.GZip;
using ICSharpCode.SharpZipLib.Tar;
using ICSharpCode.SharpZipLib.Zip;
using System.Diagnostics;
using CBoxADBcontroll;





namespace CBox_Low_Audio_Data_Config
{


    public partial class FrmMain : Form
    {
        FormCBoxConfig FrmCBoxConfig = new FormCBoxConfig();
        FormInfo frm = new FormInfo();

        
        public FrmMain()
        {
            InitializeComponent();
            this.Text = PublicConst.AppName +" - V: " +PublicConst.AppVersion;
            this.MinimumSize = new System.Drawing.Size(PublicConst.FrmWidthMin, PublicConst.FrmHeightMin);
            Load_configuration();

            //CBox2020
            //setzen Typ CBoxLow (0) oder CBox2020 (1)
            setRBtnType(PublicConst.CBoxType); 
            //setCBoxType(Convert.ToInt32(PublicConst.CBoxType));

        }

            

        private void Load_configuration()
        {
            //xml-file Configuration lesen
            Public_Const.Configuration InstPublic = new Public_Const.Configuration();
            int i = InstPublic.Cload();

            if ((PublicConst.EQ_xml_HashStart == "") | (PublicConst.EQ_xml_HashStart == null))
            { PublicConst.EQ_xml_HashStart = PublicConst.EQ_xml_HashStart_Default; }

            
            if (i == 0) //kein fehler
            {
                setAllTextboxes();
            }
        }

        private void setAllTextboxes()
        {
            tb_ProjectPath.Text = PublicConst.Project_Dir;
            tb_ProjectFile.Text = PublicConst.Project_File;
            //PublicConst.Config_version = "";

            //lua handling
            string destFileName = Path.Combine(tb_ProjectPath.Text, PublicConst.Lua_fileName);
            if (System.IO.File.Exists(destFileName) == false) { lua_copy(); }  // lua ins project kopieren, wenn noch nicht vorhanden
            tbLuaName.Text = Path.GetFileName(destFileName);
            PublicConst.Lua_CRC = file_crc16_check(destFileName);  // lua auf crc prüfen
            tb_lua_crc.Text = PublicConst.Lua_CRC;
            tb_CMIC.Text = PublicConst.NAD_CMIC;
            tb_CLVL.Text = PublicConst.NAD_CLVL;

            if (PublicConst.cb_SourceDirExt_v_checked == "1")
            {
                cb_SourceDirExt.Checked = true;
            }
            else
            {
                cb_SourceDirExt.Checked = false;
            }
            if (PublicConst.cb_SourceDirInt_v_checked == "1")
            {
                cb_SourceDirInt.Checked = true;
            }
            else
            {
                cb_SourceDirInt.Checked = false;
            }


            tb_CarAUNo.Text = PublicConst.Car_type;
            //tb_CarVersion.Text = PublicConst.Car_version; wird intern gebildet
            tb_TuningVersInt.Text = PublicConst.Tuning_version_int;
            tb_TuningVersExt.Text = PublicConst.Tuning_version_ext;
            coB_CarRevision.SelectedIndex = PublicConst.Car_revision;
            coB_AmpType.SelectedIndex = PublicConst.Car_AmpList;
            tb_AuthorInt.Text = PublicConst.Author_int;
            tb_AuthorExt.Text = PublicConst.Author_ext;
            tb_CommentInt.Text = PublicConst.Tuning_comment_int;
            tb_CommentExt.Text = PublicConst.Tuning_comment_ext;
            tb_Extern_EQ_Path.Text = PublicConst.EQ_xml_extern_Dir;
            tb_Extern_EQ_name.Text = PublicConst.EQ_xml_extern_OrgName;
            tb_Intern_EQ_Path.Text = PublicConst.EQ_xml_intern_Dir;
            tb_Intern_EQ_name.Text = PublicConst.EQ_xml_intern_OrgName;
            numUD_CarMics.Value = Convert.ToInt32(PublicConst.Car_MicNo);
            numUD_CarEQ.Value = Convert.ToInt32(PublicConst.Car_EQ);
            tb_AudioPackage.Text = "";
            lbl_AudioPackageDate.Text = "";
            fill_CarVersion();

            hash_calculation();
            EQ_file_newname();

            //CBox2020
            tb_CMICext.Text = PublicConst.NAD_CMICext;
            tb_CLVLext.Text = PublicConst.NAD_CLVLext;

        }

        private void Save_configuration()
        {
            //Xml-Configuration-File Schreiben
            Public_Const.Configuration InstPublic = new Public_Const.Configuration();
            int i = InstPublic.Csave();
        }

        private void File_Click(object sender, EventArgs e)
        {
            Save_configuration();
            Application.Exit();
        }

        private void FrmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            Save_configuration();
        }

        private void versionToolStripMenuItem_Click(object sender, EventArgs e)
        {

            frm.ShowDialog();
        }

        private void btn_extern_EQ_Dir_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Title = "Select a File";
            openFileDialog1.Filter = "EQ-file|*.xml";
            if ((PublicConst.EQ_xml_extern_Dir != "") & (cb_SourceDirExt.Checked==true))
            {
                openFileDialog1.InitialDirectory = PublicConst.EQ_xml_extern_Dir;
            }
            else
            {
                if (PublicConst.Project_Dir != "")
                {
                    openFileDialog1.InitialDirectory = PublicConst.Project_Dir;
                }
                else
                {
                    openFileDialog1.InitialDirectory = Application.StartupPath;
                }
            }
            if (PublicConst.expert_modus == 1) { openFileDialog1.Filter = "EQ-File|*.xml|All-File|*.*"; }
            
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                string thelp = openFileDialog1.FileName;
                tb_Extern_EQ_Path.Text = Path.GetDirectoryName(thelp);
                PublicConst.EQ_xml_extern_Dir = tb_Extern_EQ_Path.Text;
                tb_Extern_EQ_name.Text = Path.GetFileName(thelp);
                PublicConst.EQ_xml_extern_OrgName = tb_Extern_EQ_name.Text;
                hash_calculation();
                EQ_file_newname();
            }
        }

        private void btn_Intern_EQ_Dir_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog2 = new OpenFileDialog();
            openFileDialog2.Title = "Select a File";
            openFileDialog2.Filter = "EQ-file|*.xml";
            if ((PublicConst.EQ_xml_intern_Dir != "") & (cb_SourceDirInt.Checked ==true))
            {
                openFileDialog2.InitialDirectory = PublicConst.EQ_xml_intern_Dir;
            }
            else
            {
                if (PublicConst.Project_Dir != "")
                {
                    openFileDialog2.InitialDirectory = PublicConst.Project_Dir;
                }
                else
                {
                    openFileDialog2.InitialDirectory = Application.StartupPath;
                }
            }
            if (PublicConst.expert_modus == 1) { openFileDialog2.Filter = "EQ-File|*.xml|All-File|*.*"; }

            if (openFileDialog2.ShowDialog() == DialogResult.OK)
            {
                string thelp=openFileDialog2.FileName;
                tb_Intern_EQ_Path.Text = Path.GetDirectoryName(thelp);
                PublicConst.EQ_xml_intern_Dir = tb_Intern_EQ_Path.Text;
                tb_Intern_EQ_name.Text = Path.GetFileName(thelp);
                PublicConst.EQ_xml_intern_OrgName = tb_Intern_EQ_name.Text;
                hash_calculation();
                EQ_file_newname();
            }
        }

        private void hash_calculation()
        {
            //Berechnet die hash werte der EQ's
            string str_hash1 = "";
            string str_hash2 = "";

            try
            {
                if (rb_CRC16.Checked)
                {
                    str_hash1 = CRC16_hash_calc(Path.Combine(PublicConst.EQ_xml_intern_Dir, PublicConst.EQ_xml_intern_OrgName));
                    str_hash2 = CRC16_hash_calc(Path.Combine(PublicConst.EQ_xml_extern_Dir, PublicConst.EQ_xml_extern_OrgName));
                }
                if (rb_CRC32.Checked)
                {
                    str_hash1 = CRC32_hash_calc(Path.Combine(PublicConst.EQ_xml_intern_Dir, PublicConst.EQ_xml_intern_OrgName));
                    str_hash2 = CRC32_hash_calc(Path.Combine(PublicConst.EQ_xml_extern_Dir, PublicConst.EQ_xml_extern_OrgName));
                }
                if (rb_MD5.Checked)
                {
                    str_hash1 = MD5_hash_calc(Path.Combine(PublicConst.EQ_xml_intern_Dir, PublicConst.EQ_xml_intern_OrgName));
                    str_hash2 = MD5_hash_calc(Path.Combine(PublicConst.EQ_xml_extern_Dir, PublicConst.EQ_xml_extern_OrgName));
                }

                if (rb_SHA1.Checked)
                {
                    str_hash1 = SHA1_hash_calc(Path.Combine(PublicConst.EQ_xml_intern_Dir, PublicConst.EQ_xml_intern_OrgName));
                    str_hash2 = SHA1_hash_calc(Path.Combine(PublicConst.EQ_xml_extern_Dir, PublicConst.EQ_xml_extern_OrgName));
                }
            }
            catch
            { }

            tb_Intern_EQ_HashName.Text = str_hash1;
            PublicConst.EQ_xml_intern_Hash = str_hash1;
            tb_Extern_EQ_HashName.Text = str_hash2;
            PublicConst.EQ_xml_extern_Hash = str_hash2;
        }

        private string SHA1_hash_calc(string filename)
        {
            string Berechnet = "";
            if (System.IO.File.Exists(filename))
            {
                //Datei einlesen
                System.IO.FileStream FileCheck = System.IO.File.OpenRead(filename);
                // SHA1-Hash aus dem Byte-Array berechnen
                System.Security.Cryptography.SHA1 sha1 = new System.Security.Cryptography.SHA1CryptoServiceProvider();
                byte[] sha1Hash = sha1.ComputeHash(FileCheck);
                FileCheck.Close();

                //in string wandeln
                //Berechnet = BitConverter.ToString(md5Hash).Replace("-", "").ToLower();
                Berechnet = BitConverter.ToString(sha1Hash).ToUpper();
                // Vergleichen
                //if (Berechnet == Checksumme.ToLower())
            }
            return Berechnet;
        }


        private string MD5_hash_calc(string filename)
        {
            string Berechnet = "";
            if (System.IO.File.Exists(filename))
            { 
                //Datei einlesen
                System.IO.FileStream FileCheck = System.IO.File.OpenRead(filename);                
                // MD5-Hash aus dem Byte-Array berechnen
                System.Security.Cryptography.MD5 md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
                byte[] md5Hash = md5.ComputeHash(FileCheck);                
                FileCheck.Close();
                
                //in string wandeln
                //Berechnet = BitConverter.ToString(md5Hash).Replace("-", "").ToLower();
                Berechnet = BitConverter.ToString(md5Hash).ToUpper();
                // Vergleichen
                //if (Berechnet == Checksumme.ToLower())
            }
            return Berechnet;
        }

        private string CRC32_hash_calc(string filename)
        {
            string Berechnet = "";
            if (System.IO.File.Exists(filename))
            {
                // CRC32-hash berechnen
                CRC_check.File_CRC32 InstPublic = new CRC_check.File_CRC32();
                uint iCRC = InstPublic.GetCRC32(filename);
                
                //in string wandeln
                Berechnet = iCRC.ToString("x08").ToUpper();
                
                // Vergleichen
                //if (Berechnet == Checksumme.ToLower())
            }
            return Berechnet;
        }

        private string CRC16_hash_calc(string filename)
        {
            string Berechnet = "";
            if (System.IO.File.Exists(filename))
            {
                // CRC16-hash berechnen
                CRC_check.File_CRC32 InstPublic = new CRC_check.File_CRC32();
                uint iCRC = InstPublic.GetCRC16(filename);
                Berechnet = iCRC.ToString("x04").ToUpper();
                
                // Vergleichen
                //if (Berechnet == Checksumme.ToLower())
            }
            return Berechnet;
        }

        private void rb_CRC16_CheckedChanged(object sender, EventArgs e)
        {
            hash_calculation();
            EQ_file_newname();
        }

        private void EQ_file_newname()
        {
            //interner EQ-filename
            string Te1 = tb_Intern_EQ_name.Text;
            //GetFileNameWithoutExtension
            string f_ext = Path.GetExtension(Te1);  // Erweiterung merken

            string Te1sub=Te1;
            string Te1New = "";
            if (Te1 != "")
            {
                int iPos = Te1.IndexOf(PublicConst.EQ_xml_HashStart, 0);
                //if (iPos > 0) { Te1sub = Te1.Substring(0, iPos); } else { Te1sub = Te1.Substring(0, Te1.IndexOf(PublicConst.EQ_xml_suffix)); }
                //Te1New = Te1sub + PublicConst.EQ_xml_HashStart + tb_Intern_EQ_HashName.Text + PublicConst.EQ_xml_suffix;
                if (iPos > 0) { Te1sub = Te1.Substring(0, iPos); } else { Te1sub = Te1.Substring(0, Te1.IndexOf(f_ext)); }
                Te1New = Te1sub + PublicConst.EQ_xml_HashStart + tb_Intern_EQ_HashName.Text + f_ext;
            }
            tb_Intern_EQ_Newname.Text = Te1New;
            PublicConst.EQ_xml_intern_NewName = Te1New;
            
            //externer EQ-filename
            Te1 = tb_Extern_EQ_name.Text;
            f_ext = Path.GetExtension(Te1);  // Erweiterung merken
            Te1sub = Te1;
            Te1New = "";
            if (Te1 != "")
            {
                int iPos = Te1.IndexOf(PublicConst.EQ_xml_HashStart, 0);
                //if (iPos > 0) { Te1sub = Te1.Substring(0, iPos); } else { Te1sub = Te1.Substring(0, Te1.IndexOf(PublicConst.EQ_xml_suffix)); }
                //Te1New = Te1sub + PublicConst.EQ_xml_HashStart + tb_Extern_EQ_HashName.Text + PublicConst.EQ_xml_suffix;
                if (iPos > 0) { Te1sub = Te1.Substring(0, iPos); } else { Te1sub = Te1.Substring(0, Te1.IndexOf(f_ext)); }
                Te1New = Te1sub + PublicConst.EQ_xml_HashStart + tb_Extern_EQ_HashName.Text + f_ext;
            }
            tb_Extern_EQ_Newname.Text = Te1New;
            PublicConst.EQ_xml_extern_NewName = Te1New;
        }

        private void tb_TuningVersInt_TextChanged(object sender, EventArgs e)
        {
            PublicConst.Tuning_version_int = tb_TuningVersInt.Text;
        }

        private void tb_TuningVersExt_TextChanged(object sender, EventArgs e)
        {
            PublicConst.Tuning_version_ext = tb_TuningVersExt.Text;
        }

        private void coB_CarRevision_SelectedIndexChanged(object sender, EventArgs e)
        {
            PublicConst.Car_revision = coB_CarRevision.SelectedIndex;
            fill_CarVersion();
        }

        private void numUD_CarMics_ValueChanged(object sender, EventArgs e)
        {
            PublicConst.Car_MicNo = numUD_CarMics.Value.ToString();
            fill_CarVersion();
        }

        private void numUD_CarEQ_ValueChanged(object sender, EventArgs e)
        {
            PublicConst.Car_EQ = numUD_CarEQ.Value.ToString("00");
            fill_CarVersion();
        }


        
        private void fill_CarVersion()
        {
            string f1 = "";
            int Ind = 0;
            Ind = coB_CarRevision.SelectedIndex;
            if (Ind >= 0)
            {
                f1 = coB_CarRevision.SelectedItem.ToString();
            }
            else { f1 = "xxx"; }
            f1 = PublicConst.Car_MicNo + f1.Substring(0, 1);
            if (PublicConst.Car_EQ.Length <2)
            {
                f1 = f1 + "0" + PublicConst.Car_EQ;
            }
            else
            {
                f1 = f1 + PublicConst.Car_EQ;
            }
            tb_CarVersion.Text = f1;
            PublicConst.Car_version = f1;
        }

        private void but_ExtDataInfo_Click(object sender, EventArgs e)
        {
            if ((Control.ModifierKeys & Keys.Shift) == Keys.Shift)
            {
                if (pnlCRC.Visible == false)
                {
                    pnlCRC.Visible = true;
                }
                else
                {
                    pnlCRC.Visible = false;
                }
                rb_CRC16.Checked = true;
            }
            else
            {
                if (tb_CLVL.Enabled == true)
                {
                    tb_lua_crc.Enabled = false;
                    tbLuaName.Enabled = false;
                    tb_CMIC.Enabled = false;
                    tb_CLVL.Enabled = false;
                    tb_CMICext.Enabled = false;
                    tb_CLVLext.Enabled = false;
                    cbByPassExt.Enabled = false;
                    btn_LuaSelection.Visible = false;
                    btnGetAudioPackage.Visible = false;
                    PublicConst.expert_modus = 0;
                }
                else
                {
                    tb_lua_crc.Enabled = false;
                    tbLuaName.Enabled = true;
                    tb_CMIC.Enabled = true;
                    tb_CLVL.Enabled = true;
                    tb_CMICext.Enabled = true;
                    tb_CLVLext.Enabled = true;
                    cbByPassExt.Enabled = true;
                    btn_LuaSelection.Visible = true;
                    btnGetAudioPackage.Visible = true;
                    PublicConst.expert_modus = 1;
                }
            }
        }

        private void btn_OpenFile_Click(object sender, EventArgs e)
        {
            openProject();
        }
         
        private void openProject()
        {
            clearAllTBox();
            PublicConst.EQ_xml_extern_Dir = "";
            PublicConst.EQ_xml_extern_OrgName = "";
            PublicConst.EQ_xml_intern_Dir = "";
            PublicConst.EQ_xml_intern_OrgName = "";
         
            OpenFileDialog openFileDialog3 = new OpenFileDialog();
            openFileDialog3.Filter = "JSON-Config-File|*.json";
            openFileDialog3.Title = "Select a Config-File";
            if (PublicConst.Project_Dir != "") { openFileDialog3.InitialDirectory = PublicConst.Project_Dir; }
            else { openFileDialog3.InitialDirectory = Application.StartupPath; }

            string thelp = "";
            if (openFileDialog3.ShowDialog() == DialogResult.OK)
            {
                thelp = openFileDialog3.FileName;
                PublicConst.Project_File = Path.GetFileName(thelp);
                PublicConst.Project_Dir = Path.GetDirectoryName(thelp);
            }
            if (thelp != "")
            {
                //read json
                try
                {
                    Class_JSON InstJSON = new Class_JSON();
                    int w = InstJSON.read_jsonConfig(Path.Combine(PublicConst.Project_Dir,PublicConst.Project_File));
                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.Message + ", Error while open project file");
                }
                //refresh publics

                string shelpxx = PublicConst.Car_version;
                PublicConst.Car_EQ = shelpxx.Substring(shelpxx.Length - 2, 2);
                PublicConst.Car_MicNo = shelpxx.Substring(0, 1);
                PublicConst.EQ_xml_extern_Dir = PublicConst.Project_Dir;
                PublicConst.EQ_xml_extern_OrgName = PublicConst.EQ_xml_extern_NewName;
                PublicConst.EQ_xml_intern_Dir = PublicConst.Project_Dir;
                PublicConst.EQ_xml_intern_OrgName = PublicConst.EQ_xml_intern_NewName;

                setAllTextboxes();  //incl. hash_calculation EQs;

                PublicConst.package_name = PublicConst.package_separator + PublicConst.Car_version + "_" + PublicConst.Car_Datensatzname;
                string packageName = Path.Combine(PublicConst.Project_Dir, PublicConst.Package_subdir, PublicConst.package_name + ".tar.gz");
                if(System.IO.File.Exists(packageName))
                {
                    tb_AudioPackage.Text = packageName;
                    lbl_AudioPackageDate.Text = (System.IO.File.GetLastWriteTime(System.IO.Path.Combine(PublicConst.Project_Dir, PublicConst.Package_subdir, PublicConst.package_name + ".tar.gz"))).ToString();

                    // update in Config-Box
                    if (FrmCBoxConfig.Visible)
                    {
                        FrmCBoxConfig.Activate();
                    }
                    this.Activate();
                }
            }
        }

        private void newProjectDir()
        {
            // Legt neeuen Projekt Ordner fest
            FolderBrowserDialog folderDialog1 = new FolderBrowserDialog();
            folderDialog1.SelectedPath = Application.StartupPath;
            if (PublicConst.Project_Dir != "") { folderDialog1.SelectedPath = PublicConst.Project_Dir; }
            if (folderDialog1.ShowDialog() == DialogResult.OK)
            {
                tb_ProjectPath.Text = folderDialog1.SelectedPath;
                PublicConst.Project_Dir = folderDialog1.SelectedPath;
            }
        }

        private void newProject()
        {
            clearAllTBox();
            newProjectDir();
            lua_copy();
            tb_ProjectFile.Text = PublicConst.Project_File;
        }

        private void clearAllTBox()
        {
            //löscht alle Textboxen
            foreach (Control c in gb_AudioControl.Controls)
            {
                if ((c.GetType() == typeof(TextBox))  && (c.Name.Contains("tb_"))) 
                { c.Text = ""; }
            }
            foreach (Control c in gb_InternalEQ.Controls)
            {
                if ((c.GetType() == typeof(TextBox)) && (c.Name.Contains("tb_")))
                { c.Text = ""; }
            }
            foreach (Control c in gb_ExternalEQ.Controls)
            {
                if ((c.GetType() == typeof(TextBox)) && (c.Name.Contains("tb_")))
                { c.Text = ""; }
            }
            tb_AudioPackage.Text = "";
            lbl_AudioPackageDate.Text = "";
            coB_CarRevision.SelectedIndex = -1;
        }

        private void btn_NewProject_Click(object sender, EventArgs e)
        {
            newProject();
        }

        private void menue_OpenFile_Click(object sender, EventArgs e)
        {
            btn_OpenFile_Click(sender, e);
        }

        private void menue_NewProject_Click(object sender, EventArgs e)
        {
            btn_NewProject_Click(sender, e);
        }

        private int eq_files_copy()
        {
            int err1 = 0;
            try
            {
                //public static void Copy(string sourceFileName, string destFileName, bool overwrite)
                string sourceFileName = Path.Combine(tb_Intern_EQ_Path.Text, tb_Intern_EQ_name.Text);
                string destFileName = Path.Combine(tb_ProjectPath.Text, tb_Intern_EQ_Newname.Text);

                // EQ intern        
                if (System.IO.File.Exists(destFileName) == false)
                {
                    System.IO.File.Copy(sourceFileName, destFileName, true);
                    tb_Intern_EQ_Path.Text = tb_ProjectPath.Text;
                    tb_Intern_EQ_name.Text = tb_Intern_EQ_Newname.Text;
                }
                // EQ extern        
                sourceFileName = Path.Combine(tb_Extern_EQ_Path.Text, tb_Extern_EQ_name.Text);
                destFileName = Path.Combine(tb_ProjectPath.Text, tb_Extern_EQ_Newname.Text);
                if (System.IO.File.Exists(destFileName) == false)
                {
                    System.IO.File.Copy(sourceFileName, destFileName, true);
                    tb_Extern_EQ_Path.Text = tb_ProjectPath.Text;
                    tb_Extern_EQ_name.Text = tb_Extern_EQ_Newname.Text;
                }
            }
            catch (Exception ex)
            {
                err1 = 1;
                Console.WriteLine(ex.Message);
                MessageBox.Show(ex.Message + ", Error while copy EQ-File");
            }
            return err1;
        }

        private void lua_copy()
        {
            try
            {
                //public static void Copy(string sourceFileName, string destFileName, bool overwrite)
                string sourceFileName = Path.Combine(Application.StartupPath, PublicConst.Lua_sourcePath, PublicConst.Lua_fileName);
                string destFileName = Path.Combine(tb_ProjectPath.Text, PublicConst.Lua_fileName);

                if (System.IO.File.Exists(destFileName) == false)
                {
                    System.IO.File.Copy(sourceFileName, destFileName, true);
                }
                tbLuaName.Text = Path.GetFileName(destFileName);
                PublicConst.Lua_CRC = file_crc16_check(destFileName);  // lua auf crc prüfen
                tb_lua_crc.Text = PublicConst.Lua_CRC;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                MessageBox.Show(ex.Message + ", Error while copy LUA-File");
            }
        }


        private void btn_AudioPackage_Click(object sender, EventArgs e)
        {
            int err = eq_files_copy();
            if (err == 0)
            {
                PublicConst.package_name = PublicConst.package_separator + PublicConst.Car_version + "_" + PublicConst.Car_Datensatzname;
                PublicConst.Date_create = DateTime.Now.ToString("dd.MM.yy HH:mm:ss");
                project_file_create();
                try
                {
                    create_package();
                }
                catch (Exception ex)
                {
                    err = 2;
                    MessageBox.Show(ex.Message + ", Error while create package");
                }
                if (err == 0)
                {
                    if (FrmCBoxConfig.Visible)
                    {
                        FrmCBoxConfig.Activate();
                    }
                    this.Activate();
                }
            }
        }


        public void project_file_create()
        {
            Class_JSON InstJSON = new Class_JSON();
            int w = InstJSON.write_jsonConfig(Path.Combine(PublicConst.Project_Dir, PublicConst.Project_File));
        }


        public string file_crc16_check(string filename)
        {
            string vret = "";
            string str_hash1 ="";
            string thelp="";
            string str_hashInName = "";
            
            if (System.IO.File.Exists(filename))
            {
                str_hash1 = CRC16_hash_calc(filename);
                vret = str_hash1;
                thelp = Path.GetFileName(filename);
                int iPos = thelp.IndexOf(PublicConst.EQ_xml_HashStart, 0);
                if (iPos > 0) { str_hashInName = thelp.Substring(iPos+3, 4); }
                if (str_hash1 != str_hashInName) 
                {  
                    MessageBox.Show("LUA-file has false CRC value! File: " + str_hash1 + " and Hash in name: " + str_hashInName);
                    vret = "-1";
                }
            }
            return vret;
        }

        private void tb_CMIC_TextChanged(object sender, EventArgs e)
        {
            PublicConst.NAD_CMIC= tb_CMIC.Text;
        }

        private void tb_CLVL_TextChanged(object sender, EventArgs e)
        {
            PublicConst.NAD_CLVL = tb_CLVL.Text;
        }

        private void tb_CarAUNo_TextChanged(object sender, EventArgs e)
        {
            PublicConst.Car_type = tb_CarAUNo.Text;
            filldatensatzname();
  //            PublicConst.Car_Datensatzname = PublicConst.Car_Datensatzname_Praefix +PublicConst.Car_type +PublicConst.Car_Datensatzname_Suffix +PublicConst.Car_AmpType;
  //            tb_CarDatensatzname.Text = PublicConst.Car_Datensatzname;
        }

        private void coB_AmpType_SelectedIndexChanged(object sender, EventArgs e)
        {
            PublicConst.Car_AmpList = coB_AmpType.SelectedIndex;
            string stemp = coB_AmpType.SelectedItem.ToString();
            PublicConst.Car_AmpType = stemp.Substring(0,5);
            filldatensatzname();
        }

        public void filldatensatzname()
        {
            PublicConst.Car_Datensatzname = PublicConst.Car_Datensatzname_Praefix + PublicConst.Car_type + PublicConst.Car_Datensatzname_Suffix + PublicConst.Car_AmpType;
            tb_CarDatensatzname.Text = PublicConst.Car_Datensatzname;
        }

        private void tb_AuthorInt_TextChanged(object sender, EventArgs e)
        {
            PublicConst.Author_int = tb_AuthorInt.Text;
        }

        private void tb_AuthorExt_TextChanged(object sender, EventArgs e)
        {
            PublicConst.Author_ext = tb_AuthorExt.Text;
        }

        private void tb_CommentInt_TextChanged(object sender, EventArgs e)
        {
            PublicConst.Tuning_comment_int = tb_CommentInt.Text;
        }

        private void tb_CommentExt_TextChanged(object sender, EventArgs e)
        {
            PublicConst.Tuning_comment_ext = tb_CommentExt.Text;
        }

        public void create_package1()
        // funktioniert nicht mit kurzem Namen der zu packenden Dateien !!!!
        {
            string tarDir = Path.Combine(PublicConst.Project_Dir, PublicConst.Package_subdir);
            if (Directory.Exists(tarDir) == false)
            { 
                Directory.CreateDirectory(tarDir);
            }
            string tarArchiveName = Path.Combine("C:\\A\\cBox_audio_Dataset\\Dataset_X01", PublicConst.Package_subdir, PublicConst.package_name + ".tar.gz");
  //          string tarArchiveName = Path.Combine(PublicConst.Project_Dir, PublicConst.Package_subdir, PublicConst.package_name + ".tar.gz");
            if (System.IO.File.Exists(tarArchiveName))
            { 
                System.IO.File.Delete(tarArchiveName); 
            }

            DirectoryInfo directoryOfFilesToBeTarred = new DirectoryInfo(PublicConst.Project_Dir);
            FileInfo[] filesInDirectory = directoryOfFilesToBeTarred.GetFiles();

            try
            {
                    using (Stream targetStream = new GZipOutputStream(System.IO.File.Create(tarArchiveName)))
                {
                    using (TarArchive tarArchive = TarArchive.CreateOutputTarArchive(targetStream, TarBuffer.DefaultBlockFactor))
                    {
                        foreach (FileInfo fileToBeTarred in filesInDirectory)
                        {
                            //TarEntry entry = TarEntry.CreateEntryFromFile(fileToBeTarred.FullName);
                            //TarEntry entry = TarEntry.CreateEntryFromFile(Path.GetFileName(fileToBeTarred.FullName));
                            TarEntry entry = TarEntry.CreateEntryFromFile(fileToBeTarred.Name);
                            tarArchive.WriteEntry(entry, true);
                        }
                    }
                }

            }
                    catch(Exception ex)
            {
                MessageBox.Show("Warning: Package is corrupt!: " +ex);
            }
        }

        public void create_package()
        {
            string tarDir = Path.Combine(PublicConst.Project_Dir, PublicConst.Package_subdir);
            if (Directory.Exists(tarDir) == false)
            {
                Directory.CreateDirectory(tarDir);
            }
         //   string tarArchiveName = Path.Combine("C:\\A\\cBox_audio_Dataset\\Dataset_X01", PublicConst.Package_subdir, PublicConst.package_name + ".tar.gz");
            // funktioniert nur, wenn rekursiv false ist, da das Package in einem Subdir des Projektes liegt !!!!!!!! 
            string tarArchiveName = Path.Combine(PublicConst.Project_Dir, PublicConst.Package_subdir, PublicConst.package_name + ".tar.gz");
            if (System.IO.File.Exists(tarArchiveName))
            {
                System.IO.File.Delete(tarArchiveName);
            }

            string sourceDirectory = PublicConst.Project_Dir;

            Stream outStream = System.IO.File.Create(tarArchiveName);

            Stream gzoStream = new GZipOutputStream(outStream);
            TarArchive tarArchive = TarArchive.CreateOutputTarArchive(gzoStream);
            try
            {
                // Note that the RootPath is currently case sensitive and must be forward slashes e.g. "c:/temp"
            // and must not end with a slash, otherwise cuts off first char of filename
                // This is scheduled for fix in next release
                tarArchive.RootPath = sourceDirectory.Replace('\\', '/');
                if (tarArchive.RootPath.EndsWith("/"))
                    tarArchive.RootPath = tarArchive.RootPath.Remove(tarArchive.RootPath.Length - 1);

                //AddDirectoryFilesToTar(tarArchive, sourceDirectory, false);  //alle Dateien i, Verzeichnis, recursive = false
                AddFileListToTar(tarArchive, sourceDirectory);  // Liste von Dateien

                tarArchive.Close();
                tb_AudioPackage.Text = Path.GetFileName(tarArchiveName);
                lbl_AudioPackageDate.Text = (System.IO.File.GetLastWriteTime(System.IO.Path.Combine(PublicConst.Project_Dir, PublicConst.Package_subdir, PublicConst.package_name + ".tar.gz"))).ToString();
            }

            catch (Exception ex)
            {
                tarArchive.Close();
                MessageBox.Show("Warning: Package is corrupt!: " + ex);
            }
        }

        private void AddFileListToTar(TarArchive tarArchive, string destPath)
        {
            TarEntry tarEntry = TarEntry.CreateEntryFromFile(destPath);
            string[] filenames = new string[10];
            filenames[0] = Path.Combine(PublicConst.Project_Dir, PublicConst.Lua_fileName);  //lua_File
            filenames[1] = Path.Combine(PublicConst.Project_Dir, PublicConst.EQ_xml_intern_NewName);    //EQ eCall Speaker
            filenames[2] = Path.Combine(PublicConst.Project_Dir, PublicConst.EQ_xml_extern_NewName);   //EQ-Lineout 
            filenames[3] = Path.Combine(PublicConst.Project_Dir, PublicConst.Project_File);    //Audio config file
            filenames[4] = Path.Combine(PublicConst.Project_Dir, PublicConst.Project_textFile);    //Audio config text file

            //foreach string
            string filename = "";
            for (int i = 0; i < 5; i++)
            {
                filename = filenames[i];
                tarEntry = TarEntry.CreateEntryFromFile(filename);
                tarArchive.WriteEntry(tarEntry, true);
            }
        }
    
    	private void AddDirectoryFilesToTar(TarArchive tarArchive, string sourceDirectory, bool recurse) 
        {
        // Optionally, write an entry for the directory itself.
    	    // Specify false for recursion here if we will add the directory's files individually.
    	    //
    	    TarEntry tarEntry = TarEntry.CreateEntryFromFile(sourceDirectory);
    	    tarArchive.WriteEntry(tarEntry, false);
    
    	    // Write each file to the tar.
    	    //

            string[] filenames = Directory.GetFiles(sourceDirectory);
            foreach (string filename in filenames) 
            {
                tarEntry = TarEntry.CreateEntryFromFile(filename);
                tarArchive.WriteEntry(tarEntry, true);
            }
    
            if (recurse) 
            {
                string[] directories = Directory.GetDirectories(sourceDirectory);
                foreach (string directory in directories)
                    AddDirectoryFilesToTar(tarArchive, directory, recurse);
            }
        }

        public void FastZipUnpack(string zipFileName, string targetDir)
        {

            FastZip fastZip = new FastZip();
            string fileFilter = null;

            // Will always overwrite if target filenames already exist
            fastZip.ExtractZip(zipFileName, targetDir, fileFilter);
        }

        // example: ExtractTGZ(@"c:\temp\test.tar.gz", @"C:\DestinationFolder")

        public void ExtractTGZ(String gzArchiveName, String destFolder)
        {

            Stream inStream = System.IO.File.OpenRead(gzArchiveName);
            Stream gzipStream = new GZipInputStream(inStream);

            TarArchive tarArchive = TarArchive.CreateInputTarArchive(gzipStream);
            tarArchive.ExtractContents(destFolder);
            tarArchive.Close();

            gzipStream.Close();
            inStream.Close();
        }
        
        private void btn_ImportPackage_Click(object sender, EventArgs e)
        {
             clearAllTBox();
            PublicConst.Car_version = "";
            PublicConst.EQ_xml_extern_Dir = "";
            PublicConst.EQ_xml_extern_OrgName = "";
            PublicConst.EQ_xml_extern_NewName = "";
            PublicConst.EQ_xml_intern_Dir = "";
            PublicConst.EQ_xml_intern_OrgName = "";
            PublicConst.EQ_xml_intern_NewName = "";

            OpenFileDialog openFileDialog3 = new OpenFileDialog();
            openFileDialog3.Filter = "Package|*.tar.gz";
            openFileDialog3.Title = "Select a Package";
            if (PublicConst.Project_Dir != "") { openFileDialog3.InitialDirectory = PublicConst.Project_Dir; }
            else { openFileDialog3.InitialDirectory = Application.StartupPath; }

            string thelp = "";
            if (openFileDialog3.ShowDialog() == DialogResult.OK)
            {
                thelp = openFileDialog3.FileName;
                PublicConst.Project_Dir = Path.GetDirectoryName(thelp);
            }

            //Unpack files
            if (thelp != "")
            {
                //FastZipUnpack(thelp, PublicConst.Project_Dir);
                ExtractTGZ(thelp, PublicConst.Project_Dir);

            }
            
            //read json
            //if (thelp != "")
            PublicConst.Project_File = PublicConst.Project_FileOrg;  //"audio_config.txt"
            if (System.IO.File.Exists(Path.Combine(PublicConst.Project_Dir, PublicConst.Project_File)))
                {
                //set json.file
                int readErr = 0;
                try
                {
                    Class_JSON InstJSON = new Class_JSON();
                    readErr = InstJSON.read_jsonConfig(Path.Combine(PublicConst.Project_Dir, PublicConst.Project_File));
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message + ", Error while open project file");
                }
                //refresh publics

                string shelpxx = PublicConst.Car_version;
                PublicConst.Car_EQ = shelpxx.Substring(shelpxx.Length -2,2);
                PublicConst.Car_MicNo = shelpxx.Substring(0, 1);
                PublicConst.EQ_xml_extern_Dir = PublicConst.Project_Dir;
                PublicConst.EQ_xml_extern_OrgName = PublicConst.EQ_xml_extern_NewName;
                PublicConst.EQ_xml_intern_Dir = PublicConst.Project_Dir;
                PublicConst.EQ_xml_intern_OrgName = PublicConst.EQ_xml_intern_NewName;

                setAllTextboxes();  //incl. hash_calculation EQs;

                //move package
                string tarDir = Path.Combine(Path.GetDirectoryName(thelp), PublicConst.Package_subdir);
                if (Directory.Exists(tarDir) == false)
                {
                    Directory.CreateDirectory(tarDir);
                }

                string tarArchiveName = Path.Combine(Path.GetDirectoryName(thelp), PublicConst.Package_subdir, Path.GetFileName(thelp));
                if (System.IO.File.Exists(tarArchiveName))
                {
                    System.IO.File.Delete(tarArchiveName);
                }
                System.IO.File.Move(thelp, tarArchiveName);

                //set package name
                tb_AudioPackage.Text = Path.GetFileName(thelp);
                
                string thelp11 = Path.GetFileName(tarArchiveName);
                thelp11 = Path.GetFileNameWithoutExtension(thelp11);
                PublicConst.package_name = Path.GetFileNameWithoutExtension(thelp11);

                lbl_AudioPackageDate.Text = (System.IO.File.GetLastWriteTime(System.IO.Path.Combine(PublicConst.Project_Dir, PublicConst.Package_subdir, PublicConst.package_name + ".tar.gz"))).ToString();

                // update in Config-Box
                if (FrmCBoxConfig.Visible)
                {
                    FrmCBoxConfig.Activate();
                }
                this.Activate();

            }
        }
        


        private void btn_LuaSelection_Click(object sender, EventArgs e)
        {
            luaSelect();
        }

        public void luaSelect ()
        {
            OpenFileDialog openFileDialog4 = new OpenFileDialog();
            openFileDialog4.Filter = "LUA-Config-File|*.lua";
            openFileDialog4.Title = "Select a Config-File";
            //openFileDialog4.InitialDirectory = Application.StartupPath;
            openFileDialog4.InitialDirectory = PublicConst.Project_Dir;

            if (openFileDialog4.ShowDialog() == DialogResult.OK)
            {
                string thelp = openFileDialog4.FileName;
                string tDir = Path.GetDirectoryName(thelp);
                
                //lua handling
                PublicConst.Lua_fileName = "";
                tbLuaName.Text = "";

                string vst1 = file_crc16_check(thelp);  // lua auf crc prüfen
                if ((vst1 != "-1")&(vst1!=""))
                {
                    PublicConst.Lua_fileName = Path.GetFileName(thelp);
                    PublicConst.Lua_sourcePath = tDir;
                    string destFileName = Path.Combine(tb_ProjectPath.Text, PublicConst.Lua_fileName);
                    tbLuaName.Text = Path.GetFileName(destFileName);
                    PublicConst.Lua_CRC = vst1;
                    tb_lua_crc.Text = PublicConst.Lua_CRC;
                    if (System.IO.File.Exists(destFileName) == false) { lua_copy(); }  // lua ins project kopieren, wenn noch nicht vorhanden
                }
            }
        }

        private void uploadECallConfigToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        public void eCallConfiSelect()
        {
            OpenFileDialog openFileDialog5 = new OpenFileDialog();
            openFileDialog5.Filter = "eCall-Config-File|*.json";
            openFileDialog5.Title = "Select a Config-File";
            if (PublicConst.eCallParamDir != "") { openFileDialog5.InitialDirectory = PublicConst.eCallParamDir; }
              else { openFileDialog5.InitialDirectory = Application.StartupPath; }

            if (openFileDialog5.ShowDialog() == DialogResult.OK)
            {
                PublicConst.eCallParamDir = Path.GetDirectoryName(openFileDialog5.FileName);
                string eCallConfigPath = openFileDialog5.FileName;
                
                //eCall-json handling
                string MyBatchFile = PublicConst.batch_upload_eCallConfig;
                string sourcePath1 = eCallConfigPath;
                
                var process = new Process
                {
                    StartInfo =
                    {
                        Arguments = string.Format("{0}",
                                                  sourcePath1)
                    }
                };
                process.StartInfo.FileName = MyBatchFile;
                try
                {
                    bool b = process.Start();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error with running - " + MyBatchFile + " - " + Environment.NewLine + ex);
                }
            }
        }

        private void Reboot_toolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

 

        private void cb_SourceDirInt_CheckedChanged(object sender, EventArgs e)
        {
            if (cb_SourceDirInt.Checked==true)
            {
                PublicConst.cb_SourceDirInt_v_checked="1";
            }
            else
            {
                PublicConst.cb_SourceDirInt_v_checked="0";
            }
        }

        private void cb_SourceDirExt_CheckedChanged(object sender, EventArgs e)
        {
            if (cb_SourceDirExt.Checked == true)
            {
                PublicConst.cb_SourceDirExt_v_checked = "1";
            }
            else
            {
                PublicConst.cb_SourceDirExt_v_checked = "0";
            }
        }

        private void conBoxConfigurationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmCBoxConfig.Show();
        }

        private void createToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // internen Container für ODX bauen
            string json_formated=""; 
            string archiveName = Path.Combine(PublicConst.Project_Dir, PublicConst.Package_subdir, PublicConst.package_name + ".tar.gz");
            string str_hash1 = SHA1_hash_calc(archiveName);
            str_hash1 = str_hash1.Replace("-", "") ;

            string NL = Environment.NewLine;
                //string NL = ;
                    json_formated = "{" + NL;
                    json_formated = json_formated + "    " + '"' + "Actual_FileName" + '"' + ": " + '"' + PublicConst.package_name +".tar.gz" + '"' + "," + NL;
                    json_formated = json_formated + "    " + '"' + "Archive_FileName" + '"' + ": " + '"' + "block7100" + '"' + "," + NL;
                    json_formated = json_formated + "    " + '"' + "CheckSum" + '"' + ": " + '"' + str_hash1 + '"' + "," + NL;
                    json_formated = json_formated + "    " + '"' + "Version" + '"' + ": " + '"' + PublicConst.Car_version + '"' + "," + NL;
                    json_formated = json_formated + "    " + '"' + "DataSet" + '"' + ": "  + "true"  + "," + NL;
                    json_formated = json_formated + "    " + '"' + "DataSetName" + '"' + ": " + '"' +PublicConst.Car_Datensatzname + '"' + NL;
                    json_formated = json_formated + "}" + NL;

            //{
            //    "Actual_FileName": "V1000_LOAU651EXAMP.tar.gz",
            //    "Archive_FileName": "block7100",
            //    "CheckSum": "84085ED5D52A14C9DDBDAD9B6C32CE0DFD7999AC",
            //    "Version": "1000",
            //    "DataSet": true,
            //    "DataSetName": "LOAU651INAMP"
            //} 

            try
            {
                string block7100InfoFile = Path.Combine(PublicConst.Project_Dir, PublicConst.Package_subdir, PublicConst.odx_block7100);
                //PublicConst.odx_block7100;
                using (var writer = new System.IO.StreamWriter(block7100InfoFile))
                {
                    writer.Write(json_formated);
                }
            }
            catch (Exception ex)
            {
            }

            string tarArchiveName = Path.Combine(PublicConst.Project_Dir, PublicConst.Package_subdir, PublicConst.package_name + "_block7100" + ".tar.gz");
            if (System.IO.File.Exists(tarArchiveName))
            {
                System.IO.File.Delete(tarArchiveName);
            }

            string sourceDirectory = Path.Combine(PublicConst.Project_Dir, PublicConst.Package_subdir);

            Stream outStream = System.IO.File.Create(tarArchiveName);

            Stream gzoStream = new GZipOutputStream(outStream);
            TarArchive tarArchive = TarArchive.CreateOutputTarArchive(gzoStream);
            try
            {
                // Note that the RootPath is currently case sensitive and must be forward slashes e.g. "c:/temp"
            // and must not end with a slash, otherwise cuts off first char of filename
                // This is scheduled for fix in next release
                tarArchive.RootPath = sourceDirectory.Replace('\\', '/');
                if (tarArchive.RootPath.EndsWith("/"))
                    tarArchive.RootPath = tarArchive.RootPath.Remove(tarArchive.RootPath.Length - 1);

                //AddDirectoryFilesToTar(tarArchive, sourceDirectory, false);  //alle Dateien i, Verzeichnis, recursive = false
                AddBlock7100FileListToTar(tarArchive, sourceDirectory);  // Liste von Dateien

                tarArchive.Close();
                tb_AudioPackage.Text = Path.GetFileName(tarArchiveName);
            }

            catch (Exception ex)
            {
                tarArchive.Close();
                MessageBox.Show("Warning: Package is corrupt!: " + ex);
            }
        }

        private void AddBlock7100FileListToTar(TarArchive tarArchive, string destPath)
        {
            TarEntry tarEntry = TarEntry.CreateEntryFromFile(destPath);
            string[] filenames = new string[10];
            filenames[0] = Path.Combine(PublicConst.Project_Dir, PublicConst.Package_subdir, PublicConst.package_name + ".tar.gz");  //audio package
            filenames[1] = Path.Combine(PublicConst.Project_Dir, PublicConst.Package_subdir, PublicConst.odx_block7100);    //EQ eCall Speaker
            
            //foreach string
            string filename = "";
            for (int i = 0; i < 2; i++)
            {
                filename = filenames[i];
                tarEntry = TarEntry.CreateEntryFromFile(filename);
                tarArchive.WriteEntry(tarEntry, true);
            }
        }

        private void btnGetAudioPackage_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Title = "Select a Package";
            openFileDialog1.Filter = "Tar-File|*.tar.gz";
            string vpfad = Path.Combine(PublicConst.Project_Dir, PublicConst.Package_subdir);
                if (Directory.Exists(vpfad))
                {
                    openFileDialog1.InitialDirectory = vpfad;
                }
                else
                {
                    MessageBox.Show("Kein Package Verzeichnis gefunden!");
                    return;
                }

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                string thelp = openFileDialog1.FileName;

                //set package name
                tb_AudioPackage.Text = Path.GetFileName(thelp);

                //string thelp11 = Path.GetFileName(tarArchiveName);
                string thelp11 = Path.GetFileName(thelp);
                thelp11 = Path.GetFileNameWithoutExtension(thelp11);
                PublicConst.package_name = Path.GetFileNameWithoutExtension(thelp11);

                lbl_AudioPackageDate.Text = (System.IO.File.GetLastWriteTime(System.IO.Path.Combine(PublicConst.Project_Dir, PublicConst.Package_subdir, PublicConst.package_name + ".tar.gz"))).ToString();

                // update in Config-Box
                if (FrmCBoxConfig.Visible)
                {
                    FrmCBoxConfig.Activate();
                }
                this.Activate();
            
            
            
            
            }

        }

        private void mergePackagesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pnlMerge.Visible = true;
        }

        private void btnMergeFinish_Click(object sender, EventArgs e)
        {

            pnlMerge.Visible = false;
        }

        //===================================================================================================
        //  CBox2020 

        private void tb_CMICext_TextChanged(object sender, EventArgs e)
        {
            PublicConst.NAD_CMICext = tb_CMICext.Text;
        }

        private void tb_CLVLext_TextChanged(object sender, EventArgs e)
        {
            PublicConst.NAD_CLVLext = tb_CLVLext.Text;
        }

        private void cbByPassExt_CheckedChanged(object sender, EventArgs e)
        {
            if (cbByPassExt.Checked)
                { PublicConst.NAD_ByPassExtActive = "1"; }
            else { PublicConst.NAD_ByPassExtActive = "0"; }
        }


        private void setCBoxType(int typ) {
            if(typ == 0) { setCBoxLow(); }
            if (typ == 1) { setCBox2020(); }
        }

        private void setCBox2020()
        {
            lbl_CMICext.Visible = true;
            tb_CMICext.Visible = true;
            lbl_CLVLext.Visible = true;
            tb_CLVLext.Visible = true;
            cbByPassExt.Visible = true;
            lbl_CMIC.Text = "CMIC int";
            lbl_CLVL.Text = "CLVL int";
            PublicConst.CBoxType = "1"; //Cbox2020
            if (PublicConst.NAD_ByPassExtActive=="1") { cbByPassExt.Checked = true; } else { cbByPassExt.Checked = false; }
        }

        private void setCBoxLow()
        {
            lbl_CMICext.Visible = false;
            tb_CMICext.Visible = false;
            lbl_CLVLext.Visible = false;
            tb_CLVLext.Visible = false;
            cbByPassExt.Visible = false;
            lbl_CMIC.Text = "CMIC";
            lbl_CLVL.Text = "CLVL";
            PublicConst.CBoxType = "0"; //CboxLow
            if (PublicConst.NAD_ByPassExtActive == "1") { cbByPassExt.Checked = true; } else { cbByPassExt.Checked = false; }
        }

        private void rBtnCBox2020_CheckedChanged(object sender, EventArgs e)
        {
            if (rBtnCBox2020.Checked==true) { setCBox2020(); }
        }

        private void rBtnCBoxLow_CheckedChanged(object sender, EventArgs e)
        {
            if (rBtnCBoxLow.Checked == true) { setCBoxLow(); }
        }

        private void setRBtnType(string typ)
        {
            if (typ == "0") { rBtnCBoxLow.Checked = true; }
            if (typ == "1") { rBtnCBox2020.Checked = true; }
        }

    }
}
