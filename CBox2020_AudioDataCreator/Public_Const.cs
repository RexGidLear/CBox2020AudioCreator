﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Windows.Forms;
using System.IO;



namespace Public_Const
{
    
    public class PublicConst
    {
       
        // Konstanten
        static public string NL = System.Environment.NewLine;
        static public string AppName = "CBox2020_AudioDataCreator";
        static public string AppVersion = "E01.00";
        static public string Config_file = "CBox_config.xml";
        static public string Config_version = "01.10";
        static public int expert_modus = 0;  //1= extended
        static public int FrmWidthMin = 830;  //Fenstergröße
        static public int FrmHeightMin = 650;  //Fenstergröße
        //Infos
        static public string textRevisiondate = "25.08.2020";
        static public string licences = "SharpZipLib (NuGet Package)" +NL  +"AndroidLib (@xda-developrs by regaw_leinad)";
        static public string textCopyright = "(@) Copyright Lear Corporation 2020";
        static public string PhoneBook_name = "ConBox-PhoneBook.txt";  

        static public string package_name = "";
        static public string package_separator = "V";        
        
        static public string Lua_sourcePath = "";   //oder "Template";
        static public string Lua_default_file = "audio_3dB_V03_CS9DE3.lua";
        static public string Lua_fileName = Lua_default_file;
        static public string Lua_version = "00.03";
        static public string Lua_CRC;
        static public string NAD_CMIC="6";
        static public string NAD_CLVL="8";
        static public string NAD_CMICext = "6";
        static public string NAD_CLVLext = "8";
        static public string NAD_ByPassExtActive = "1";
        static public string CBoxType = "0";


        static public string eCallParamDir = "";
               static public string batch_upload_eCallConfig = "upload_eCallConfig.bat";
        /*       
               static public string batch_upload_ConBoxPackage = "upload_ConBoxPackage.bat";
               static public string batch_upload_EQ_XML="upload_EQ-XML+Lua.bat";
               static public string batch_Reboot = "ConBoxReboot.bat";
               static public string batch_ReadECallTelData = "ShowTelData.bat";
        */
        static public string cbox_PathECallData = "/data/ecall/";
        static public string cboxLow_PathECallData = "/data/ecall/";
        static public string cbox2020_PathECallData = "/data/ecall/";

        static public string Project_Dir = "";
        static public string Project_FileOrg = "audio_config.json";
        static public string Project_File = "audio_config.json";
        static public string Project_textFile = Project_FileOrg; //"audio_config.txt";
        static public string Package_subdir = "Package";
        static public string odx_block7100 = "VersionInfo_block7100.json";

        static public string Car_type = "AUxxx";            //vehicle type
        static public int Car_revision = -1;                //vehicle status 0-Prototyp und Test, 1...9 Serie
        static public string Car_AmpType = "xx";            //car AmpType.  "INAMP", "ESAMP"
        static public int Car_AmpList = -1;                 //car Amp Type
        static public string Tuning_version_int = "00";     //tuning version: pre-tuning: xx.0x, Serie: xx.00
        static public string Tuning_version_ext = "00";     //tuning version: pre-tuning: xx.0x, Serie: xx.00
        static public string Car_version = "0";             //car version: Umbau: 00.xx, Prototyp_first: 01.xx, Prototyp_final: 03xx, Serie1: 11.xx, usw.
        static public string Author_int = "IAV-M.Mustermann";
        static public string Author_ext = "Lear-M.Mustermann";
        static public string Tuning_comment_int = "Tuning A5 NB+WB";
        static public string Tuning_comment_ext = "Line/speaker Out set to 100%";
        static public string Date_create = "29.11.2017";
        static public string Car_Datensatzname = "xx";
        static public string Car_Datensatzname_Praefix = "LO";
        static public string Car_Datensatzname_Suffix = "";   //"AUDIO"; // bevor der AmpType von Audi eingeführt wurde
        static public string Car_MicNo = "2";
        static public string Car_EQ = "0";


        static public string EQ_xml_intern_Dir="";
        static public string cb_SourceDirInt_v_checked="";
        static public string EQ_xml_intern_OrgName;
        static public string EQ_xml_intern_Hash;
        static public string EQ_xml_intern_NewName;

        static public string EQ_xml_extern_Dir="";
        static public string cb_SourceDirExt_v_checked = "";
        static public string EQ_xml_extern_OrgName;
        static public string EQ_xml_extern_Hash;
        static public string EQ_xml_extern_NewName;

        static public string EQ_xml_HashStart_Default ="_CS";
        static public string EQ_xml_HashStart;
        static public string EQ_xml_suffix = ".xml";

        static public string CMD_readText = "";

        // NAD directories
        static public string dirNADaudioFiles = "/data/audio/";
        static public string dirNADeCallFiles = "/data/ecall/";
        static public string dirNADdefaultFiles = "/system/eso/telephone/default_audio/";



        //Pool 
        /*
        static public int seriell_Comport;
        static public byte[] a2b_dataout = new byte[65535];
        static public bool timeOut_Event;
        static public List<string> LiBoI2CAdapterList = new List<string> { };
            */
    }
    
    
    //public class JsonValueCBox
    //{


    //        // JSON Tuning Config Values, Keys in der Klasse unten
    //        static public string json_val_car_type = PublicConst.Car_type;            //vehicle type
    //        static public string json_val_car_version = PublicConst.Car_version;        //car version: Umbau: 00.xx, Prototyp_first: 01.xx, Prototyp_final: 03xx, Serie1: 11.xx, usw.
    //        static public string json_val_tuning_version_int = PublicConst.Tuning_version_int;   //tuning version: pre-tuning: xx.0x, Serie: xx.00
    //        static public string json_val_tuning_version_ext = PublicConst.Tuning_version_ext;   //tuning version: pre-tuning: xx.0x, Serie: xx.00
    //        static public string json_val_author_intern = PublicConst.Author_int; //Ersteller
    //        static public string json_val_author_extern = PublicConst.Author_ext;  //Ersteller
    //        static public string json_val_comment_extern = PublicConst.Tuning_comment_int;   //Kommentar extern
    //        static public string json_val_comment_intern = PublicConst.Tuning_comment_ext; //Kommentar intern
    //        static public string json_val_created = PublicConst.Date_create;       //Erstell-Datum;
        
    //        // JSON File Config Data
    //        static public string json_val_lua_file = PublicConst.Lua_fileName;
    //        static public string json_val_lua_version = PublicConst.Lua_version;
    //        static public string json_val_lua_crc = PublicConst.Lua_CRC;
    //        static public string json_val_audio_EQ_int = PublicConst.EQ_xml_intern_NewName; //EQ internes Tuning
    //        static public string json_val_audio_EQ_int_crc = PublicConst.EQ_xml_intern_Hash;
    //        static public string json_val_audio_EQ_ext = PublicConst.EQ_xml_extern_NewName; //EQ externes Tuning
    //        static public string json_val_audio_EQ_ext_crc = PublicConst.EQ_xml_extern_Hash;
    //        static public string json_val_NAD_CMIC = PublicConst.NAD_CMIC;   //NAD microphone gain
    //        static public string json_val_NAD_CLVL = PublicConst.NAD_CLVL;   //NAD telephone volume level gain
    //        //static public string json_val_ = "";
        
    //}


    // JSON Tuning Config Data
    [Serializable]
    public class JsonKeysCBox
    {
        //Definition aller Keys
        public string package_name { set; get; }
        public string car_DataSetName { set; get; }
        public string car_version { set; get; }
        public string created { set; get; }
        public string car_type { set; get; }
        public string car_revision { set; get; }
        public string car_AmpList { set; get; }
        public string tuning_version_int { set; get; }
        public string tuning_version_ext { set; get; }
        public string author_intern { set; get; }
        public string author_extern { set; get; }
        public string comment_extern { set; get; }
        public string comment_intern { set; get; }
        public string lua_file { set; get; }
        public string lua_crc { set; get; }
        public string audio_EQ_int { set; get; }
        public string audio_EQ_int_crc { set; get; }
        public string audio_EQ_ext { set; get; }
        public string audio_EQ_ext_crc { set; get; }
        public int NAD_CMIC { set; get; }
        public int NAD_CLVL { set; get; }
        //CBox2020
        public int NAD_CMICext { set; get; }
        public int NAD_CLVLext { set; get; }
        public int NAD_ByPass { set; get; }
        public int CBoxType { set; get; }
    }


    public class Configuration
    {
        public int Csave()
        { 
            int Err=0;
            try
            {
                //Xml-Configuration-File Schreiben

                XmlDocument XD = new XmlDocument();
                XmlElement root = XD.CreateElement("Config");

                if (PublicConst.Config_version == "") {PublicConst.Config_version="00.01";}
                XmlElement xVersion = XD.CreateElement("Version"); xVersion.InnerText =PublicConst.Config_version;
                    
                    //xVersion.InnerText = NumFileVersion.Value.ToString("0");
                root.AppendChild(xVersion);

                XmlElement xNode_ext = XD.CreateElement("External");
                   XmlElement xEl001 = XD.CreateElement("Dir"); xEl001.InnerText = PublicConst.EQ_xml_extern_Dir;
                   XmlElement xEl002 = XD.CreateElement("OrgName"); xEl002.InnerText = PublicConst.EQ_xml_extern_OrgName;
                   XmlElement xEl003 = XD.CreateElement("NewName"); xEl003.InnerText = PublicConst.EQ_xml_extern_NewName;
                   XmlElement xEl004 = XD.CreateElement("SourcDirExtCb"); xEl004.InnerText = PublicConst.cb_SourceDirExt_v_checked;
       
                   xNode_ext.AppendChild(xEl001);
                   xNode_ext.AppendChild(xEl002);
                   xNode_ext.AppendChild(xEl003);
                   xNode_ext.AppendChild(xEl004);
                   root.AppendChild(xNode_ext);

                XmlElement xNode_int = XD.CreateElement("Internal");
                   XmlElement xEl011 = XD.CreateElement("Dir"); xEl011.InnerText = PublicConst.EQ_xml_intern_Dir;
                   XmlElement xEl012 = XD.CreateElement("OrgName"); xEl012.InnerText = PublicConst.EQ_xml_intern_OrgName;
                   XmlElement xEl013 = XD.CreateElement("NewName"); xEl013.InnerText = PublicConst.EQ_xml_intern_NewName;
                   XmlElement xEl014 = XD.CreateElement("SourcDirIntCb"); xEl014.InnerText = PublicConst.cb_SourceDirInt_v_checked;
       
                   xNode_int.AppendChild(xEl011);
                   xNode_int.AppendChild(xEl012);
                   xNode_int.AppendChild(xEl013);
                   xNode_int.AppendChild(xEl014);
                   root.AppendChild(xNode_int);

                XmlElement xNode_Package = XD.CreateElement("Package");
                  XmlElement xEl021 = XD.CreateElement("CS_Head"); xEl021.InnerText = PublicConst.EQ_xml_HashStart;
                  xNode_Package.AppendChild(xEl021);
                  root.AppendChild(xNode_Package);

                XmlElement xNode_Lua = XD.CreateElement("Project");
                  XmlElement xEl051 = XD.CreateElement("Proj_Dir"); xEl051.InnerText = PublicConst.Project_Dir;
                  XmlElement xEl052 = XD.CreateElement("Proj_File"); xEl052.InnerText = PublicConst.Project_File;
                  XmlElement xEl053 = XD.CreateElement("LUA_File"); xEl053.InnerText = PublicConst.Lua_fileName;
                  XmlElement xEl054 = XD.CreateElement("CMIC"); xEl054.InnerText = PublicConst.NAD_CMIC;
                  XmlElement xEl055 = XD.CreateElement("CLVL"); xEl055.InnerText = PublicConst.NAD_CLVL;
                  XmlElement xEl056 = XD.CreateElement("Car_Type"); xEl056.InnerText = PublicConst.Car_type;
                  XmlElement xEl057 = XD.CreateElement("Car-Version"); xEl057.InnerText = PublicConst.Car_version;
                  XmlElement xEl058 = XD.CreateElement("Car_Revision"); xEl058.InnerText = Convert.ToString(PublicConst.Car_revision);
                  XmlElement xEl059 = XD.CreateElement("Tuning_Version_internal"); xEl059.InnerText = PublicConst.Tuning_version_int;
                  XmlElement xEl060 = XD.CreateElement("Tuning_Version_external"); xEl060.InnerText = PublicConst.Tuning_version_ext;
                  XmlElement xEl061 = XD.CreateElement("Author_Internal"); xEl061.InnerText = PublicConst.Author_int;
                  XmlElement xEl062 = XD.CreateElement("Author_External"); xEl062.InnerText = PublicConst.Author_ext;
                  XmlElement xEl063 = XD.CreateElement("Comment_Internal"); xEl063.InnerText = PublicConst.Tuning_comment_int;
                  XmlElement xEl064 = XD.CreateElement("Comment_External"); xEl064.InnerText = PublicConst.Tuning_comment_ext;
                  XmlElement xEl065 = XD.CreateElement("Car_MicNo"); xEl065.InnerText = PublicConst.Car_MicNo;
                  XmlElement xEl066 = XD.CreateElement("Car_EQ"); xEl066.InnerText = PublicConst.Car_EQ;
                  XmlElement xEl067 = XD.CreateElement("Car_AmpList"); xEl067.InnerText = Convert.ToString(PublicConst.Car_AmpList);
                //CBox2020
                XmlElement xEl068 = XD.CreateElement("CMICext"); xEl068.InnerText = PublicConst.NAD_CMICext;
                XmlElement xEl069 = XD.CreateElement("CLVLext"); xEl069.InnerText = PublicConst.NAD_CLVLext;
                XmlElement xEl070 = XD.CreateElement("ByPass"); xEl070.InnerText = PublicConst.NAD_ByPassExtActive.ToString();
                XmlElement xEl071 = XD.CreateElement("CBoxType"); xEl071.InnerText = PublicConst.CBoxType.ToString();

                xNode_Lua.AppendChild(xEl051);
                  xNode_Lua.AppendChild(xEl052);
                  xNode_Lua.AppendChild(xEl053);
                  xNode_Lua.AppendChild(xEl054);
                  xNode_Lua.AppendChild(xEl055);
                  xNode_Lua.AppendChild(xEl056);
                  xNode_Lua.AppendChild(xEl057);
                  xNode_Lua.AppendChild(xEl058);
                  xNode_Lua.AppendChild(xEl059);
                  xNode_Lua.AppendChild(xEl060);
                  xNode_Lua.AppendChild(xEl061);
                  xNode_Lua.AppendChild(xEl062);
                  xNode_Lua.AppendChild(xEl063);
                  xNode_Lua.AppendChild(xEl064);
                  xNode_Lua.AppendChild(xEl065);
                  xNode_Lua.AppendChild(xEl066);
                  xNode_Lua.AppendChild(xEl067);
                xNode_Lua.AppendChild(xEl068);
                xNode_Lua.AppendChild(xEl069);
                xNode_Lua.AppendChild(xEl070);
                xNode_Lua.AppendChild(xEl071);
                root.AppendChild(xNode_Lua);

                XmlElement xNode_Expand = XD.CreateElement("Expansion");
                  XmlElement xEl100 = XD.CreateElement("eCallData"); xEl100.InnerText = PublicConst.eCallParamDir;
                xNode_Expand.AppendChild(xEl100);
                root.AppendChild(xNode_Expand);


                XD.AppendChild(root);
                XD.Save(PublicConst.Config_file);

            }
            catch (Exception ex)
            {
                Err = 1;
                Console.WriteLine(ex.Message);
                MessageBox.Show(ex.Message+ ", Error while save configuration file");
            }
            return (Err);
        }

        public int Cload()
        {
            int Err = 0;

            try
            {
                //Xml-Configuration-File lesen
                 XDocument xmlDoc = XDocument.Load(PublicConst.Config_file);
                //XDocument xmlDoc = XDocument.Load("JLRSpeakerDefinition.xml");
               
                // get version from file
                var xmlversions = from element in xmlDoc.Descendants("Version")
                                  select element;
                if (xmlversions.Count() > 0)
                {
                    //int iVersion = int.Parse(((XElement)xmlversions.ElementAt(0)).Value);
                    PublicConst.Config_version = ((XElement)xmlversions.ElementAt(0)).Value;
                    
                   // int Ver.Value = iVersion + 1;
                }

                // Get externals from file
                var xmlExternals = from xExtern in xmlDoc.Descendants("External")
                                   select xExtern;
                if (xmlExternals.Count() > 0)
                {
                    foreach (XElement xe in xmlExternals)
                    {
                        PublicConst.EQ_xml_extern_Dir = xe.Element("Dir").Value;
                        PublicConst.EQ_xml_extern_OrgName = xe.Element("OrgName").Value;
                        PublicConst.EQ_xml_extern_NewName = xe.Element("NewName").Value;
                        PublicConst.cb_SourceDirExt_v_checked = xe.Element("SourcDirExtCb").Value;
                    }
                }

                // Get internals from file
                var xmlInternals = from xIntern in xmlDoc.Descendants("Internal")
                                   select xIntern;
                if (xmlExternals.Count() > 0)
                {
                    foreach (XElement xe in xmlInternals)
                    {
                        PublicConst.EQ_xml_intern_Dir = xe.Element("Dir").Value;
                        PublicConst.EQ_xml_intern_OrgName = xe.Element("OrgName").Value;
                        PublicConst.EQ_xml_intern_NewName = xe.Element("NewName").Value;
                        PublicConst.cb_SourceDirInt_v_checked = xe.Element("SourcDirIntCb").Value;
                    }
                }

                // Get data from package
                var xmlPackages = from xPackage in xmlDoc.Descendants("Package")
                                  select xPackage;
                if (xmlExternals.Count() > 0)
                {
                    foreach (XElement xe in xmlPackages)
                    {
                        PublicConst.EQ_xml_HashStart = xe.Element("CS_Head").Value;
                    }
                }


                // Get data from Lua
                var xmlLua = from xLua in xmlDoc.Descendants("Project")
                                  select xLua;
                if (xmlExternals.Count() > 0)
                {
                    foreach (XElement xe in xmlLua)
                    {
                        PublicConst.Project_Dir = xe.Element("Proj_Dir").Value;
                        PublicConst.Project_File = xe.Element("Proj_File").Value;
                        PublicConst.NAD_CMIC = xe.Element("CMIC").Value;
                        PublicConst.NAD_CLVL = xe.Element("CLVL").Value;
                        if (xe.Element("LUA_File").Value != "")
                        {
                            PublicConst.Lua_fileName = xe.Element("LUA_File").Value;
                        }
                        else
                        {
                            PublicConst.Lua_fileName = PublicConst.Lua_default_file;
                        }

                        PublicConst.Car_type = xe.Element("Car_Type").Value;
                        PublicConst.Car_version = xe.Element("Car-Version").Value;
                        PublicConst.Car_revision = Convert.ToInt32(xe.Element("Car_Revision").Value);
                        PublicConst.Tuning_version_int = xe.Element("Tuning_Version_internal").Value;
                        PublicConst.Tuning_version_ext = xe.Element("Tuning_Version_external").Value;
                        PublicConst.Author_int = xe.Element("Author_Internal").Value;
                        PublicConst.Author_ext = xe.Element("Author_External").Value;
                        PublicConst.Tuning_comment_int = xe.Element("Comment_Internal").Value;
                        PublicConst.Tuning_comment_ext = xe.Element("Comment_External").Value;
                        PublicConst.Car_MicNo = xe.Element("Car_MicNo").Value;
                        PublicConst.Car_EQ = xe.Element("Car_EQ").Value;
                        PublicConst.Car_AmpList = Convert.ToInt32(xe.Element("Car_AmpList").Value);

                        //CBox2020
                        PublicConst.NAD_CMICext = xe.Element("CMICext").Value;
                        PublicConst.NAD_CLVLext = xe.Element("CLVLext").Value;
                        PublicConst.NAD_ByPassExtActive = xe.Element("ByPass").Value;
                        PublicConst.CBoxType = xe.Element("CBoxType").Value;
                        
                    }


                    // Get data from expansion
                    var xmlExpansion = from xExpand in xmlDoc.Descendants("Expansion")
                                       select xExpand;
                    if (xmlExternals.Count() > 0)
                    {
                        foreach (XElement xe in xmlExpansion)
                        {
                            PublicConst.eCallParamDir = xe.Element("eCallData").Value;
                        }
                    }


                
                
                }
            }
            catch (Exception ex)
            {
                Err = 1;
                PublicConst.CBoxType = "0"; // eventuell CBoxLow-File
                Console.WriteLine(ex.Message);
                MessageBox.Show(ex.Message+ ", Error while read configuration file");
            }
            return (Err);
        }
    }

}
