﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CBox_Low_Audio_Data_Config
{
    public partial class FormInfo : Form
    {
        public FormInfo()
        {
            InitializeComponent();
            lblInfo1.Text = "Title:  " +Public_Const.PublicConst.AppName;
            lblInfo1.Visible = true;

            lblInfoL3.Text = "Version:";
            lblInfoL3.Visible = true;
            lblInfo3.Text = Public_Const.PublicConst.AppVersion;
            lblInfo3.Visible = true;

            lblInfoL4.Text = "Created:";
            lblInfoL4.Visible = true;
            lblInfo4.Text = Public_Const.PublicConst.textRevisiondate;
            lblInfo4.Visible = true;

            lblInfoL5.Text = "Public licences: ";
            lblInfoL5.Visible = true;
            lblInfo5.Text = Public_Const.PublicConst.licences;
            lblInfo5.Visible = true;
            
            lblInfo6.Text = Public_Const.PublicConst.textCopyright;
            lblInfo6.Visible = true;
            
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
