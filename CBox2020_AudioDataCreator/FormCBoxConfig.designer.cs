﻿namespace CBoxADBcontroll
{
    partial class FormCBoxConfig
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnPullPhoneNo = new System.Windows.Forms.Button();
            this.liBoNADserNo = new System.Windows.Forms.ListBox();
            this.liBo_PhnoeNums = new System.Windows.Forms.ListBox();
            this.btnWriteTelNum = new System.Windows.Forms.Button();
            this.teBo_NewTelNum = new System.Windows.Forms.TextBox();
            this.btn_NAD_Reboot = new System.Windows.Forms.Button();
            this.bgw_Thread_Reboot = new System.ComponentModel.BackgroundWorker();
            this.lblNewPhoneNumber = new System.Windows.Forms.Label();
            this.lblAppStatus = new System.Windows.Forms.Label();
            this.pnlStatusLine = new System.Windows.Forms.Panel();
            this.pnlStatusMid = new System.Windows.Forms.Panel();
            this.pnlStatusRight = new System.Windows.Forms.Panel();
            this.btn_Close = new System.Windows.Forms.Button();
            this.pnlStatusLeft = new System.Windows.Forms.Panel();
            this.lblADBstatus = new System.Windows.Forms.Label();
            this.pnlPhoneNumber = new System.Windows.Forms.Panel();
            this.btnPullAudioDir = new System.Windows.Forms.Button();
            this.btnCopyList = new System.Windows.Forms.Button();
            this.btnPhoneBook = new System.Windows.Forms.Button();
            this.btnWriteOrgTelNum = new System.Windows.Forms.Button();
            this.pageSetupDialog1 = new System.Windows.Forms.PageSetupDialog();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblADBtitel = new System.Windows.Forms.Label();
            this.cbADBautoCon = new System.Windows.Forms.CheckBox();
            this.btnADBreconnect = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.raBu_LineOut = new System.Windows.Forms.RadioButton();
            this.raBu_eCallSpk = new System.Windows.Forms.RadioButton();
            this.btn_readOutConf = new System.Windows.Forms.Button();
            this.btn_writeOutConf = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lblPackDownDate = new System.Windows.Forms.Label();
            this.lblPackDownD = new System.Windows.Forms.Label();
            this.lblPackDownFile = new System.Windows.Forms.Label();
            this.lblPackDownN = new System.Windows.Forms.Label();
            this.btnPackDownload = new System.Windows.Forms.Button();
            this.bgw_AndoidUpdateDevice = new System.ComponentModel.BackgroundWorker();
            this.bgw_Thread_Reconnect = new System.ComponentModel.BackgroundWorker();
            this.pnlStatusLine.SuspendLayout();
            this.pnlStatusRight.SuspendLayout();
            this.pnlStatusLeft.SuspendLayout();
            this.pnlPhoneNumber.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnPullPhoneNo
            // 
            this.btnPullPhoneNo.Location = new System.Drawing.Point(192, 13);
            this.btnPullPhoneNo.Name = "btnPullPhoneNo";
            this.btnPullPhoneNo.Size = new System.Drawing.Size(136, 23);
            this.btnPullPhoneNo.TabIndex = 5;
            this.btnPullPhoneNo.Text = "Read Phone Numbers";
            this.btnPullPhoneNo.UseVisualStyleBackColor = true;
            this.btnPullPhoneNo.Click += new System.EventHandler(this.btnPullFile_Click_1);
            // 
            // liBoNADserNo
            // 
            this.liBoNADserNo.FormattingEnabled = true;
            this.liBoNADserNo.Location = new System.Drawing.Point(12, 27);
            this.liBoNADserNo.Name = "liBoNADserNo";
            this.liBoNADserNo.Size = new System.Drawing.Size(160, 43);
            this.liBoNADserNo.TabIndex = 6;
            // 
            // liBo_PhnoeNums
            // 
            this.liBo_PhnoeNums.FormattingEnabled = true;
            this.liBo_PhnoeNums.HorizontalScrollbar = true;
            this.liBo_PhnoeNums.Location = new System.Drawing.Point(14, 41);
            this.liBo_PhnoeNums.Name = "liBo_PhnoeNums";
            this.liBo_PhnoeNums.Size = new System.Drawing.Size(416, 121);
            this.liBo_PhnoeNums.TabIndex = 6;
            // 
            // btnWriteTelNum
            // 
            this.btnWriteTelNum.Location = new System.Drawing.Point(192, 173);
            this.btnWriteTelNum.Name = "btnWriteTelNum";
            this.btnWriteTelNum.Size = new System.Drawing.Size(92, 37);
            this.btnWriteTelNum.TabIndex = 5;
            this.btnWriteTelNum.Text = "Write New Phone Number";
            this.btnWriteTelNum.UseVisualStyleBackColor = true;
            this.btnWriteTelNum.Click += new System.EventHandler(this.btnWriteTelNum_Click);
            // 
            // teBo_NewTelNum
            // 
            this.teBo_NewTelNum.Location = new System.Drawing.Point(17, 189);
            this.teBo_NewTelNum.Name = "teBo_NewTelNum";
            this.teBo_NewTelNum.Size = new System.Drawing.Size(169, 20);
            this.teBo_NewTelNum.TabIndex = 4;
            this.teBo_NewTelNum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btn_NAD_Reboot
            // 
            this.btn_NAD_Reboot.Location = new System.Drawing.Point(637, 175);
            this.btn_NAD_Reboot.Name = "btn_NAD_Reboot";
            this.btn_NAD_Reboot.Size = new System.Drawing.Size(126, 36);
            this.btn_NAD_Reboot.TabIndex = 5;
            this.btn_NAD_Reboot.Text = "Reboot";
            this.btn_NAD_Reboot.UseVisualStyleBackColor = true;
            this.btn_NAD_Reboot.Click += new System.EventHandler(this.btn_NAD_Reboot_Click);
            // 
            // bgw_Thread_Reboot
            // 
            this.bgw_Thread_Reboot.WorkerReportsProgress = true;
            this.bgw_Thread_Reboot.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgw_Thread_Reboot_DoWork);
            this.bgw_Thread_Reboot.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bgw_Thread_Reboot_ProgressChanged);
            this.bgw_Thread_Reboot.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgw_Thread_Reboot_RunWorkerCompleted);
            // 
            // lblNewPhoneNumber
            // 
            this.lblNewPhoneNumber.AutoSize = true;
            this.lblNewPhoneNumber.Location = new System.Drawing.Point(14, 173);
            this.lblNewPhoneNumber.Name = "lblNewPhoneNumber";
            this.lblNewPhoneNumber.Size = new System.Drawing.Size(106, 13);
            this.lblNewPhoneNumber.TabIndex = 7;
            this.lblNewPhoneNumber.Text = "New Phone Number:";
            // 
            // lblAppStatus
            // 
            this.lblAppStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblAppStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAppStatus.Location = new System.Drawing.Point(184, 0);
            this.lblAppStatus.Name = "lblAppStatus";
            this.lblAppStatus.Size = new System.Drawing.Size(437, 44);
            this.lblAppStatus.TabIndex = 7;
            this.lblAppStatus.Text = "?";
            this.lblAppStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnlStatusLine
            // 
            this.pnlStatusLine.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlStatusLine.Controls.Add(this.lblAppStatus);
            this.pnlStatusLine.Controls.Add(this.pnlStatusMid);
            this.pnlStatusLine.Controls.Add(this.pnlStatusRight);
            this.pnlStatusLine.Controls.Add(this.pnlStatusLeft);
            this.pnlStatusLine.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlStatusLine.Location = new System.Drawing.Point(0, 223);
            this.pnlStatusLine.Name = "pnlStatusLine";
            this.pnlStatusLine.Size = new System.Drawing.Size(776, 46);
            this.pnlStatusLine.TabIndex = 8;
            // 
            // pnlStatusMid
            // 
            this.pnlStatusMid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlStatusMid.Location = new System.Drawing.Point(184, 0);
            this.pnlStatusMid.Name = "pnlStatusMid";
            this.pnlStatusMid.Size = new System.Drawing.Size(437, 44);
            this.pnlStatusMid.TabIndex = 2;
            // 
            // pnlStatusRight
            // 
            this.pnlStatusRight.Controls.Add(this.btn_Close);
            this.pnlStatusRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnlStatusRight.Location = new System.Drawing.Point(621, 0);
            this.pnlStatusRight.Name = "pnlStatusRight";
            this.pnlStatusRight.Size = new System.Drawing.Size(153, 44);
            this.pnlStatusRight.TabIndex = 1;
            // 
            // btn_Close
            // 
            this.btn_Close.Location = new System.Drawing.Point(30, 11);
            this.btn_Close.Name = "btn_Close";
            this.btn_Close.Size = new System.Drawing.Size(91, 28);
            this.btn_Close.TabIndex = 5;
            this.btn_Close.Text = "Close";
            this.btn_Close.UseVisualStyleBackColor = true;
            this.btn_Close.Click += new System.EventHandler(this.btn_Close_Click);
            // 
            // pnlStatusLeft
            // 
            this.pnlStatusLeft.Controls.Add(this.lblADBstatus);
            this.pnlStatusLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlStatusLeft.Location = new System.Drawing.Point(0, 0);
            this.pnlStatusLeft.Name = "pnlStatusLeft";
            this.pnlStatusLeft.Size = new System.Drawing.Size(184, 44);
            this.pnlStatusLeft.TabIndex = 0;
            // 
            // lblADBstatus
            // 
            this.lblADBstatus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.lblADBstatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblADBstatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblADBstatus.Location = new System.Drawing.Point(0, 0);
            this.lblADBstatus.Name = "lblADBstatus";
            this.lblADBstatus.Size = new System.Drawing.Size(184, 44);
            this.lblADBstatus.TabIndex = 0;
            this.lblADBstatus.Text = "0";
            this.lblADBstatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnlPhoneNumber
            // 
            this.pnlPhoneNumber.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlPhoneNumber.Controls.Add(this.liBo_PhnoeNums);
            this.pnlPhoneNumber.Controls.Add(this.teBo_NewTelNum);
            this.pnlPhoneNumber.Controls.Add(this.lblNewPhoneNumber);
            this.pnlPhoneNumber.Controls.Add(this.btnPullAudioDir);
            this.pnlPhoneNumber.Controls.Add(this.btnCopyList);
            this.pnlPhoneNumber.Controls.Add(this.btnPullPhoneNo);
            this.pnlPhoneNumber.Controls.Add(this.btnPhoneBook);
            this.pnlPhoneNumber.Controls.Add(this.btnWriteOrgTelNum);
            this.pnlPhoneNumber.Controls.Add(this.btnWriteTelNum);
            this.pnlPhoneNumber.Location = new System.Drawing.Point(184, 0);
            this.pnlPhoneNumber.Name = "pnlPhoneNumber";
            this.pnlPhoneNumber.Size = new System.Drawing.Size(438, 224);
            this.pnlPhoneNumber.TabIndex = 9;
            // 
            // btnPullAudioDir
            // 
            this.btnPullAudioDir.Location = new System.Drawing.Point(17, 13);
            this.btnPullAudioDir.Name = "btnPullAudioDir";
            this.btnPullAudioDir.Size = new System.Drawing.Size(136, 23);
            this.btnPullAudioDir.TabIndex = 5;
            this.btnPullAudioDir.Text = "Read Audio Data";
            this.btnPullAudioDir.UseVisualStyleBackColor = true;
            this.btnPullAudioDir.Click += new System.EventHandler(this.btnPullAudioDir_Click);
            // 
            // btnCopyList
            // 
            this.btnCopyList.Location = new System.Drawing.Point(361, 13);
            this.btnCopyList.Name = "btnCopyList";
            this.btnCopyList.Size = new System.Drawing.Size(63, 23);
            this.btnCopyList.TabIndex = 5;
            this.btnCopyList.Text = "Copy";
            this.btnCopyList.UseVisualStyleBackColor = true;
            this.btnCopyList.Click += new System.EventHandler(this.btnCopyList_Click);
            // 
            // btnPhoneBook
            // 
            this.btnPhoneBook.Location = new System.Drawing.Point(361, 172);
            this.btnPhoneBook.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.btnPhoneBook.Name = "btnPhoneBook";
            this.btnPhoneBook.Size = new System.Drawing.Size(63, 37);
            this.btnPhoneBook.TabIndex = 5;
            this.btnPhoneBook.Text = "Phone Book";
            this.btnPhoneBook.UseVisualStyleBackColor = true;
            this.btnPhoneBook.Click += new System.EventHandler(this.btnPhoneBook_Click);
            // 
            // btnWriteOrgTelNum
            // 
            this.btnWriteOrgTelNum.Location = new System.Drawing.Point(292, 173);
            this.btnWriteOrgTelNum.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.btnWriteOrgTelNum.Name = "btnWriteOrgTelNum";
            this.btnWriteOrgTelNum.Size = new System.Drawing.Size(63, 37);
            this.btnWriteOrgTelNum.TabIndex = 5;
            this.btnWriteOrgTelNum.Text = "Write Org Number";
            this.btnWriteOrgTelNum.UseVisualStyleBackColor = true;
            this.btnWriteOrgTelNum.Click += new System.EventHandler(this.btnWriteOrgTelNum_Click);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.lblADBtitel);
            this.panel1.Controls.Add(this.cbADBautoCon);
            this.panel1.Controls.Add(this.btnADBreconnect);
            this.panel1.Controls.Add(this.liBoNADserNo);
            this.panel1.Location = new System.Drawing.Point(0, 92);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(185, 132);
            this.panel1.TabIndex = 10;
            // 
            // lblADBtitel
            // 
            this.lblADBtitel.AutoSize = true;
            this.lblADBtitel.Location = new System.Drawing.Point(12, 6);
            this.lblADBtitel.Name = "lblADBtitel";
            this.lblADBtitel.Size = new System.Drawing.Size(74, 13);
            this.lblADBtitel.TabIndex = 8;
            this.lblADBtitel.Text = "ADB-Interface";
            // 
            // cbADBautoCon
            // 
            this.cbADBautoCon.AutoSize = true;
            this.cbADBautoCon.Checked = true;
            this.cbADBautoCon.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbADBautoCon.Location = new System.Drawing.Point(95, 91);
            this.cbADBautoCon.Name = "cbADBautoCon";
            this.cbADBautoCon.Size = new System.Drawing.Size(85, 17);
            this.cbADBautoCon.TabIndex = 7;
            this.cbADBautoCon.Text = "Auto Search";
            this.cbADBautoCon.UseVisualStyleBackColor = true;
            this.cbADBautoCon.CheckedChanged += new System.EventHandler(this.cbADBautoCon_CheckedChanged);
            // 
            // btnADBreconnect
            // 
            this.btnADBreconnect.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnADBreconnect.Location = new System.Drawing.Point(12, 80);
            this.btnADBreconnect.Name = "btnADBreconnect";
            this.btnADBreconnect.Size = new System.Drawing.Size(70, 37);
            this.btnADBreconnect.TabIndex = 5;
            this.btnADBreconnect.Text = "Reconnect";
            this.btnADBreconnect.UseVisualStyleBackColor = true;
            this.btnADBreconnect.Click += new System.EventHandler(this.btnADBreconnect_Click);
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.raBu_LineOut);
            this.panel2.Controls.Add(this.raBu_eCallSpk);
            this.panel2.Controls.Add(this.btn_readOutConf);
            this.panel2.Controls.Add(this.btn_writeOutConf);
            this.panel2.Location = new System.Drawing.Point(621, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(155, 163);
            this.panel2.TabIndex = 11;
            // 
            // raBu_LineOut
            // 
            this.raBu_LineOut.AutoSize = true;
            this.raBu_LineOut.Location = new System.Drawing.Point(29, 68);
            this.raBu_LineOut.Name = "raBu_LineOut";
            this.raBu_LineOut.Size = new System.Drawing.Size(62, 17);
            this.raBu_LineOut.TabIndex = 6;
            this.raBu_LineOut.TabStop = true;
            this.raBu_LineOut.Text = "LineOut";
            this.raBu_LineOut.UseVisualStyleBackColor = true;
            // 
            // raBu_eCallSpk
            // 
            this.raBu_eCallSpk.AutoSize = true;
            this.raBu_eCallSpk.Location = new System.Drawing.Point(29, 45);
            this.raBu_eCallSpk.Name = "raBu_eCallSpk";
            this.raBu_eCallSpk.Size = new System.Drawing.Size(91, 17);
            this.raBu_eCallSpk.TabIndex = 6;
            this.raBu_eCallSpk.TabStop = true;
            this.raBu_eCallSpk.Text = "eCall-Speaker";
            this.raBu_eCallSpk.UseVisualStyleBackColor = true;
            // 
            // btn_readOutConf
            // 
            this.btn_readOutConf.Location = new System.Drawing.Point(15, 13);
            this.btn_readOutConf.Name = "btn_readOutConf";
            this.btn_readOutConf.Size = new System.Drawing.Size(126, 23);
            this.btn_readOutConf.TabIndex = 5;
            this.btn_readOutConf.Text = "Read Output-Config";
            this.btn_readOutConf.UseVisualStyleBackColor = true;
            this.btn_readOutConf.Click += new System.EventHandler(this.btn_readOutConf_Click);
            // 
            // btn_writeOutConf
            // 
            this.btn_writeOutConf.Location = new System.Drawing.Point(15, 102);
            this.btn_writeOutConf.Name = "btn_writeOutConf";
            this.btn_writeOutConf.Size = new System.Drawing.Size(126, 37);
            this.btn_writeOutConf.TabIndex = 5;
            this.btn_writeOutConf.Text = "Write Output-Config";
            this.btn_writeOutConf.UseVisualStyleBackColor = true;
            this.btn_writeOutConf.Click += new System.EventHandler(this.btn_writeOutConf_Click);
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.lblPackDownDate);
            this.panel3.Controls.Add(this.lblPackDownD);
            this.panel3.Controls.Add(this.lblPackDownFile);
            this.panel3.Controls.Add(this.lblPackDownN);
            this.panel3.Controls.Add(this.btnPackDownload);
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(185, 94);
            this.panel3.TabIndex = 12;
            // 
            // lblPackDownDate
            // 
            this.lblPackDownDate.Location = new System.Drawing.Point(41, 65);
            this.lblPackDownDate.Name = "lblPackDownDate";
            this.lblPackDownDate.Size = new System.Drawing.Size(131, 15);
            this.lblPackDownDate.TabIndex = 1;
            this.lblPackDownDate.Text = "-";
            // 
            // lblPackDownD
            // 
            this.lblPackDownD.AutoSize = true;
            this.lblPackDownD.Location = new System.Drawing.Point(12, 65);
            this.lblPackDownD.Name = "lblPackDownD";
            this.lblPackDownD.Size = new System.Drawing.Size(33, 13);
            this.lblPackDownD.TabIndex = 1;
            this.lblPackDownD.Text = "Date:";
            // 
            // lblPackDownFile
            // 
            this.lblPackDownFile.Location = new System.Drawing.Point(44, 46);
            this.lblPackDownFile.Name = "lblPackDownFile";
            this.lblPackDownFile.Size = new System.Drawing.Size(128, 13);
            this.lblPackDownFile.TabIndex = 1;
            this.lblPackDownFile.Text = "-";
            // 
            // lblPackDownN
            // 
            this.lblPackDownN.AutoSize = true;
            this.lblPackDownN.Location = new System.Drawing.Point(12, 46);
            this.lblPackDownN.Name = "lblPackDownN";
            this.lblPackDownN.Size = new System.Drawing.Size(26, 13);
            this.lblPackDownN.TabIndex = 1;
            this.lblPackDownN.Text = "File:";
            // 
            // btnPackDownload
            // 
            this.btnPackDownload.Location = new System.Drawing.Point(12, 13);
            this.btnPackDownload.Name = "btnPackDownload";
            this.btnPackDownload.Size = new System.Drawing.Size(160, 23);
            this.btnPackDownload.TabIndex = 0;
            this.btnPackDownload.Text = "Package download";
            this.btnPackDownload.UseVisualStyleBackColor = true;
            this.btnPackDownload.Click += new System.EventHandler(this.btnPackDownload_Click);
            // 
            // bgw_AndoidUpdateDevice
            // 
            this.bgw_AndoidUpdateDevice.WorkerReportsProgress = true;
            this.bgw_AndoidUpdateDevice.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgw_AndoidUpdateDevice_DoWork);
            this.bgw_AndoidUpdateDevice.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bgw_AndoidUpdateDevice_ProgressChanged);
            this.bgw_AndoidUpdateDevice.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgw_AndoidUpdateDevice_RunWorkerCompleted);
            // 
            // bgw_Thread_Reconnect
            // 
            this.bgw_Thread_Reconnect.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgw_Thread_Reconnect_DoWork);
            this.bgw_Thread_Reconnect.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgw_Thread_Reconnect_RunWorkerCompleted);
            // 
            // FormCBoxConfig
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(776, 269);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pnlPhoneNumber);
            this.Controls.Add(this.pnlStatusLine);
            this.Controls.Add(this.btn_NAD_Reboot);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "FormCBoxConfig";
            this.Text = "ConBox-Low Configuration";
            this.Activated += new System.EventHandler(this.FormCBoxConfig_Activated);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.pnlStatusLine.ResumeLayout(false);
            this.pnlStatusRight.ResumeLayout(false);
            this.pnlStatusLeft.ResumeLayout(false);
            this.pnlPhoneNumber.ResumeLayout(false);
            this.pnlPhoneNumber.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Button btnPullPhoneNo;
        private System.Windows.Forms.ListBox liBoNADserNo;
        private System.Windows.Forms.ListBox liBo_PhnoeNums;
        internal System.Windows.Forms.Button btnWriteTelNum;
        internal System.Windows.Forms.TextBox teBo_NewTelNum;
        internal System.Windows.Forms.Button btn_NAD_Reboot;
        private System.ComponentModel.BackgroundWorker bgw_Thread_Reboot;
        private System.Windows.Forms.Label lblNewPhoneNumber;
        private System.Windows.Forms.Label lblAppStatus;
        private System.Windows.Forms.Panel pnlStatusLine;
        private System.Windows.Forms.Panel pnlStatusMid;
        private System.Windows.Forms.Panel pnlStatusRight;
        private System.Windows.Forms.Panel pnlStatusLeft;
        private System.Windows.Forms.Panel pnlPhoneNumber;
        private System.Windows.Forms.PageSetupDialog pageSetupDialog1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        internal System.Windows.Forms.Button btn_readOutConf;
        private System.Windows.Forms.RadioButton raBu_LineOut;
        private System.Windows.Forms.RadioButton raBu_eCallSpk;
        internal System.Windows.Forms.Button btn_writeOutConf;
        internal System.Windows.Forms.Button btn_Close;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btnPackDownload;
        private System.Windows.Forms.Label lblPackDownDate;
        private System.Windows.Forms.Label lblPackDownD;
        private System.Windows.Forms.Label lblPackDownFile;
        private System.Windows.Forms.Label lblPackDownN;
        internal System.Windows.Forms.Button btnPullAudioDir;
        internal System.Windows.Forms.Button btnADBreconnect;
        private System.ComponentModel.BackgroundWorker bgw_AndoidUpdateDevice;
        private System.Windows.Forms.Label lblADBstatus;
        private System.Windows.Forms.Label lblADBtitel;
        private System.Windows.Forms.CheckBox cbADBautoCon;
        private System.ComponentModel.BackgroundWorker bgw_Thread_Reconnect;
        internal System.Windows.Forms.Button btnWriteOrgTelNum;
        internal System.Windows.Forms.Button btnPhoneBook;
        internal System.Windows.Forms.Button btnCopyList;
    }
}

