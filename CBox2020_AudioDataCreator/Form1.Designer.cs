﻿namespace CBox_Low_Audio_Data_Config
{
    partial class FrmMain
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.menue_OpenFile = new System.Windows.Forms.ToolStripMenuItem();
            this.menue_NewProject = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.File = new System.Windows.Forms.ToolStripMenuItem();
            this.ConBoxToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.setConBox2020ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.setConBoxLowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.conBoxConfigurationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.preODXToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.mergePackagesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.infoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.versionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pnl_EQ = new System.Windows.Forms.Panel();
            this.gb_ExternalEQ = new System.Windows.Forms.GroupBox();
            this.cb_SourceDirExt = new System.Windows.Forms.CheckBox();
            this.tb_Extern_EQ_Newname = new System.Windows.Forms.TextBox();
            this.lbl_Extern_EQ_Newname = new System.Windows.Forms.Label();
            this.tb_Extern_EQ_HashName = new System.Windows.Forms.TextBox();
            this.tb_Extern_EQ_name = new System.Windows.Forms.TextBox();
            this.lbl_Extern_EQ_HashName = new System.Windows.Forms.Label();
            this.tb_Extern_EQ_Path = new System.Windows.Forms.TextBox();
            this.lbl_Extern_EQ_name = new System.Windows.Forms.Label();
            this.btn_extern_EQ_Dir = new System.Windows.Forms.Button();
            this.lbl_Extern_EQ_Path = new System.Windows.Forms.Label();
            this.gb_InternalEQ = new System.Windows.Forms.GroupBox();
            this.cb_SourceDirInt = new System.Windows.Forms.CheckBox();
            this.tb_Intern_EQ_HashName = new System.Windows.Forms.TextBox();
            this.tb_Intern_EQ_name = new System.Windows.Forms.TextBox();
            this.lbl_Intern_EQ_HashName = new System.Windows.Forms.Label();
            this.tb_Intern_EQ_Newname = new System.Windows.Forms.TextBox();
            this.tb_Intern_EQ_Path = new System.Windows.Forms.TextBox();
            this.lbl_Intern_EQ_Newname = new System.Windows.Forms.Label();
            this.lbl_Intern_EQ_name = new System.Windows.Forms.Label();
            this.btn_Intern_EQ_Dir = new System.Windows.Forms.Button();
            this.lbl_Intern_EQ_Path = new System.Windows.Forms.Label();
            this.pnlCRC = new System.Windows.Forms.Panel();
            this.rb_CRC32 = new System.Windows.Forms.RadioButton();
            this.rb_SHA1 = new System.Windows.Forms.RadioButton();
            this.rb_MD5 = new System.Windows.Forms.RadioButton();
            this.rb_CRC16 = new System.Windows.Forms.RadioButton();
            this.lbl_einstellungen = new System.Windows.Forms.Label();
            this.pnl_lua_config = new System.Windows.Forms.Panel();
            this.gb_AudioControl = new System.Windows.Forms.GroupBox();
            this.gb_Divider = new System.Windows.Forms.GroupBox();
            this.numUD_CarEQ = new System.Windows.Forms.NumericUpDown();
            this.numUD_CarMics = new System.Windows.Forms.NumericUpDown();
            this.tb_ProjectFile = new System.Windows.Forms.TextBox();
            this.tb_ProjectPath = new System.Windows.Forms.TextBox();
            this.lbl_LuaFile = new System.Windows.Forms.Label();
            this.lbl_LuaPath = new System.Windows.Forms.Label();
            this.btn_OpenFile = new System.Windows.Forms.Button();
            this.btn_ImportPackage = new System.Windows.Forms.Button();
            this.btn_NewProject = new System.Windows.Forms.Button();
            this.coB_AmpType = new System.Windows.Forms.ComboBox();
            this.coB_CarRevision = new System.Windows.Forms.ComboBox();
            this.tb_CommentExt = new System.Windows.Forms.TextBox();
            this.tb_CommentInt = new System.Windows.Forms.TextBox();
            this.tb_AuthorExt = new System.Windows.Forms.TextBox();
            this.tb_AuthorInt = new System.Windows.Forms.TextBox();
            this.tb_CarDatensatzname = new System.Windows.Forms.TextBox();
            this.tb_CarVersion = new System.Windows.Forms.TextBox();
            this.tb_TuningVersExt = new System.Windows.Forms.TextBox();
            this.tb_TuningVersInt = new System.Windows.Forms.TextBox();
            this.lbl_CommentInt = new System.Windows.Forms.Label();
            this.lbl_AuthorExt = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.Lbl_AuthorInt = new System.Windows.Forms.Label();
            this.lblAmpType = new System.Windows.Forms.Label();
            this.lb_CarDatensatzname = new System.Windows.Forms.Label();
            this.lb_CarVersion = new System.Windows.Forms.Label();
            this.lb_TuningVersInt = new System.Windows.Forms.Label();
            this.lbl_CarEQ = new System.Windows.Forms.Label();
            this.lbl_CarMics = new System.Windows.Forms.Label();
            this.lbl_CarType = new System.Windows.Forms.Label();
            this.tb_CarAUNo = new System.Windows.Forms.TextBox();
            this.lb_CarAUNo = new System.Windows.Forms.Label();
            this.gb_ConfigVersion = new System.Windows.Forms.GroupBox();
            this.cbByPassExt = new System.Windows.Forms.CheckBox();
            this.lbl_ADI_ConfVersion = new System.Windows.Forms.Label();
            this.btn_LuaSelection = new System.Windows.Forms.Button();
            this.but_ExtDataInfo = new System.Windows.Forms.Button();
            this.lbl_CLVLext = new System.Windows.Forms.Label();
            this.lbl_CLVL = new System.Windows.Forms.Label();
            this.lbl_CMICext = new System.Windows.Forms.Label();
            this.lbl_CMIC = new System.Windows.Forms.Label();
            this.tb_CLVLext = new System.Windows.Forms.TextBox();
            this.tb_CMICext = new System.Windows.Forms.TextBox();
            this.tb_CLVL = new System.Windows.Forms.TextBox();
            this.tb_CMIC = new System.Windows.Forms.TextBox();
            this.tbLuaName = new System.Windows.Forms.TextBox();
            this.tb_lua_crc = new System.Windows.Forms.TextBox();
            this.lbl_LuaName = new System.Windows.Forms.Label();
            this.btn_AudioPackage = new System.Windows.Forms.Button();
            this.tb_AudioPackage = new System.Windows.Forms.TextBox();
            this.lbl_AudioPackageDate = new System.Windows.Forms.Label();
            this.btnGetAudioPackage = new System.Windows.Forms.Button();
            this.pnlMerge = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lblCLVLSpeaker = new System.Windows.Forms.Label();
            this.lblCLVLSoundSys = new System.Windows.Forms.Label();
            this.btnCheckCLVL = new System.Windows.Forms.Button();
            this.tbxCLVLSpeaker = new System.Windows.Forms.TextBox();
            this.btnMergePathSearch = new System.Windows.Forms.Button();
            this.btnCheckLUA = new System.Windows.Forms.Button();
            this.tbxCLVLSoundSys = new System.Windows.Forms.TextBox();
            this.lblPackSpeaker = new System.Windows.Forms.Label();
            this.numUDversion = new System.Windows.Forms.NumericUpDown();
            this.lblPackSoundSystem = new System.Windows.Forms.Label();
            this.btnMergeFinish = new System.Windows.Forms.Button();
            this.tbxPackSoundSystem = new System.Windows.Forms.TextBox();
            this.btnPackSpeaker = new System.Windows.Forms.Button();
            this.tbxPackSpeaker = new System.Windows.Forms.TextBox();
            this.btnMerge = new System.Windows.Forms.Button();
            this.tbxMergeDir = new System.Windows.Forms.TextBox();
            this.btnPackSoundsystem = new System.Windows.Forms.Button();
            this.lblVersion = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblSelectConBox = new System.Windows.Forms.Label();
            this.rBtnCBoxLow = new System.Windows.Forms.RadioButton();
            this.rBtnCBox2020 = new System.Windows.Forms.RadioButton();
            this.menuStrip1.SuspendLayout();
            this.pnl_EQ.SuspendLayout();
            this.gb_ExternalEQ.SuspendLayout();
            this.gb_InternalEQ.SuspendLayout();
            this.pnlCRC.SuspendLayout();
            this.pnl_lua_config.SuspendLayout();
            this.gb_AudioControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_CarEQ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_CarMics)).BeginInit();
            this.gb_ConfigVersion.SuspendLayout();
            this.pnlMerge.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUDversion)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.ConBoxToolStripMenuItem,
            this.preODXToolStripMenuItem,
            this.infoToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(812, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menue_OpenFile,
            this.menue_NewProject,
            this.toolStripSeparator1,
            this.File});
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(37, 20);
            this.toolStripMenuItem1.Text = "File";
            // 
            // menue_OpenFile
            // 
            this.menue_OpenFile.Name = "menue_OpenFile";
            this.menue_OpenFile.Size = new System.Drawing.Size(138, 22);
            this.menue_OpenFile.Text = "Open File";
            this.menue_OpenFile.Click += new System.EventHandler(this.menue_OpenFile_Click);
            // 
            // menue_NewProject
            // 
            this.menue_NewProject.Name = "menue_NewProject";
            this.menue_NewProject.Size = new System.Drawing.Size(138, 22);
            this.menue_NewProject.Text = "New Project";
            this.menue_NewProject.Click += new System.EventHandler(this.menue_NewProject_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(135, 6);
            // 
            // File
            // 
            this.File.Name = "File";
            this.File.Size = new System.Drawing.Size(138, 22);
            this.File.Text = "Close";
            this.File.Click += new System.EventHandler(this.File_Click);
            // 
            // ConBoxToolStripMenuItem
            // 
            this.ConBoxToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.setConBox2020ToolStripMenuItem,
            this.setConBoxLowToolStripMenuItem,
            this.toolStripSeparator3,
            this.conBoxConfigurationToolStripMenuItem});
            this.ConBoxToolStripMenuItem.Name = "ConBoxToolStripMenuItem";
            this.ConBoxToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.ConBoxToolStripMenuItem.Text = "ConBox";
            // 
            // setConBox2020ToolStripMenuItem
            // 
            this.setConBox2020ToolStripMenuItem.Name = "setConBox2020ToolStripMenuItem";
            this.setConBox2020ToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.setConBox2020ToolStripMenuItem.Text = "SetConBox2020";
            // 
            // setConBoxLowToolStripMenuItem
            // 
            this.setConBoxLowToolStripMenuItem.Name = "setConBoxLowToolStripMenuItem";
            this.setConBoxLowToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.setConBoxLowToolStripMenuItem.Text = "SetConBoxLow";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(189, 6);
            // 
            // conBoxConfigurationToolStripMenuItem
            // 
            this.conBoxConfigurationToolStripMenuItem.Name = "conBoxConfigurationToolStripMenuItem";
            this.conBoxConfigurationToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.conBoxConfigurationToolStripMenuItem.Text = "ConBox Configuration";
            this.conBoxConfigurationToolStripMenuItem.Click += new System.EventHandler(this.conBoxConfigurationToolStripMenuItem_Click);
            // 
            // preODXToolStripMenuItem
            // 
            this.preODXToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createToolStripMenuItem,
            this.toolStripSeparator2,
            this.mergePackagesToolStripMenuItem});
            this.preODXToolStripMenuItem.Name = "preODXToolStripMenuItem";
            this.preODXToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
            this.preODXToolStripMenuItem.Text = "Pre-ODX";
            // 
            // createToolStripMenuItem
            // 
            this.createToolStripMenuItem.Name = "createToolStripMenuItem";
            this.createToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.createToolStripMenuItem.Text = "Create Archiv";
            this.createToolStripMenuItem.Click += new System.EventHandler(this.createToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(157, 6);
            // 
            // mergePackagesToolStripMenuItem
            // 
            this.mergePackagesToolStripMenuItem.Enabled = false;
            this.mergePackagesToolStripMenuItem.Name = "mergePackagesToolStripMenuItem";
            this.mergePackagesToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.mergePackagesToolStripMenuItem.Text = "Merge Packages";
            this.mergePackagesToolStripMenuItem.Click += new System.EventHandler(this.mergePackagesToolStripMenuItem_Click);
            // 
            // infoToolStripMenuItem
            // 
            this.infoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.versionToolStripMenuItem});
            this.infoToolStripMenuItem.Name = "infoToolStripMenuItem";
            this.infoToolStripMenuItem.Size = new System.Drawing.Size(40, 20);
            this.infoToolStripMenuItem.Text = "Info";
            // 
            // versionToolStripMenuItem
            // 
            this.versionToolStripMenuItem.Name = "versionToolStripMenuItem";
            this.versionToolStripMenuItem.Size = new System.Drawing.Size(112, 22);
            this.versionToolStripMenuItem.Text = "Version";
            this.versionToolStripMenuItem.Click += new System.EventHandler(this.versionToolStripMenuItem_Click);
            // 
            // pnl_EQ
            // 
            this.pnl_EQ.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnl_EQ.Controls.Add(this.gb_ExternalEQ);
            this.pnl_EQ.Controls.Add(this.gb_InternalEQ);
            this.pnl_EQ.Location = new System.Drawing.Point(14, 341);
            this.pnl_EQ.Name = "pnl_EQ";
            this.pnl_EQ.Size = new System.Drawing.Size(769, 268);
            this.pnl_EQ.TabIndex = 4;
            // 
            // gb_ExternalEQ
            // 
            this.gb_ExternalEQ.Controls.Add(this.cb_SourceDirExt);
            this.gb_ExternalEQ.Controls.Add(this.tb_Extern_EQ_Newname);
            this.gb_ExternalEQ.Controls.Add(this.lbl_Extern_EQ_Newname);
            this.gb_ExternalEQ.Controls.Add(this.tb_Extern_EQ_HashName);
            this.gb_ExternalEQ.Controls.Add(this.tb_Extern_EQ_name);
            this.gb_ExternalEQ.Controls.Add(this.lbl_Extern_EQ_HashName);
            this.gb_ExternalEQ.Controls.Add(this.tb_Extern_EQ_Path);
            this.gb_ExternalEQ.Controls.Add(this.lbl_Extern_EQ_name);
            this.gb_ExternalEQ.Controls.Add(this.btn_extern_EQ_Dir);
            this.gb_ExternalEQ.Controls.Add(this.lbl_Extern_EQ_Path);
            this.gb_ExternalEQ.Location = new System.Drawing.Point(4, 135);
            this.gb_ExternalEQ.Name = "gb_ExternalEQ";
            this.gb_ExternalEQ.Size = new System.Drawing.Size(760, 126);
            this.gb_ExternalEQ.TabIndex = 8;
            this.gb_ExternalEQ.TabStop = false;
            this.gb_ExternalEQ.Text = "  External-EQ (Sound System)  ";
            // 
            // cb_SourceDirExt
            // 
            this.cb_SourceDirExt.AutoSize = true;
            this.cb_SourceDirExt.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cb_SourceDirExt.Location = new System.Drawing.Point(640, 49);
            this.cb_SourceDirExt.Name = "cb_SourceDirExt";
            this.cb_SourceDirExt.Size = new System.Drawing.Size(91, 17);
            this.cb_SourceDirExt.TabIndex = 20;
            this.cb_SourceDirExt.Text = "Last Directory";
            this.cb_SourceDirExt.UseVisualStyleBackColor = true;
            this.cb_SourceDirExt.CheckedChanged += new System.EventHandler(this.cb_SourceDirExt_CheckedChanged);
            // 
            // tb_Extern_EQ_Newname
            // 
            this.tb_Extern_EQ_Newname.Location = new System.Drawing.Point(85, 99);
            this.tb_Extern_EQ_Newname.Name = "tb_Extern_EQ_Newname";
            this.tb_Extern_EQ_Newname.ReadOnly = true;
            this.tb_Extern_EQ_Newname.Size = new System.Drawing.Size(593, 20);
            this.tb_Extern_EQ_Newname.TabIndex = 24;
            this.tb_Extern_EQ_Newname.TabStop = false;
            // 
            // lbl_Extern_EQ_Newname
            // 
            this.lbl_Extern_EQ_Newname.AutoSize = true;
            this.lbl_Extern_EQ_Newname.Location = new System.Drawing.Point(22, 102);
            this.lbl_Extern_EQ_Newname.Name = "lbl_Extern_EQ_Newname";
            this.lbl_Extern_EQ_Newname.Size = new System.Drawing.Size(51, 13);
            this.lbl_Extern_EQ_Newname.TabIndex = 12;
            this.lbl_Extern_EQ_Newname.Text = "New-File:";
            // 
            // tb_Extern_EQ_HashName
            // 
            this.tb_Extern_EQ_HashName.Location = new System.Drawing.Point(85, 73);
            this.tb_Extern_EQ_HashName.Name = "tb_Extern_EQ_HashName";
            this.tb_Extern_EQ_HashName.ReadOnly = true;
            this.tb_Extern_EQ_HashName.Size = new System.Drawing.Size(481, 20);
            this.tb_Extern_EQ_HashName.TabIndex = 23;
            this.tb_Extern_EQ_HashName.TabStop = false;
            // 
            // tb_Extern_EQ_name
            // 
            this.tb_Extern_EQ_name.Enabled = false;
            this.tb_Extern_EQ_name.Location = new System.Drawing.Point(85, 47);
            this.tb_Extern_EQ_name.Name = "tb_Extern_EQ_name";
            this.tb_Extern_EQ_name.Size = new System.Drawing.Size(481, 20);
            this.tb_Extern_EQ_name.TabIndex = 22;
            // 
            // lbl_Extern_EQ_HashName
            // 
            this.lbl_Extern_EQ_HashName.AutoSize = true;
            this.lbl_Extern_EQ_HashName.Location = new System.Drawing.Point(22, 73);
            this.lbl_Extern_EQ_HashName.Name = "lbl_Extern_EQ_HashName";
            this.lbl_Extern_EQ_HashName.Size = new System.Drawing.Size(54, 13);
            this.lbl_Extern_EQ_HashName.TabIndex = 5;
            this.lbl_Extern_EQ_HashName.Text = "Hash-File:";
            // 
            // tb_Extern_EQ_Path
            // 
            this.tb_Extern_EQ_Path.Enabled = false;
            this.tb_Extern_EQ_Path.Location = new System.Drawing.Point(85, 21);
            this.tb_Extern_EQ_Path.Name = "tb_Extern_EQ_Path";
            this.tb_Extern_EQ_Path.Size = new System.Drawing.Size(593, 20);
            this.tb_Extern_EQ_Path.TabIndex = 21;
            // 
            // lbl_Extern_EQ_name
            // 
            this.lbl_Extern_EQ_name.AutoSize = true;
            this.lbl_Extern_EQ_name.Location = new System.Drawing.Point(22, 47);
            this.lbl_Extern_EQ_name.Name = "lbl_Extern_EQ_name";
            this.lbl_Extern_EQ_name.Size = new System.Drawing.Size(26, 13);
            this.lbl_Extern_EQ_name.TabIndex = 6;
            this.lbl_Extern_EQ_name.Text = "File:";
            // 
            // btn_extern_EQ_Dir
            // 
            this.btn_extern_EQ_Dir.Location = new System.Drawing.Point(684, 19);
            this.btn_extern_EQ_Dir.Name = "btn_extern_EQ_Dir";
            this.btn_extern_EQ_Dir.Size = new System.Drawing.Size(62, 23);
            this.btn_extern_EQ_Dir.TabIndex = 20;
            this.btn_extern_EQ_Dir.Text = "Search";
            this.btn_extern_EQ_Dir.UseVisualStyleBackColor = true;
            this.btn_extern_EQ_Dir.Click += new System.EventHandler(this.btn_extern_EQ_Dir_Click);
            // 
            // lbl_Extern_EQ_Path
            // 
            this.lbl_Extern_EQ_Path.AutoSize = true;
            this.lbl_Extern_EQ_Path.Location = new System.Drawing.Point(22, 21);
            this.lbl_Extern_EQ_Path.Name = "lbl_Extern_EQ_Path";
            this.lbl_Extern_EQ_Path.Size = new System.Drawing.Size(32, 13);
            this.lbl_Extern_EQ_Path.TabIndex = 7;
            this.lbl_Extern_EQ_Path.Text = "Path:";
            // 
            // gb_InternalEQ
            // 
            this.gb_InternalEQ.Controls.Add(this.cb_SourceDirInt);
            this.gb_InternalEQ.Controls.Add(this.tb_Intern_EQ_HashName);
            this.gb_InternalEQ.Controls.Add(this.tb_Intern_EQ_name);
            this.gb_InternalEQ.Controls.Add(this.lbl_Intern_EQ_HashName);
            this.gb_InternalEQ.Controls.Add(this.tb_Intern_EQ_Newname);
            this.gb_InternalEQ.Controls.Add(this.tb_Intern_EQ_Path);
            this.gb_InternalEQ.Controls.Add(this.lbl_Intern_EQ_Newname);
            this.gb_InternalEQ.Controls.Add(this.lbl_Intern_EQ_name);
            this.gb_InternalEQ.Controls.Add(this.btn_Intern_EQ_Dir);
            this.gb_InternalEQ.Controls.Add(this.lbl_Intern_EQ_Path);
            this.gb_InternalEQ.Location = new System.Drawing.Point(3, 3);
            this.gb_InternalEQ.Name = "gb_InternalEQ";
            this.gb_InternalEQ.Size = new System.Drawing.Size(760, 126);
            this.gb_InternalEQ.TabIndex = 7;
            this.gb_InternalEQ.TabStop = false;
            this.gb_InternalEQ.Text = "  Internal-EQ (eCall-Speaker)  ";
            // 
            // cb_SourceDirInt
            // 
            this.cb_SourceDirInt.AutoSize = true;
            this.cb_SourceDirInt.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cb_SourceDirInt.Location = new System.Drawing.Point(641, 48);
            this.cb_SourceDirInt.Name = "cb_SourceDirInt";
            this.cb_SourceDirInt.Size = new System.Drawing.Size(91, 17);
            this.cb_SourceDirInt.TabIndex = 20;
            this.cb_SourceDirInt.Text = "Last Directory";
            this.cb_SourceDirInt.UseVisualStyleBackColor = true;
            this.cb_SourceDirInt.CheckedChanged += new System.EventHandler(this.cb_SourceDirInt_CheckedChanged);
            // 
            // tb_Intern_EQ_HashName
            // 
            this.tb_Intern_EQ_HashName.Location = new System.Drawing.Point(85, 72);
            this.tb_Intern_EQ_HashName.Name = "tb_Intern_EQ_HashName";
            this.tb_Intern_EQ_HashName.ReadOnly = true;
            this.tb_Intern_EQ_HashName.Size = new System.Drawing.Size(481, 20);
            this.tb_Intern_EQ_HashName.TabIndex = 18;
            this.tb_Intern_EQ_HashName.TabStop = false;
            // 
            // tb_Intern_EQ_name
            // 
            this.tb_Intern_EQ_name.Enabled = false;
            this.tb_Intern_EQ_name.Location = new System.Drawing.Point(85, 46);
            this.tb_Intern_EQ_name.Name = "tb_Intern_EQ_name";
            this.tb_Intern_EQ_name.Size = new System.Drawing.Size(481, 20);
            this.tb_Intern_EQ_name.TabIndex = 17;
            // 
            // lbl_Intern_EQ_HashName
            // 
            this.lbl_Intern_EQ_HashName.AutoSize = true;
            this.lbl_Intern_EQ_HashName.Location = new System.Drawing.Point(22, 72);
            this.lbl_Intern_EQ_HashName.Name = "lbl_Intern_EQ_HashName";
            this.lbl_Intern_EQ_HashName.Size = new System.Drawing.Size(54, 13);
            this.lbl_Intern_EQ_HashName.TabIndex = 5;
            this.lbl_Intern_EQ_HashName.Text = "Hash-File:";
            // 
            // tb_Intern_EQ_Newname
            // 
            this.tb_Intern_EQ_Newname.Location = new System.Drawing.Point(85, 99);
            this.tb_Intern_EQ_Newname.Name = "tb_Intern_EQ_Newname";
            this.tb_Intern_EQ_Newname.ReadOnly = true;
            this.tb_Intern_EQ_Newname.Size = new System.Drawing.Size(593, 20);
            this.tb_Intern_EQ_Newname.TabIndex = 19;
            this.tb_Intern_EQ_Newname.TabStop = false;
            // 
            // tb_Intern_EQ_Path
            // 
            this.tb_Intern_EQ_Path.Enabled = false;
            this.tb_Intern_EQ_Path.Location = new System.Drawing.Point(85, 20);
            this.tb_Intern_EQ_Path.Name = "tb_Intern_EQ_Path";
            this.tb_Intern_EQ_Path.Size = new System.Drawing.Size(593, 20);
            this.tb_Intern_EQ_Path.TabIndex = 16;
            // 
            // lbl_Intern_EQ_Newname
            // 
            this.lbl_Intern_EQ_Newname.AutoSize = true;
            this.lbl_Intern_EQ_Newname.Location = new System.Drawing.Point(22, 102);
            this.lbl_Intern_EQ_Newname.Name = "lbl_Intern_EQ_Newname";
            this.lbl_Intern_EQ_Newname.Size = new System.Drawing.Size(51, 13);
            this.lbl_Intern_EQ_Newname.TabIndex = 6;
            this.lbl_Intern_EQ_Newname.Text = "New-File:";
            // 
            // lbl_Intern_EQ_name
            // 
            this.lbl_Intern_EQ_name.AutoSize = true;
            this.lbl_Intern_EQ_name.Location = new System.Drawing.Point(22, 46);
            this.lbl_Intern_EQ_name.Name = "lbl_Intern_EQ_name";
            this.lbl_Intern_EQ_name.Size = new System.Drawing.Size(26, 13);
            this.lbl_Intern_EQ_name.TabIndex = 6;
            this.lbl_Intern_EQ_name.Text = "File:";
            // 
            // btn_Intern_EQ_Dir
            // 
            this.btn_Intern_EQ_Dir.Location = new System.Drawing.Point(684, 18);
            this.btn_Intern_EQ_Dir.Name = "btn_Intern_EQ_Dir";
            this.btn_Intern_EQ_Dir.Size = new System.Drawing.Size(62, 23);
            this.btn_Intern_EQ_Dir.TabIndex = 15;
            this.btn_Intern_EQ_Dir.Text = "Search";
            this.btn_Intern_EQ_Dir.UseVisualStyleBackColor = true;
            this.btn_Intern_EQ_Dir.Click += new System.EventHandler(this.btn_Intern_EQ_Dir_Click);
            // 
            // lbl_Intern_EQ_Path
            // 
            this.lbl_Intern_EQ_Path.AutoSize = true;
            this.lbl_Intern_EQ_Path.Location = new System.Drawing.Point(22, 20);
            this.lbl_Intern_EQ_Path.Name = "lbl_Intern_EQ_Path";
            this.lbl_Intern_EQ_Path.Size = new System.Drawing.Size(32, 13);
            this.lbl_Intern_EQ_Path.TabIndex = 8;
            this.lbl_Intern_EQ_Path.Text = "Path:";
            // 
            // pnlCRC
            // 
            this.pnlCRC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlCRC.Controls.Add(this.rb_CRC32);
            this.pnlCRC.Controls.Add(this.rb_SHA1);
            this.pnlCRC.Controls.Add(this.rb_MD5);
            this.pnlCRC.Controls.Add(this.rb_CRC16);
            this.pnlCRC.Controls.Add(this.lbl_einstellungen);
            this.pnlCRC.Location = new System.Drawing.Point(477, 612);
            this.pnlCRC.Margin = new System.Windows.Forms.Padding(0);
            this.pnlCRC.Name = "pnlCRC";
            this.pnlCRC.Size = new System.Drawing.Size(306, 20);
            this.pnlCRC.TabIndex = 6;
            this.pnlCRC.Visible = false;
            // 
            // rb_CRC32
            // 
            this.rb_CRC32.AutoSize = true;
            this.rb_CRC32.Location = new System.Drawing.Point(136, 3);
            this.rb_CRC32.Margin = new System.Windows.Forms.Padding(3, 0, 0, 3);
            this.rb_CRC32.Name = "rb_CRC32";
            this.rb_CRC32.Size = new System.Drawing.Size(59, 17);
            this.rb_CRC32.TabIndex = 0;
            this.rb_CRC32.Text = "CRC32";
            this.rb_CRC32.UseVisualStyleBackColor = true;
            this.rb_CRC32.CheckedChanged += new System.EventHandler(this.rb_CRC16_CheckedChanged);
            // 
            // rb_SHA1
            // 
            this.rb_SHA1.AutoSize = true;
            this.rb_SHA1.Location = new System.Drawing.Point(255, 4);
            this.rb_SHA1.Margin = new System.Windows.Forms.Padding(3, 0, 0, 3);
            this.rb_SHA1.Name = "rb_SHA1";
            this.rb_SHA1.Size = new System.Drawing.Size(53, 17);
            this.rb_SHA1.TabIndex = 0;
            this.rb_SHA1.Text = "SHA1";
            this.rb_SHA1.UseVisualStyleBackColor = true;
            this.rb_SHA1.CheckedChanged += new System.EventHandler(this.rb_CRC16_CheckedChanged);
            // 
            // rb_MD5
            // 
            this.rb_MD5.AutoSize = true;
            this.rb_MD5.Location = new System.Drawing.Point(201, 3);
            this.rb_MD5.Margin = new System.Windows.Forms.Padding(3, 0, 0, 3);
            this.rb_MD5.Name = "rb_MD5";
            this.rb_MD5.Size = new System.Drawing.Size(48, 17);
            this.rb_MD5.TabIndex = 0;
            this.rb_MD5.Text = "MD5";
            this.rb_MD5.UseVisualStyleBackColor = true;
            this.rb_MD5.CheckedChanged += new System.EventHandler(this.rb_CRC16_CheckedChanged);
            // 
            // rb_CRC16
            // 
            this.rb_CRC16.AutoSize = true;
            this.rb_CRC16.Checked = true;
            this.rb_CRC16.Location = new System.Drawing.Point(71, 3);
            this.rb_CRC16.Margin = new System.Windows.Forms.Padding(3, 0, 0, 3);
            this.rb_CRC16.Name = "rb_CRC16";
            this.rb_CRC16.Size = new System.Drawing.Size(59, 17);
            this.rb_CRC16.TabIndex = 0;
            this.rb_CRC16.TabStop = true;
            this.rb_CRC16.Text = "CRC16";
            this.rb_CRC16.UseVisualStyleBackColor = true;
            this.rb_CRC16.CheckedChanged += new System.EventHandler(this.rb_CRC16_CheckedChanged);
            // 
            // lbl_einstellungen
            // 
            this.lbl_einstellungen.AutoSize = true;
            this.lbl_einstellungen.Location = new System.Drawing.Point(3, 3);
            this.lbl_einstellungen.Name = "lbl_einstellungen";
            this.lbl_einstellungen.Size = new System.Drawing.Size(62, 13);
            this.lbl_einstellungen.TabIndex = 2;
            this.lbl_einstellungen.Text = "Hash-Type:";
            // 
            // pnl_lua_config
            // 
            this.pnl_lua_config.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnl_lua_config.Controls.Add(this.gb_AudioControl);
            this.pnl_lua_config.Controls.Add(this.gb_ConfigVersion);
            this.pnl_lua_config.Location = new System.Drawing.Point(14, 60);
            this.pnl_lua_config.Name = "pnl_lua_config";
            this.pnl_lua_config.Size = new System.Drawing.Size(769, 275);
            this.pnl_lua_config.TabIndex = 7;
            // 
            // gb_AudioControl
            // 
            this.gb_AudioControl.Controls.Add(this.gb_Divider);
            this.gb_AudioControl.Controls.Add(this.numUD_CarEQ);
            this.gb_AudioControl.Controls.Add(this.numUD_CarMics);
            this.gb_AudioControl.Controls.Add(this.tb_ProjectFile);
            this.gb_AudioControl.Controls.Add(this.tb_ProjectPath);
            this.gb_AudioControl.Controls.Add(this.lbl_LuaFile);
            this.gb_AudioControl.Controls.Add(this.lbl_LuaPath);
            this.gb_AudioControl.Controls.Add(this.btn_OpenFile);
            this.gb_AudioControl.Controls.Add(this.btn_ImportPackage);
            this.gb_AudioControl.Controls.Add(this.btn_NewProject);
            this.gb_AudioControl.Controls.Add(this.coB_AmpType);
            this.gb_AudioControl.Controls.Add(this.coB_CarRevision);
            this.gb_AudioControl.Controls.Add(this.tb_CommentExt);
            this.gb_AudioControl.Controls.Add(this.tb_CommentInt);
            this.gb_AudioControl.Controls.Add(this.tb_AuthorExt);
            this.gb_AudioControl.Controls.Add(this.tb_AuthorInt);
            this.gb_AudioControl.Controls.Add(this.tb_CarDatensatzname);
            this.gb_AudioControl.Controls.Add(this.tb_CarVersion);
            this.gb_AudioControl.Controls.Add(this.tb_TuningVersExt);
            this.gb_AudioControl.Controls.Add(this.tb_TuningVersInt);
            this.gb_AudioControl.Controls.Add(this.lbl_CommentInt);
            this.gb_AudioControl.Controls.Add(this.lbl_AuthorExt);
            this.gb_AudioControl.Controls.Add(this.label1);
            this.gb_AudioControl.Controls.Add(this.Lbl_AuthorInt);
            this.gb_AudioControl.Controls.Add(this.lblAmpType);
            this.gb_AudioControl.Controls.Add(this.lb_CarDatensatzname);
            this.gb_AudioControl.Controls.Add(this.lb_CarVersion);
            this.gb_AudioControl.Controls.Add(this.lb_TuningVersInt);
            this.gb_AudioControl.Controls.Add(this.lbl_CarEQ);
            this.gb_AudioControl.Controls.Add(this.lbl_CarMics);
            this.gb_AudioControl.Controls.Add(this.lbl_CarType);
            this.gb_AudioControl.Controls.Add(this.tb_CarAUNo);
            this.gb_AudioControl.Controls.Add(this.lb_CarAUNo);
            this.gb_AudioControl.Location = new System.Drawing.Point(5, 3);
            this.gb_AudioControl.Name = "gb_AudioControl";
            this.gb_AudioControl.Size = new System.Drawing.Size(574, 266);
            this.gb_AudioControl.TabIndex = 1;
            this.gb_AudioControl.TabStop = false;
            this.gb_AudioControl.Text = "Audio-Control";
            // 
            // gb_Divider
            // 
            this.gb_Divider.Location = new System.Drawing.Point(4, 163);
            this.gb_Divider.Name = "gb_Divider";
            this.gb_Divider.Size = new System.Drawing.Size(559, 10);
            this.gb_Divider.TabIndex = 21;
            this.gb_Divider.TabStop = false;
            // 
            // numUD_CarEQ
            // 
            this.numUD_CarEQ.CausesValidation = false;
            this.numUD_CarEQ.Location = new System.Drawing.Point(471, 102);
            this.numUD_CarEQ.Maximum = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.numUD_CarEQ.Name = "numUD_CarEQ";
            this.numUD_CarEQ.Size = new System.Drawing.Size(43, 20);
            this.numUD_CarEQ.TabIndex = 6;
            this.numUD_CarEQ.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numUD_CarEQ.ValueChanged += new System.EventHandler(this.numUD_CarEQ_ValueChanged);
            // 
            // numUD_CarMics
            // 
            this.numUD_CarMics.Location = new System.Drawing.Point(226, 103);
            this.numUD_CarMics.Maximum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.numUD_CarMics.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numUD_CarMics.Name = "numUD_CarMics";
            this.numUD_CarMics.Size = new System.Drawing.Size(32, 20);
            this.numUD_CarMics.TabIndex = 4;
            this.numUD_CarMics.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numUD_CarMics.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.numUD_CarMics.ValueChanged += new System.EventHandler(this.numUD_CarMics_ValueChanged);
            // 
            // tb_ProjectFile
            // 
            this.tb_ProjectFile.Enabled = false;
            this.tb_ProjectFile.Location = new System.Drawing.Point(83, 68);
            this.tb_ProjectFile.Name = "tb_ProjectFile";
            this.tb_ProjectFile.Size = new System.Drawing.Size(344, 20);
            this.tb_ProjectFile.TabIndex = 52;
            this.tb_ProjectFile.TabStop = false;
            // 
            // tb_ProjectPath
            // 
            this.tb_ProjectPath.Enabled = false;
            this.tb_ProjectPath.Location = new System.Drawing.Point(83, 43);
            this.tb_ProjectPath.Name = "tb_ProjectPath";
            this.tb_ProjectPath.Size = new System.Drawing.Size(480, 20);
            this.tb_ProjectPath.TabIndex = 51;
            this.tb_ProjectPath.TabStop = false;
            // 
            // lbl_LuaFile
            // 
            this.lbl_LuaFile.AutoSize = true;
            this.lbl_LuaFile.Location = new System.Drawing.Point(14, 71);
            this.lbl_LuaFile.Name = "lbl_LuaFile";
            this.lbl_LuaFile.Size = new System.Drawing.Size(62, 13);
            this.lbl_LuaFile.TabIndex = 17;
            this.lbl_LuaFile.Text = "Project-File:";
            // 
            // lbl_LuaPath
            // 
            this.lbl_LuaPath.AutoSize = true;
            this.lbl_LuaPath.Location = new System.Drawing.Point(14, 45);
            this.lbl_LuaPath.Name = "lbl_LuaPath";
            this.lbl_LuaPath.Size = new System.Drawing.Size(68, 13);
            this.lbl_LuaPath.TabIndex = 18;
            this.lbl_LuaPath.Text = "Project-Path:";
            // 
            // btn_OpenFile
            // 
            this.btn_OpenFile.Location = new System.Drawing.Point(83, 14);
            this.btn_OpenFile.Name = "btn_OpenFile";
            this.btn_OpenFile.Size = new System.Drawing.Size(105, 23);
            this.btn_OpenFile.TabIndex = 1;
            this.btn_OpenFile.Text = "Open Project";
            this.btn_OpenFile.UseVisualStyleBackColor = true;
            this.btn_OpenFile.Click += new System.EventHandler(this.btn_OpenFile_Click);
            // 
            // btn_ImportPackage
            // 
            this.btn_ImportPackage.Location = new System.Drawing.Point(392, 14);
            this.btn_ImportPackage.Name = "btn_ImportPackage";
            this.btn_ImportPackage.Size = new System.Drawing.Size(105, 23);
            this.btn_ImportPackage.TabIndex = 2;
            this.btn_ImportPackage.Text = "Import Package";
            this.btn_ImportPackage.UseVisualStyleBackColor = true;
            this.btn_ImportPackage.Click += new System.EventHandler(this.btn_ImportPackage_Click);
            // 
            // btn_NewProject
            // 
            this.btn_NewProject.Location = new System.Drawing.Point(238, 14);
            this.btn_NewProject.Name = "btn_NewProject";
            this.btn_NewProject.Size = new System.Drawing.Size(105, 23);
            this.btn_NewProject.TabIndex = 2;
            this.btn_NewProject.Text = "New Project";
            this.btn_NewProject.UseVisualStyleBackColor = true;
            this.btn_NewProject.Click += new System.EventHandler(this.btn_NewProject_Click);
            // 
            // coB_AmpType
            // 
            this.coB_AmpType.FormattingEnabled = true;
            this.coB_AmpType.Items.AddRange(new object[] {
            "INAMP - 9VD-Intern",
            "EXAMP - 9VS-Harman",
            "EXAMP - 8RF-B&O",
            "INAMP - 9VK-Klangpaket",
            "EXAMP - 9VL-Bose ",
            "EXAMP - 9VJ-Burmester",
            "RUAMP - 9VK-9VL"});
            this.coB_AmpType.Location = new System.Drawing.Point(471, 128);
            this.coB_AmpType.Name = "coB_AmpType";
            this.coB_AmpType.Size = new System.Drawing.Size(92, 21);
            this.coB_AmpType.TabIndex = 5;
            this.coB_AmpType.Text = "   select";
            this.coB_AmpType.SelectedIndexChanged += new System.EventHandler(this.coB_AmpType_SelectedIndexChanged);
            // 
            // coB_CarRevision
            // 
            this.coB_CarRevision.FormattingEnabled = true;
            this.coB_CarRevision.Items.AddRange(new object[] {
            "0 - Test/Prototyp",
            "1 - Series car",
            "2 - Series car",
            "3 - Series car",
            "4 - Series car",
            "5 - Series car",
            "6 - Series car",
            "7 - Series car",
            "8 - Series car",
            "9 - Series car"});
            this.coB_CarRevision.Location = new System.Drawing.Point(310, 102);
            this.coB_CarRevision.Name = "coB_CarRevision";
            this.coB_CarRevision.Size = new System.Drawing.Size(116, 21);
            this.coB_CarRevision.TabIndex = 5;
            this.coB_CarRevision.Text = "please select";
            this.coB_CarRevision.SelectedIndexChanged += new System.EventHandler(this.coB_CarRevision_SelectedIndexChanged);
            // 
            // tb_CommentExt
            // 
            this.tb_CommentExt.Location = new System.Drawing.Point(221, 234);
            this.tb_CommentExt.Name = "tb_CommentExt";
            this.tb_CommentExt.Size = new System.Drawing.Size(276, 20);
            this.tb_CommentExt.TabIndex = 13;
            this.tb_CommentExt.TextChanged += new System.EventHandler(this.tb_CommentExt_TextChanged);
            // 
            // tb_CommentInt
            // 
            this.tb_CommentInt.Location = new System.Drawing.Point(221, 207);
            this.tb_CommentInt.Name = "tb_CommentInt";
            this.tb_CommentInt.Size = new System.Drawing.Size(276, 20);
            this.tb_CommentInt.TabIndex = 10;
            this.tb_CommentInt.TextChanged += new System.EventHandler(this.tb_CommentInt_TextChanged);
            // 
            // tb_AuthorExt
            // 
            this.tb_AuthorExt.Location = new System.Drawing.Point(90, 234);
            this.tb_AuthorExt.Name = "tb_AuthorExt";
            this.tb_AuthorExt.Size = new System.Drawing.Size(114, 20);
            this.tb_AuthorExt.TabIndex = 12;
            this.tb_AuthorExt.Text = "Max Mustermann";
            this.tb_AuthorExt.TextChanged += new System.EventHandler(this.tb_AuthorExt_TextChanged);
            // 
            // tb_AuthorInt
            // 
            this.tb_AuthorInt.Location = new System.Drawing.Point(90, 207);
            this.tb_AuthorInt.Name = "tb_AuthorInt";
            this.tb_AuthorInt.Size = new System.Drawing.Size(114, 20);
            this.tb_AuthorInt.TabIndex = 9;
            this.tb_AuthorInt.Text = "James Blond";
            this.tb_AuthorInt.TextChanged += new System.EventHandler(this.tb_AuthorInt_TextChanged);
            // 
            // tb_CarDatensatzname
            // 
            this.tb_CarDatensatzname.Enabled = false;
            this.tb_CarDatensatzname.Location = new System.Drawing.Point(271, 128);
            this.tb_CarDatensatzname.Name = "tb_CarDatensatzname";
            this.tb_CarDatensatzname.Size = new System.Drawing.Size(155, 20);
            this.tb_CarDatensatzname.TabIndex = 8;
            this.tb_CarDatensatzname.Text = "xx";
            // 
            // tb_CarVersion
            // 
            this.tb_CarVersion.Enabled = false;
            this.tb_CarVersion.Location = new System.Drawing.Point(83, 128);
            this.tb_CarVersion.Name = "tb_CarVersion";
            this.tb_CarVersion.Size = new System.Drawing.Size(84, 20);
            this.tb_CarVersion.TabIndex = 7;
            this.tb_CarVersion.Text = "MSEQ";
            // 
            // tb_TuningVersExt
            // 
            this.tb_TuningVersExt.Location = new System.Drawing.Point(529, 230);
            this.tb_TuningVersExt.Name = "tb_TuningVersExt";
            this.tb_TuningVersExt.Size = new System.Drawing.Size(31, 20);
            this.tb_TuningVersExt.TabIndex = 14;
            this.tb_TuningVersExt.Text = "00";
            this.tb_TuningVersExt.TextChanged += new System.EventHandler(this.tb_TuningVersExt_TextChanged);
            // 
            // tb_TuningVersInt
            // 
            this.tb_TuningVersInt.Location = new System.Drawing.Point(529, 207);
            this.tb_TuningVersInt.Name = "tb_TuningVersInt";
            this.tb_TuningVersInt.Size = new System.Drawing.Size(31, 20);
            this.tb_TuningVersInt.TabIndex = 11;
            this.tb_TuningVersInt.Text = "00";
            this.tb_TuningVersInt.TextChanged += new System.EventHandler(this.tb_TuningVersInt_TextChanged);
            // 
            // lbl_CommentInt
            // 
            this.lbl_CommentInt.AutoSize = true;
            this.lbl_CommentInt.Location = new System.Drawing.Point(218, 186);
            this.lbl_CommentInt.Name = "lbl_CommentInt";
            this.lbl_CommentInt.Size = new System.Drawing.Size(54, 13);
            this.lbl_CommentInt.TabIndex = 12;
            this.lbl_CommentInt.Text = "Comment:";
            // 
            // lbl_AuthorExt
            // 
            this.lbl_AuthorExt.AutoSize = true;
            this.lbl_AuthorExt.Location = new System.Drawing.Point(12, 237);
            this.lbl_AuthorExt.Name = "lbl_AuthorExt";
            this.lbl_AuthorExt.Size = new System.Drawing.Size(48, 13);
            this.lbl_AuthorExt.TabIndex = 12;
            this.lbl_AuthorExt.Text = "External:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(87, 186);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "Author:";
            // 
            // Lbl_AuthorInt
            // 
            this.Lbl_AuthorInt.AutoSize = true;
            this.Lbl_AuthorInt.Location = new System.Drawing.Point(12, 210);
            this.Lbl_AuthorInt.Name = "Lbl_AuthorInt";
            this.Lbl_AuthorInt.Size = new System.Drawing.Size(45, 13);
            this.Lbl_AuthorInt.TabIndex = 12;
            this.Lbl_AuthorInt.Text = "Internal:";
            // 
            // lblAmpType
            // 
            this.lblAmpType.AutoSize = true;
            this.lblAmpType.Location = new System.Drawing.Point(440, 131);
            this.lblAmpType.Name = "lblAmpType";
            this.lblAmpType.Size = new System.Drawing.Size(31, 13);
            this.lblAmpType.TabIndex = 12;
            this.lblAmpType.Text = "Amp:";
            // 
            // lb_CarDatensatzname
            // 
            this.lb_CarDatensatzname.AutoSize = true;
            this.lb_CarDatensatzname.Location = new System.Drawing.Point(184, 131);
            this.lb_CarDatensatzname.Name = "lb_CarDatensatzname";
            this.lb_CarDatensatzname.Size = new System.Drawing.Size(77, 13);
            this.lb_CarDatensatzname.TabIndex = 12;
            this.lb_CarDatensatzname.Text = "DataSetName:";
            // 
            // lb_CarVersion
            // 
            this.lb_CarVersion.AutoSize = true;
            this.lb_CarVersion.Location = new System.Drawing.Point(14, 131);
            this.lb_CarVersion.Name = "lb_CarVersion";
            this.lb_CarVersion.Size = new System.Drawing.Size(45, 13);
            this.lb_CarVersion.TabIndex = 12;
            this.lb_CarVersion.Text = "Version:";
            // 
            // lb_TuningVersInt
            // 
            this.lb_TuningVersInt.AutoSize = true;
            this.lb_TuningVersInt.Location = new System.Drawing.Point(482, 186);
            this.lb_TuningVersInt.Name = "lb_TuningVersInt";
            this.lb_TuningVersInt.Size = new System.Drawing.Size(81, 13);
            this.lb_TuningVersInt.TabIndex = 12;
            this.lb_TuningVersInt.Text = "Tuning-Version:";
            // 
            // lbl_CarEQ
            // 
            this.lbl_CarEQ.AutoSize = true;
            this.lbl_CarEQ.Location = new System.Drawing.Point(440, 104);
            this.lbl_CarEQ.Name = "lbl_CarEQ";
            this.lbl_CarEQ.Size = new System.Drawing.Size(25, 13);
            this.lbl_CarEQ.TabIndex = 12;
            this.lbl_CarEQ.Text = "EQ:";
            // 
            // lbl_CarMics
            // 
            this.lbl_CarMics.AutoSize = true;
            this.lbl_CarMics.Location = new System.Drawing.Point(184, 105);
            this.lbl_CarMics.Name = "lbl_CarMics";
            this.lbl_CarMics.Size = new System.Drawing.Size(44, 13);
            this.lbl_CarMics.TabIndex = 12;
            this.lbl_CarMics.Text = "Mic-No:";
            // 
            // lbl_CarType
            // 
            this.lbl_CarType.AutoSize = true;
            this.lbl_CarType.Location = new System.Drawing.Point(270, 105);
            this.lbl_CarType.Name = "lbl_CarType";
            this.lbl_CarType.Size = new System.Drawing.Size(34, 13);
            this.lbl_CarType.TabIndex = 12;
            this.lbl_CarType.Text = "Type:";
            // 
            // tb_CarAUNo
            // 
            this.tb_CarAUNo.Location = new System.Drawing.Point(83, 102);
            this.tb_CarAUNo.Name = "tb_CarAUNo";
            this.tb_CarAUNo.Size = new System.Drawing.Size(84, 20);
            this.tb_CarAUNo.TabIndex = 3;
            this.tb_CarAUNo.Text = "AUxxx";
            this.tb_CarAUNo.TextChanged += new System.EventHandler(this.tb_CarAUNo_TextChanged);
            // 
            // lb_CarAUNo
            // 
            this.lb_CarAUNo.AutoSize = true;
            this.lb_CarAUNo.Location = new System.Drawing.Point(14, 105);
            this.lb_CarAUNo.Name = "lb_CarAUNo";
            this.lb_CarAUNo.Size = new System.Drawing.Size(61, 13);
            this.lb_CarAUNo.TabIndex = 12;
            this.lb_CarAUNo.Text = "Car-AU-No:";
            // 
            // gb_ConfigVersion
            // 
            this.gb_ConfigVersion.Controls.Add(this.cbByPassExt);
            this.gb_ConfigVersion.Controls.Add(this.lbl_ADI_ConfVersion);
            this.gb_ConfigVersion.Controls.Add(this.btn_LuaSelection);
            this.gb_ConfigVersion.Controls.Add(this.but_ExtDataInfo);
            this.gb_ConfigVersion.Controls.Add(this.lbl_CLVLext);
            this.gb_ConfigVersion.Controls.Add(this.lbl_CLVL);
            this.gb_ConfigVersion.Controls.Add(this.lbl_CMICext);
            this.gb_ConfigVersion.Controls.Add(this.lbl_CMIC);
            this.gb_ConfigVersion.Controls.Add(this.tb_CLVLext);
            this.gb_ConfigVersion.Controls.Add(this.tb_CMICext);
            this.gb_ConfigVersion.Controls.Add(this.tb_CLVL);
            this.gb_ConfigVersion.Controls.Add(this.tb_CMIC);
            this.gb_ConfigVersion.Controls.Add(this.tbLuaName);
            this.gb_ConfigVersion.Controls.Add(this.tb_lua_crc);
            this.gb_ConfigVersion.Controls.Add(this.lbl_LuaName);
            this.gb_ConfigVersion.Location = new System.Drawing.Point(585, 3);
            this.gb_ConfigVersion.Name = "gb_ConfigVersion";
            this.gb_ConfigVersion.Size = new System.Drawing.Size(179, 266);
            this.gb_ConfigVersion.TabIndex = 0;
            this.gb_ConfigVersion.TabStop = false;
            this.gb_ConfigVersion.Text = "Extended Data Information";
            // 
            // cbByPassExt
            // 
            this.cbByPassExt.AutoSize = true;
            this.cbByPassExt.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cbByPassExt.Location = new System.Drawing.Point(14, 115);
            this.cbByPassExt.Name = "cbByPassExt";
            this.cbByPassExt.Size = new System.Drawing.Size(131, 17);
            this.cbByPassExt.TabIndex = 33;
            this.cbByPassExt.Text = "ByPass extern active: ";
            this.cbByPassExt.UseVisualStyleBackColor = true;
            this.cbByPassExt.CheckedChanged += new System.EventHandler(this.cbByPassExt_CheckedChanged);
            // 
            // lbl_ADI_ConfVersion
            // 
            this.lbl_ADI_ConfVersion.AutoSize = true;
            this.lbl_ADI_ConfVersion.Location = new System.Drawing.Point(17, 149);
            this.lbl_ADI_ConfVersion.Name = "lbl_ADI_ConfVersion";
            this.lbl_ADI_ConfVersion.Size = new System.Drawing.Size(56, 13);
            this.lbl_ADI_ConfVersion.TabIndex = 5;
            this.lbl_ADI_ConfVersion.Text = "LUA-CRC:";
            // 
            // btn_LuaSelection
            // 
            this.btn_LuaSelection.Location = new System.Drawing.Point(43, 197);
            this.btn_LuaSelection.Name = "btn_LuaSelection";
            this.btn_LuaSelection.Size = new System.Drawing.Size(107, 26);
            this.btn_LuaSelection.TabIndex = 31;
            this.btn_LuaSelection.Text = "LUA Selection";
            this.btn_LuaSelection.UseVisualStyleBackColor = true;
            this.btn_LuaSelection.Visible = false;
            this.btn_LuaSelection.Click += new System.EventHandler(this.btn_LuaSelection_Click);
            // 
            // but_ExtDataInfo
            // 
            this.but_ExtDataInfo.Location = new System.Drawing.Point(18, 232);
            this.but_ExtDataInfo.Name = "but_ExtDataInfo";
            this.but_ExtDataInfo.Size = new System.Drawing.Size(146, 23);
            this.but_ExtDataInfo.TabIndex = 32;
            this.but_ExtDataInfo.Text = "Expert Modus";
            this.but_ExtDataInfo.UseVisualStyleBackColor = true;
            this.but_ExtDataInfo.Click += new System.EventHandler(this.but_ExtDataInfo_Click);
            // 
            // lbl_CLVLext
            // 
            this.lbl_CLVLext.AutoSize = true;
            this.lbl_CLVLext.Location = new System.Drawing.Point(16, 92);
            this.lbl_CLVLext.Name = "lbl_CLVLext";
            this.lbl_CLVLext.Size = new System.Drawing.Size(53, 13);
            this.lbl_CLVLext.TabIndex = 4;
            this.lbl_CLVLext.Text = "CLVL ext:";
            // 
            // lbl_CLVL
            // 
            this.lbl_CLVL.AutoSize = true;
            this.lbl_CLVL.Location = new System.Drawing.Point(15, 43);
            this.lbl_CLVL.Name = "lbl_CLVL";
            this.lbl_CLVL.Size = new System.Drawing.Size(36, 13);
            this.lbl_CLVL.TabIndex = 4;
            this.lbl_CLVL.Text = "CLVL:";
            // 
            // lbl_CMICext
            // 
            this.lbl_CMICext.AutoSize = true;
            this.lbl_CMICext.Location = new System.Drawing.Point(16, 69);
            this.lbl_CMICext.Name = "lbl_CMICext";
            this.lbl_CMICext.Size = new System.Drawing.Size(53, 13);
            this.lbl_CMICext.TabIndex = 4;
            this.lbl_CMICext.Text = "CMIC ext:";
            // 
            // lbl_CMIC
            // 
            this.lbl_CMIC.AutoSize = true;
            this.lbl_CMIC.Location = new System.Drawing.Point(15, 20);
            this.lbl_CMIC.Name = "lbl_CMIC";
            this.lbl_CMIC.Size = new System.Drawing.Size(36, 13);
            this.lbl_CMIC.TabIndex = 4;
            this.lbl_CMIC.Text = "CMIC:";
            // 
            // tb_CLVLext
            // 
            this.tb_CLVLext.Enabled = false;
            this.tb_CLVLext.Location = new System.Drawing.Point(102, 89);
            this.tb_CLVLext.Name = "tb_CLVLext";
            this.tb_CLVLext.Size = new System.Drawing.Size(62, 20);
            this.tb_CLVLext.TabIndex = 28;
            this.tb_CLVLext.Text = "8";
            this.tb_CLVLext.TextChanged += new System.EventHandler(this.tb_CLVLext_TextChanged);
            // 
            // tb_CMICext
            // 
            this.tb_CMICext.Enabled = false;
            this.tb_CMICext.Location = new System.Drawing.Point(102, 66);
            this.tb_CMICext.Name = "tb_CMICext";
            this.tb_CMICext.Size = new System.Drawing.Size(62, 20);
            this.tb_CMICext.TabIndex = 27;
            this.tb_CMICext.Text = "5";
            this.tb_CMICext.TextChanged += new System.EventHandler(this.tb_CMICext_TextChanged);
            // 
            // tb_CLVL
            // 
            this.tb_CLVL.Enabled = false;
            this.tb_CLVL.Location = new System.Drawing.Point(102, 40);
            this.tb_CLVL.Name = "tb_CLVL";
            this.tb_CLVL.Size = new System.Drawing.Size(62, 20);
            this.tb_CLVL.TabIndex = 28;
            this.tb_CLVL.Text = "8";
            this.tb_CLVL.TextChanged += new System.EventHandler(this.tb_CLVL_TextChanged);
            // 
            // tb_CMIC
            // 
            this.tb_CMIC.Enabled = false;
            this.tb_CMIC.Location = new System.Drawing.Point(102, 17);
            this.tb_CMIC.Name = "tb_CMIC";
            this.tb_CMIC.Size = new System.Drawing.Size(62, 20);
            this.tb_CMIC.TabIndex = 27;
            this.tb_CMIC.Text = "5";
            this.tb_CMIC.TextChanged += new System.EventHandler(this.tb_CMIC_TextChanged);
            // 
            // tbLuaName
            // 
            this.tbLuaName.Enabled = false;
            this.tbLuaName.Location = new System.Drawing.Point(14, 171);
            this.tbLuaName.Name = "tbLuaName";
            this.tbLuaName.Size = new System.Drawing.Size(150, 20);
            this.tbLuaName.TabIndex = 30;
            this.tbLuaName.TabStop = false;
            // 
            // tb_lua_crc
            // 
            this.tb_lua_crc.Enabled = false;
            this.tb_lua_crc.Location = new System.Drawing.Point(102, 146);
            this.tb_lua_crc.Name = "tb_lua_crc";
            this.tb_lua_crc.Size = new System.Drawing.Size(62, 20);
            this.tb_lua_crc.TabIndex = 29;
            this.tb_lua_crc.TabStop = false;
            // 
            // lbl_LuaName
            // 
            this.lbl_LuaName.AutoSize = true;
            this.lbl_LuaName.Location = new System.Drawing.Point(15, 155);
            this.lbl_LuaName.Name = "lbl_LuaName";
            this.lbl_LuaName.Size = new System.Drawing.Size(62, 13);
            this.lbl_LuaName.TabIndex = 5;
            this.lbl_LuaName.Text = "LUA-Name:";
            this.lbl_LuaName.Visible = false;
            // 
            // btn_AudioPackage
            // 
            this.btn_AudioPackage.Location = new System.Drawing.Point(43, 617);
            this.btn_AudioPackage.Name = "btn_AudioPackage";
            this.btn_AudioPackage.Size = new System.Drawing.Size(125, 47);
            this.btn_AudioPackage.TabIndex = 25;
            this.btn_AudioPackage.Text = "Create Audio Package";
            this.btn_AudioPackage.UseVisualStyleBackColor = true;
            this.btn_AudioPackage.Click += new System.EventHandler(this.btn_AudioPackage_Click);
            // 
            // tb_AudioPackage
            // 
            this.tb_AudioPackage.Enabled = false;
            this.tb_AudioPackage.Location = new System.Drawing.Point(186, 617);
            this.tb_AudioPackage.Name = "tb_AudioPackage";
            this.tb_AudioPackage.Size = new System.Drawing.Size(274, 20);
            this.tb_AudioPackage.TabIndex = 26;
            this.tb_AudioPackage.TabStop = false;
            this.tb_AudioPackage.Text = "-";
            // 
            // lbl_AudioPackageDate
            // 
            this.lbl_AudioPackageDate.Location = new System.Drawing.Point(186, 642);
            this.lbl_AudioPackageDate.Name = "lbl_AudioPackageDate";
            this.lbl_AudioPackageDate.Size = new System.Drawing.Size(274, 23);
            this.lbl_AudioPackageDate.TabIndex = 27;
            this.lbl_AudioPackageDate.Text = "label2";
            // 
            // btnGetAudioPackage
            // 
            this.btnGetAudioPackage.Location = new System.Drawing.Point(477, 634);
            this.btnGetAudioPackage.Name = "btnGetAudioPackage";
            this.btnGetAudioPackage.Size = new System.Drawing.Size(125, 31);
            this.btnGetAudioPackage.TabIndex = 25;
            this.btnGetAudioPackage.Text = "Get Audio Package";
            this.btnGetAudioPackage.UseVisualStyleBackColor = true;
            this.btnGetAudioPackage.Visible = false;
            this.btnGetAudioPackage.Click += new System.EventHandler(this.btnGetAudioPackage_Click);
            // 
            // pnlMerge
            // 
            this.pnlMerge.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlMerge.Controls.Add(this.groupBox1);
            this.pnlMerge.Location = new System.Drawing.Point(789, 278);
            this.pnlMerge.Name = "pnlMerge";
            this.pnlMerge.Size = new System.Drawing.Size(782, 259);
            this.pnlMerge.TabIndex = 28;
            this.pnlMerge.Visible = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.lblCLVLSpeaker);
            this.groupBox1.Controls.Add(this.lblCLVLSoundSys);
            this.groupBox1.Controls.Add(this.btnCheckCLVL);
            this.groupBox1.Controls.Add(this.tbxCLVLSpeaker);
            this.groupBox1.Controls.Add(this.btnMergePathSearch);
            this.groupBox1.Controls.Add(this.btnCheckLUA);
            this.groupBox1.Controls.Add(this.tbxCLVLSoundSys);
            this.groupBox1.Controls.Add(this.lblPackSpeaker);
            this.groupBox1.Controls.Add(this.numUDversion);
            this.groupBox1.Controls.Add(this.lblPackSoundSystem);
            this.groupBox1.Controls.Add(this.btnMergeFinish);
            this.groupBox1.Controls.Add(this.tbxPackSoundSystem);
            this.groupBox1.Controls.Add(this.btnPackSpeaker);
            this.groupBox1.Controls.Add(this.tbxPackSpeaker);
            this.groupBox1.Controls.Add(this.btnMerge);
            this.groupBox1.Controls.Add(this.tbxMergeDir);
            this.groupBox1.Controls.Add(this.btnPackSoundsystem);
            this.groupBox1.Controls.Add(this.lblVersion);
            this.groupBox1.Location = new System.Drawing.Point(6, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(759, 201);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Merge - Pannel";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 27);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Project-Path:";
            // 
            // lblCLVLSpeaker
            // 
            this.lblCLVLSpeaker.AutoSize = true;
            this.lblCLVLSpeaker.Location = new System.Drawing.Point(421, 139);
            this.lblCLVLSpeaker.Name = "lblCLVLSpeaker";
            this.lblCLVLSpeaker.Size = new System.Drawing.Size(76, 13);
            this.lblCLVLSpeaker.TabIndex = 0;
            this.lblCLVLSpeaker.Text = "CLVL-Speaker";
            // 
            // lblCLVLSoundSys
            // 
            this.lblCLVLSoundSys.AutoSize = true;
            this.lblCLVLSoundSys.Location = new System.Drawing.Point(421, 161);
            this.lblCLVLSoundSys.Name = "lblCLVLSoundSys";
            this.lblCLVLSoundSys.Size = new System.Drawing.Size(101, 13);
            this.lblCLVLSoundSys.TabIndex = 0;
            this.lblCLVLSoundSys.Text = "CLVL-SoundSystem";
            // 
            // btnCheckCLVL
            // 
            this.btnCheckCLVL.Location = new System.Drawing.Point(316, 135);
            this.btnCheckCLVL.Name = "btnCheckCLVL";
            this.btnCheckCLVL.Size = new System.Drawing.Size(87, 47);
            this.btnCheckCLVL.TabIndex = 1;
            this.btnCheckCLVL.Text = "Check CLVL";
            this.btnCheckCLVL.UseVisualStyleBackColor = true;
            // 
            // tbxCLVLSpeaker
            // 
            this.tbxCLVLSpeaker.Location = new System.Drawing.Point(526, 135);
            this.tbxCLVLSpeaker.Name = "tbxCLVLSpeaker";
            this.tbxCLVLSpeaker.Size = new System.Drawing.Size(39, 20);
            this.tbxCLVLSpeaker.TabIndex = 2;
            // 
            // btnMergePathSearch
            // 
            this.btnMergePathSearch.Location = new System.Drawing.Point(673, 22);
            this.btnMergePathSearch.Name = "btnMergePathSearch";
            this.btnMergePathSearch.Size = new System.Drawing.Size(58, 23);
            this.btnMergePathSearch.TabIndex = 1;
            this.btnMergePathSearch.Text = "Search";
            this.btnMergePathSearch.UseVisualStyleBackColor = true;
            // 
            // btnCheckLUA
            // 
            this.btnCheckLUA.Location = new System.Drawing.Point(203, 135);
            this.btnCheckLUA.Name = "btnCheckLUA";
            this.btnCheckLUA.Size = new System.Drawing.Size(87, 47);
            this.btnCheckLUA.TabIndex = 1;
            this.btnCheckLUA.Text = "Check LUA";
            this.btnCheckLUA.UseVisualStyleBackColor = true;
            // 
            // tbxCLVLSoundSys
            // 
            this.tbxCLVLSoundSys.Location = new System.Drawing.Point(526, 157);
            this.tbxCLVLSoundSys.Name = "tbxCLVLSoundSys";
            this.tbxCLVLSoundSys.Size = new System.Drawing.Size(39, 20);
            this.tbxCLVLSoundSys.TabIndex = 2;
            // 
            // lblPackSpeaker
            // 
            this.lblPackSpeaker.AutoSize = true;
            this.lblPackSpeaker.Location = new System.Drawing.Point(19, 53);
            this.lblPackSpeaker.Name = "lblPackSpeaker";
            this.lblPackSpeaker.Size = new System.Drawing.Size(96, 13);
            this.lblPackSpeaker.TabIndex = 0;
            this.lblPackSpeaker.Text = "Speaker-Package:";
            // 
            // numUDversion
            // 
            this.numUDversion.CausesValidation = false;
            this.numUDversion.Location = new System.Drawing.Point(137, 159);
            this.numUDversion.Maximum = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.numUDversion.Name = "numUDversion";
            this.numUDversion.Size = new System.Drawing.Size(43, 20);
            this.numUDversion.TabIndex = 6;
            this.numUDversion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numUDversion.ValueChanged += new System.EventHandler(this.numUD_CarEQ_ValueChanged);
            // 
            // lblPackSoundSystem
            // 
            this.lblPackSoundSystem.AutoSize = true;
            this.lblPackSoundSystem.Location = new System.Drawing.Point(19, 81);
            this.lblPackSoundSystem.Name = "lblPackSoundSystem";
            this.lblPackSoundSystem.Size = new System.Drawing.Size(119, 13);
            this.lblPackSoundSystem.TabIndex = 0;
            this.lblPackSoundSystem.Text = "Soundsystem-Package:";
            // 
            // btnMergeFinish
            // 
            this.btnMergeFinish.Location = new System.Drawing.Point(644, 132);
            this.btnMergeFinish.Name = "btnMergeFinish";
            this.btnMergeFinish.Size = new System.Drawing.Size(87, 47);
            this.btnMergeFinish.TabIndex = 1;
            this.btnMergeFinish.Text = "Finish";
            this.btnMergeFinish.UseVisualStyleBackColor = true;
            this.btnMergeFinish.Click += new System.EventHandler(this.btnMergeFinish_Click);
            // 
            // tbxPackSoundSystem
            // 
            this.tbxPackSoundSystem.Location = new System.Drawing.Point(143, 78);
            this.tbxPackSoundSystem.Name = "tbxPackSoundSystem";
            this.tbxPackSoundSystem.Size = new System.Drawing.Size(516, 20);
            this.tbxPackSoundSystem.TabIndex = 2;
            // 
            // btnPackSpeaker
            // 
            this.btnPackSpeaker.Location = new System.Drawing.Point(673, 51);
            this.btnPackSpeaker.Name = "btnPackSpeaker";
            this.btnPackSpeaker.Size = new System.Drawing.Size(58, 23);
            this.btnPackSpeaker.TabIndex = 1;
            this.btnPackSpeaker.Text = "Search";
            this.btnPackSpeaker.UseVisualStyleBackColor = true;
            // 
            // tbxPackSpeaker
            // 
            this.tbxPackSpeaker.Location = new System.Drawing.Point(143, 50);
            this.tbxPackSpeaker.Name = "tbxPackSpeaker";
            this.tbxPackSpeaker.Size = new System.Drawing.Size(516, 20);
            this.tbxPackSpeaker.TabIndex = 2;
            // 
            // btnMerge
            // 
            this.btnMerge.Location = new System.Drawing.Point(22, 135);
            this.btnMerge.Name = "btnMerge";
            this.btnMerge.Size = new System.Drawing.Size(87, 47);
            this.btnMerge.TabIndex = 1;
            this.btnMerge.Text = "Merge";
            this.btnMerge.UseVisualStyleBackColor = true;
            // 
            // tbxMergeDir
            // 
            this.tbxMergeDir.Location = new System.Drawing.Point(143, 24);
            this.tbxMergeDir.Name = "tbxMergeDir";
            this.tbxMergeDir.Size = new System.Drawing.Size(516, 20);
            this.tbxMergeDir.TabIndex = 2;
            // 
            // btnPackSoundsystem
            // 
            this.btnPackSoundsystem.Location = new System.Drawing.Point(673, 79);
            this.btnPackSoundsystem.Name = "btnPackSoundsystem";
            this.btnPackSoundsystem.Size = new System.Drawing.Size(58, 23);
            this.btnPackSoundsystem.TabIndex = 1;
            this.btnPackSoundsystem.Text = "Search";
            this.btnPackSoundsystem.UseVisualStyleBackColor = true;
            // 
            // lblVersion
            // 
            this.lblVersion.AutoSize = true;
            this.lblVersion.Location = new System.Drawing.Point(134, 135);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(42, 13);
            this.lblVersion.TabIndex = 0;
            this.lblVersion.Text = "Version";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lblSelectConBox);
            this.panel1.Controls.Add(this.rBtnCBoxLow);
            this.panel1.Controls.Add(this.rBtnCBox2020);
            this.panel1.Location = new System.Drawing.Point(14, 28);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(769, 30);
            this.panel1.TabIndex = 28;
            // 
            // lblSelectConBox
            // 
            this.lblSelectConBox.AutoSize = true;
            this.lblSelectConBox.Location = new System.Drawing.Point(14, 8);
            this.lblSelectConBox.Name = "lblSelectConBox";
            this.lblSelectConBox.Size = new System.Drawing.Size(107, 13);
            this.lblSelectConBox.TabIndex = 1;
            this.lblSelectConBox.Text = "Select ConBox Type:";
            // 
            // rBtnCBoxLow
            // 
            this.rBtnCBoxLow.AutoSize = true;
            this.rBtnCBoxLow.Location = new System.Drawing.Point(278, 6);
            this.rBtnCBoxLow.Name = "rBtnCBoxLow";
            this.rBtnCBoxLow.Size = new System.Drawing.Size(82, 17);
            this.rBtnCBoxLow.TabIndex = 0;
            this.rBtnCBoxLow.Text = "ConBoxLow";
            this.rBtnCBoxLow.UseVisualStyleBackColor = true;
            this.rBtnCBoxLow.CheckedChanged += new System.EventHandler(this.rBtnCBoxLow_CheckedChanged);
            // 
            // rBtnCBox2020
            // 
            this.rBtnCBox2020.AutoSize = true;
            this.rBtnCBox2020.Location = new System.Drawing.Point(172, 6);
            this.rBtnCBox2020.Name = "rBtnCBox2020";
            this.rBtnCBox2020.Size = new System.Drawing.Size(86, 17);
            this.rBtnCBox2020.TabIndex = 0;
            this.rBtnCBox2020.Text = "ConBox2020";
            this.rBtnCBox2020.UseVisualStyleBackColor = true;
            this.rBtnCBox2020.CheckedChanged += new System.EventHandler(this.rBtnCBox2020_CheckedChanged);
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(812, 676);
            this.Controls.Add(this.pnlMerge);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lbl_AudioPackageDate);
            this.Controls.Add(this.tb_AudioPackage);
            this.Controls.Add(this.btnGetAudioPackage);
            this.Controls.Add(this.btn_AudioPackage);
            this.Controls.Add(this.pnl_lua_config);
            this.Controls.Add(this.pnlCRC);
            this.Controls.Add(this.pnl_EQ);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(828, 715);
            this.Name = "FrmMain";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmMain_FormClosing);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.pnl_EQ.ResumeLayout(false);
            this.gb_ExternalEQ.ResumeLayout(false);
            this.gb_ExternalEQ.PerformLayout();
            this.gb_InternalEQ.ResumeLayout(false);
            this.gb_InternalEQ.PerformLayout();
            this.pnlCRC.ResumeLayout(false);
            this.pnlCRC.PerformLayout();
            this.pnl_lua_config.ResumeLayout(false);
            this.gb_AudioControl.ResumeLayout(false);
            this.gb_AudioControl.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_CarEQ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_CarMics)).EndInit();
            this.gb_ConfigVersion.ResumeLayout(false);
            this.gb_ConfigVersion.PerformLayout();
            this.pnlMerge.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUDversion)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem File;
        private System.Windows.Forms.ToolStripMenuItem infoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem versionToolStripMenuItem;
        private System.Windows.Forms.Panel pnl_EQ;
        private System.Windows.Forms.Panel pnlCRC;
        private System.Windows.Forms.RadioButton rb_CRC32;
        private System.Windows.Forms.RadioButton rb_MD5;
        private System.Windows.Forms.RadioButton rb_CRC16;
        private System.Windows.Forms.Label lbl_einstellungen;
        private System.Windows.Forms.GroupBox gb_InternalEQ;
        private System.Windows.Forms.TextBox tb_Intern_EQ_HashName;
        private System.Windows.Forms.TextBox tb_Intern_EQ_name;
        private System.Windows.Forms.Label lbl_Intern_EQ_HashName;
        private System.Windows.Forms.TextBox tb_Intern_EQ_Path;
        private System.Windows.Forms.Label lbl_Intern_EQ_name;
        private System.Windows.Forms.Button btn_Intern_EQ_Dir;
        private System.Windows.Forms.Label lbl_Intern_EQ_Path;
        private System.Windows.Forms.TextBox tb_Intern_EQ_Newname;
        private System.Windows.Forms.Label lbl_Intern_EQ_Newname;
        private System.Windows.Forms.GroupBox gb_ExternalEQ;
        private System.Windows.Forms.TextBox tb_Extern_EQ_Newname;
        private System.Windows.Forms.Label lbl_Extern_EQ_Newname;
        private System.Windows.Forms.TextBox tb_Extern_EQ_HashName;
        private System.Windows.Forms.TextBox tb_Extern_EQ_name;
        private System.Windows.Forms.Label lbl_Extern_EQ_HashName;
        private System.Windows.Forms.TextBox tb_Extern_EQ_Path;
        private System.Windows.Forms.Label lbl_Extern_EQ_name;
        private System.Windows.Forms.Button btn_extern_EQ_Dir;
        private System.Windows.Forms.Label lbl_Extern_EQ_Path;
        private System.Windows.Forms.Panel pnl_lua_config;
        private System.Windows.Forms.GroupBox gb_AudioControl;
        private System.Windows.Forms.TextBox tb_CarVersion;
        private System.Windows.Forms.TextBox tb_TuningVersInt;
        private System.Windows.Forms.Label lb_CarVersion;
        private System.Windows.Forms.Label lbl_CarType;
        private System.Windows.Forms.TextBox tb_CarAUNo;
        private System.Windows.Forms.Label lb_CarAUNo;
        private System.Windows.Forms.GroupBox gb_ConfigVersion;
        private System.Windows.Forms.Label lbl_CMIC;
        private System.Windows.Forms.TextBox tb_lua_crc;
        private System.Windows.Forms.Label lbl_ADI_ConfVersion;
        private System.Windows.Forms.Label lb_TuningVersInt;
        private System.Windows.Forms.ComboBox coB_CarRevision;
        private System.Windows.Forms.TextBox tb_TuningVersExt;
        private System.Windows.Forms.TextBox tb_CommentExt;
        private System.Windows.Forms.TextBox tb_CommentInt;
        private System.Windows.Forms.TextBox tb_AuthorExt;
        private System.Windows.Forms.TextBox tb_AuthorInt;
        private System.Windows.Forms.Label lbl_CommentInt;
        private System.Windows.Forms.Label lbl_AuthorExt;
        private System.Windows.Forms.Label Lbl_AuthorInt;
        private System.Windows.Forms.TextBox tb_CMIC;
        private System.Windows.Forms.Button but_ExtDataInfo;
        private System.Windows.Forms.Label lbl_CLVL;
        private System.Windows.Forms.TextBox tb_CLVL;
        private System.Windows.Forms.TextBox tb_ProjectFile;
        private System.Windows.Forms.TextBox tb_ProjectPath;
        private System.Windows.Forms.Label lbl_LuaFile;
        private System.Windows.Forms.Label lbl_LuaPath;
        private System.Windows.Forms.Button btn_OpenFile;
        private System.Windows.Forms.Button btn_NewProject;
        private System.Windows.Forms.Button btn_AudioPackage;
        private System.Windows.Forms.ToolStripMenuItem menue_OpenFile;
        private System.Windows.Forms.ToolStripMenuItem menue_NewProject;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.TextBox tbLuaName;
        private System.Windows.Forms.Label lbl_LuaName;
        private System.Windows.Forms.ToolStripMenuItem ConBoxToolStripMenuItem;
        private System.Windows.Forms.NumericUpDown numUD_CarMics;
        private System.Windows.Forms.Label lbl_CarMics;
        private System.Windows.Forms.NumericUpDown numUD_CarEQ;
        private System.Windows.Forms.TextBox tb_CarDatensatzname;
        private System.Windows.Forms.Label lb_CarDatensatzname;
        private System.Windows.Forms.Label lbl_CarEQ;
        private System.Windows.Forms.TextBox tb_AudioPackage;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_LuaSelection;
        private System.Windows.Forms.GroupBox gb_Divider;
        private System.Windows.Forms.RadioButton rb_SHA1;
        private System.Windows.Forms.Label lblAmpType;
        private System.Windows.Forms.ComboBox coB_AmpType;
        private System.Windows.Forms.CheckBox cb_SourceDirExt;
        private System.Windows.Forms.CheckBox cb_SourceDirInt;
        private System.Windows.Forms.Button btn_ImportPackage;
        private System.Windows.Forms.ToolStripMenuItem conBoxConfigurationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem preODXToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem createToolStripMenuItem;
        private System.Windows.Forms.Label lbl_AudioPackageDate;
        private System.Windows.Forms.Button btnGetAudioPackage;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem mergePackagesToolStripMenuItem;
        private System.Windows.Forms.Panel pnlMerge;
        private System.Windows.Forms.TextBox tbxMergeDir;
        private System.Windows.Forms.Button btnMergePathSearch;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblCLVLSpeaker;
        private System.Windows.Forms.Label lblCLVLSoundSys;
        private System.Windows.Forms.Button btnCheckCLVL;
        private System.Windows.Forms.TextBox tbxCLVLSpeaker;
        private System.Windows.Forms.Button btnCheckLUA;
        private System.Windows.Forms.TextBox tbxCLVLSoundSys;
        private System.Windows.Forms.Label lblPackSpeaker;
        private System.Windows.Forms.NumericUpDown numUDversion;
        private System.Windows.Forms.Label lblPackSoundSystem;
        private System.Windows.Forms.Button btnMergeFinish;
        private System.Windows.Forms.TextBox tbxPackSoundSystem;
        private System.Windows.Forms.Button btnPackSpeaker;
        private System.Windows.Forms.TextBox tbxPackSpeaker;
        private System.Windows.Forms.Button btnMerge;
        private System.Windows.Forms.Button btnPackSoundsystem;
        private System.Windows.Forms.Label lblVersion;
        private System.Windows.Forms.ToolStripMenuItem setConBox2020ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem setConBoxLowToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton rBtnCBoxLow;
        private System.Windows.Forms.RadioButton rBtnCBox2020;
        private System.Windows.Forms.Label lbl_CLVLext;
        private System.Windows.Forms.Label lbl_CMICext;
        private System.Windows.Forms.TextBox tb_CLVLext;
        private System.Windows.Forms.TextBox tb_CMICext;
        private System.Windows.Forms.Label lblSelectConBox;
        private System.Windows.Forms.CheckBox cbByPassExt;
    }
}

