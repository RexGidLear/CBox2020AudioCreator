-- version info
t_info ={
    car_type = "AU493",  -- vehicle type
    car_version = "00.01",  --  car version: Umbau: 00.xx, Prototyp_first: 01.xx, Prototyp_final: 03xx, Serie1: 11.xx, usw.
    Contry_version = "GUS",  -- Specification: GUS->G.O.S.T, EU,US->ITU P1140
	tuning_version = "00.01",  --  tuning version: pre-tuning: xx.0x, Serie: xx.00
    created = "10.07.2017",  -- Erstell-Datum
    author_intern = "IAV-M.Mustermann",  -- Ersteller
    author_extern = "Lear-M.Mustermann",  -- Ersteller
    comment1 = "Tuning A5 NB+WB",  -- Kommentar 1
    comment2 = "Line/speaker Out set to 100%"  -- Kommentar 2
}

-- data set values
t_data_set ={
    audio_EQ_int = "Audio_PP_HS_W16K_xxV14_HPF_CS42F6.xml",  --  EQ internes Tuning
    audio_EQ_ext = "Audi-A5_NB-final+WB-copy_CS036C.xml",  --  EQ externes Tuning
    NAD_CMIC = "6",  -- NAD microphone gain
    NAD_CLVL = "6",  -- NAD telephone volume level gain
    }

-- codec stuff

gpio_device("CODEC_RESET_N") {
    pin = "91"
}
 
i2c_device("CODEC") {
    address = 0x18
}

-- amp stuff

gpio_device("AMP_STANDBY_N") {
    pin = "45"
}

gpio_device("AMP_MUTE") {
    pin = "60"
}

i2c_device("AMP") {
    address = 0x6c
}

--

function init(parameters, devices)
    codec_reset_n = devices["CODEC_RESET_N"]
    amp_standby_n = devices["AMP_STANDBY_N"]
    amp_unmute = devices["AMP_MUTE"]

    -- mute amp
    amp_unmute:setvalue("0")
	
    -- switch on amp (for diagnostic)
    amp_standby_n:setvalue("1")
	
    -- hold codec in reset for power saving
    codec_reset_n:setvalue("0")

    amp = devices["AMP"]

    -- switch off amp for power safing
    amp_standby_n:setvalue("1")
end;

function standby(devices)
    codec_reset_n = devices["CODEC_RESET_N"]
    amp_standby_n = devices["AMP_STANDBY_N"]
    amp_unmute      = devices["AMP_MUTE"]

    -- Make sure amp is in standby and muted
    amp_unmute:setvalue("0")
    amp_standby_n:setvalue("0")
	
    -- hold codec in reset for power safing
    codec_reset_n:setvalue("0")
end;

function internal(devices)
    codec_reset_n = devices["CODEC_RESET_N"]
    amp_standby_n = devices["AMP_STANDBY_N"]
    amp_unmute      = devices["AMP_MUTE"]

    -- Mute amplifier first
    amp_unmute:setvalue("0")
	
    -- Power up amp
    amp_standby_n:setvalue("1")

    codec_reset_n:setvalue("0")
    codec_reset_n:setvalue("1")

    codec = devices["CODEC"]

    codec:writereg(0, 0x00); -- Page 0

    codec:writereg(3,   0x10); -- Q=2, PLL disabled
    codec:writereg(101, 0x01); -- CODEC_CLKIN uses CLKDIV_OUT
    codec:writereg(102, 0x82); -- CLKDIV uses BCLK
    codec:writereg(7,   0x0a); -- DAC no mix
    codec:writereg(12,  0x50); -- enable highpass on ADC
    codec:writereg(15,  0x09); -- unmute left PGA and set gain to +4.5dB (6dB - 1.5 dB headroom)
    codec:writereg(16,  0x09); -- unmute right PGA and set gain to +4.5dB
    codec:writereg(19,  0x04); -- MIC1L as single ended input to left PGA with 0B and power up ADC
    codec:writereg(21,  0x00); -- MIC1R as single ended input to left PGA with 0dB
    codec:writereg(22,  0x04); -- MIC1R as single ended input to right PGA with 0dB and power up ADC
    codec:writereg(24,  0x00); -- MIC1L as single ended input to right PGA with 0dB
    codec:writereg(37,  0xc0); -- DAC on
    codec:writereg(43,  0x00); -- left DAC mute off
    codec:writereg(44,  0x00); -- right DAC mute off
    codec:writereg(89,  0x80); -- DAC_L1 routed to RIGHT_LOP and set volume = 0dB (Speaker)
    -- codec:writereg(89,  0x89); -- DAC_L1 routed to RIGHT_LOP and set volume = -4,5dB (Speaker)
    -- codec:writereg(89,  0x90); -- DAC_L1 routed to RIGHT_LOP and set volume = -8dB (Speaker)
    -- codec:writereg(89,  0x9c); -- DAC_L1 routed to RIGHT_LOP and set volume = -14dB (Speaker)
    codec:writereg(86,  0x09); -- Left Line out power up and gain = 0dB
    codec:writereg(93,  0x09); -- Right Line out power up and volume = 0dB

    -- Unmute amp
    amp_unmute:setvalue("1")
end;

function external(devices)
    codec_reset_n = devices["CODEC_RESET_N"]
    amp_standby_n = devices["AMP_STANDBY_N"]
    amp_unmute      = devices["AMP_MUTE"]

    -- Mute amplifier (not used when LINE_OUT is used)
    amp_unmute:setvalue("0")

    codec_reset_n:setvalue("0")
    codec_reset_n:setvalue("1")

    codec = devices["CODEC"]

    codec:writereg(0, 0x00); -- Page 0

    codec:writereg(3,   0x10); -- Q=2, PLL disabled
    codec:writereg(101, 0x01); -- CODEC_CLKIN uses CLKDIV_OUT
    codec:writereg(102, 0x82); -- CLKDIV uses BCLK
    codec:writereg(7,   0x0a); -- DAC no mix
    codec:writereg(12,  0x50); -- enable highpass on ADC
    codec:writereg(15,  0x09); -- unmute left PGA and set gain to +4.5dB (6dB - 1.5 dB headroom)
    codec:writereg(16,  0x09); -- unmute right PGA and set gain to +4.5dB
    codec:writereg(19,  0x04); -- MIC1L as single ended input to left PGA with 0B and power up ADC
    codec:writereg(21,  0x00); -- MIC1R as single ended input to left PGA with 0dB
    codec:writereg(22,  0x04); -- MIC1R as single ended input to right PGA with 0dB and power up ADC
    codec:writereg(24,  0x00); -- MIC1L as single ended input to right PGA with 0dB
    codec:writereg(37,  0xc0); -- DAC on
    codec:writereg(43,  0x00); -- left DAC mute off
    codec:writereg(44,  0x00); -- right DAC mute off
    codec:writereg(82,  0x80); -- DAC_L1 routed to LEFT_LOP and set volume = 0dB (Lineout)
    -- codec:writereg(82,  0x89); -- DAC_L1 routed to LEFT_LOP and set volume = -4,5dB (Speaker)
    -- codec:writereg(82,  0x90); -- DAC_L1 routed to LEFT_LOP and set volume = -8dB (Speaker)
    -- codec:writereg(82,  0x9c); -- DAC_L1 routed to LEFT_LOP and set volume = -14dB (Lineout)
    codec:writereg(86,  0x09); -- Left Line out power up and gain = 0dB
    codec:writereg(93,  0x09); -- Right Line out power up and gain = 0dB
	
end;
