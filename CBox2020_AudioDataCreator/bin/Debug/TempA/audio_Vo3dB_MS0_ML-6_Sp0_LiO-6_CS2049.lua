-- Version 0.30 neu: set_prompt_gain, set_voice_gain

-- codec stuff

gpio_device("CODEC_RESET_N") {
    pin = "91"
}

i2c_device("CODEC") {
    address = 0x18
}

-- amp stuff

gpio_device("AMP_STANDBY_N") {
    pin = "45"
}

gpio_device("AMP_MUTE") {
    pin = "60"
}

i2c_device("AMP") {
    address = 0x6c
}

--

function init(parameters, devices)
    codec_reset_n = devices["CODEC_RESET_N"]
    amp_standby_n = devices["AMP_STANDBY_N"]
    amp_unmute = devices["AMP_MUTE"]

    -- mute amp
    amp_unmute:setvalue("0")

    -- switch on amp (for diagnostic)
    amp_standby_n:setvalue("1")

    -- hold codec in reset for power saving
    codec_reset_n:setvalue("0")

    amp = devices["AMP"]

    -- switch off amp for power safing
    amp_standby_n:setvalue("1")
end;

function standby(devices)
    codec_reset_n = devices["CODEC_RESET_N"]
    amp_standby_n = devices["AMP_STANDBY_N"]
    amp_unmute      = devices["AMP_MUTE"]

    -- Make sure amp is in standby and muted
    amp_unmute:setvalue("0")
    amp_standby_n:setvalue("0")

    -- hold codec in reset for power safing
    codec_reset_n:setvalue("0")
end;

-- Set volume gain for voice calls
function set_voice_gain(devices)
    codec_reset_n = devices["CODEC_RESET_N"]

    -- TODO: write gain to codec
    codec:writereg(86,  0x39); -- Left  Line out power up and +3dB (Lineout)
    codec:writereg(93,  0x39); -- Right Line out power up and +3dB (Speaker)
end;

-- Set volume gain for prompts
function set_prompt_gain(devices)
    codec_reset_n = devices["CODEC_RESET_N"]

    -- TODO: write gain to codec
    codec:writereg(86,  0x09); -- Left  Line out power up and 0dB (Lineout)
    codec:writereg(93,  0x09); -- Right Line out power up and 0dB (Speaker)
end;

function internal(devices)
    codec_reset_n = devices["CODEC_RESET_N"]
    amp_standby_n = devices["AMP_STANDBY_N"]
    amp_unmute      = devices["AMP_MUTE"]

    -- Mute amplifier first
    amp_unmute:setvalue("0")

    -- Power up amp
    amp_standby_n:setvalue("1")

    codec_reset_n:setvalue("0")
    codec_reset_n:setvalue("1")

    codec = devices["CODEC"]

    codec:writereg(0, 0x00); -- Page 0

    codec:writereg(3,   0x10); -- Q=2, PLL disabled
    codec:writereg(101, 0x01); -- CODEC_CLKIN uses CLKDIV_OUT
    codec:writereg(102, 0x82); -- CLKDIV uses BCLK
    codec:writereg(7,   0x0a); -- DAC no mix
    codec:writereg(12,  0x50); -- enable highpass on ADC
    codec:writereg(15,  0x00); -- unmute left PGA and set gain to +0dB (3dB - 1.5 dB headroom)
    codec:writereg(16,  0x00); -- unmute right PGA and set gain to +0dB
    codec:writereg(19,  0x04); -- MIC1L as single ended input to left PGA with 0B and power up ADC
    codec:writereg(21,  0x00); -- MIC1R as single ended input to left PGA with 0dB
    codec:writereg(22,  0x04); -- MIC1R as single ended input to right PGA with 0dB and power up ADC
    codec:writereg(24,  0x00); -- MIC1L as single ended input to right PGA with 0dB
    codec:writereg(37,  0xc0); -- DAC on
    codec:writereg(43,  0x00); -- left DAC mute off
    codec:writereg(44,  0x00); -- right DAC mute off
    codec:writereg(82,  0x00); -- DAC_L1 not routed to LEFT_LOP/M (Lineout)
    codec:writereg(89,  0x80); -- DAC_L1 routed to RIGHT_LOP and set to 0dB (Speaker)
    codec:writereg(86,  0x09); -- Left Line out power up and 0dB, (Lineout)
    codec:writereg(93,  0x09); -- Right Line out power up and 0dB, (Speaker)

    -- Unmute amp
    amp_unmute:setvalue("1")
end;

function external(devices)
    codec_reset_n = devices["CODEC_RESET_N"]
    amp_standby_n = devices["AMP_STANDBY_N"]
    amp_unmute      = devices["AMP_MUTE"]

    -- Mute amplifier (not used when LINE_OUT is used)
    amp_unmute:setvalue("0")

    codec_reset_n:setvalue("0")
    codec_reset_n:setvalue("1")

    codec = devices["CODEC"]

    codec:writereg(0, 0x00); -- Page 0

    codec:writereg(3,   0x10); -- Q=2, PLL disabled
    codec:writereg(101, 0x01); -- CODEC_CLKIN uses CLKDIV_OUT
    codec:writereg(102, 0x82); -- CLKDIV uses BCLK
    codec:writereg(7,   0x0a); -- DAC no mix
    codec:writereg(12,  0x50); -- enable highpass on ADC
    codec:writereg(15,  0x00); -- unmute left PGA and set gain to +0dB (6dB - 1.5 dB headroom)
    codec:writereg(16,  0x00); -- unmute right PGA and set gain to +0dB
    -- codec:writereg(19,  0x04); -- MIC1L as single ended input to left PGA with 0B and power up ADC
    codec:writereg(19,  0x24); -- MIC1L as single ended input to left PGA with -6dB and power up ADC
    -- codec:writereg(21,  0x00); -- MIC1R as single ended input to left PGA with 0dB
    codec:writereg(21,  0x20); -- MIC1R as single ended input to left PGA with -6dB 
    -- codec:writereg(22,  0x04); -- MIC1R as single ended input to right PGA with 0dB and power up ADC
    codec:writereg(22,  0x24); -- MIC1R as single ended input to right PGA with -6dB and power up ADC
    -- codec:writereg(24,  0x00); -- MIC1L as single ended input to right PGA with 0dB
    codec:writereg(24,  0x20); -- MIC1L as single ended input to right PGA with -6dB
    codec:writereg(37,  0xc0); -- DAC on
    codec:writereg(43,  0x00); -- left DAC mute off
    codec:writereg(44,  0x00); -- right DAC mute off
    -- codec:writereg(82,  0x80); -- DAC_L1 routed to LEFT_LOP/M and set to 0dB (Lineout)
    codec:writereg(82,  0x8c); -- DAC_L1 routed to LEFT_LOP/M and set to -6dB (Lineout)
    codec:writereg(89,  0x00); -- DAC_L1 not routed to RIGHT_LOP/M (Speaker)
    codec:writereg(86,  0x09); -- Left Line out power up and 0dB (Lineout)
    codec:writereg(93,  0x09); -- Right Line out power up and 0dB (Speaker)
end;
