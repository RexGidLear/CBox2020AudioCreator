﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;


namespace CRC_check
{
    class File_CRC32
    {
        long[] pTable = new long[256];

        //Für Standard CRC32:
        //(Wert kann verändert werden)
        long Poly = 0xEDB88320;

        public File_CRC32()
        {
            long CRC;
            int i, j;

            for (i = 0; i < 256; i++)
            {
                CRC = i;

                for (j = 0; j < 8; j++)
                {
                    if ((CRC & 0x1) == 1)
                    {
                        CRC = (CRC >> 1) ^ Poly;
                    }
                    else
                    {
                        CRC = (CRC >> 1);
                    }
                }
                pTable[i] = CRC;
            }
        }

        public uint GetCRC32(string FileName)
        {
            long StreamLength, CRC;
            int BufferSize;
            byte[] Buffer;

            //4KB Buffer
            BufferSize = 0x1000;

            FileStream fs = new FileStream(FileName, FileMode.Open);
            StreamLength = fs.Length;

            CRC = 0xFFFFFFFF;
            while (StreamLength > 0)
            {
                if (StreamLength < BufferSize)
                {
                    BufferSize = (int)StreamLength;
                }
                Buffer = new byte[BufferSize];
                fs.Read(Buffer, 0, BufferSize);

                for (int i = 0; i < BufferSize; i++)
                {
                    CRC = ((CRC & 0xFFFFFF00) / 0x100) & 0xFFFFFF ^ pTable[Buffer[i] ^ CRC & 0xFF];
                }
                StreamLength = StreamLength - BufferSize;
            }
            fs.Close();
            CRC = (-(CRC)) - 1; // !(CRC)
            return (uint)CRC;
        }


        public uint GetCRC16(string FileName)
        {

            FileStream fs = new FileStream(FileName, FileMode.Open);
            byte[] data = new byte[fs.Length];
            fs.Read(data, 0, (int)fs.Length);//chksum.ComputeHash(fs);
            fs.Close();

            ushort Polynom = 0xA001;
            ushort Register = 0xFFFF;
            ushort temp = 0x00;

            // loop through the entire array of bytes
            for (int i = 0; i < data.Length; i++)
            {
                temp = data[i];

                // shift all 8 data bits once
                for (int y = 0; y < 8; y++)
                {
                    if (((Register ^ temp) & 0x01) == 0x01)
                    {
                        Register >>= 1;
                        Register ^= Polynom;
                    }
                    else
                    {
                        Register >>= 1;
                    }
                    temp >>= 1; // shift data 1 bit right, dividing it by 2

                } // end of inner for loop (bit shift loop)
            } // end of outer for loop (data loop)
            long CRC = Register;
            // now we have got our overall 2-byte CRC "Checksum" number
            //return new byte[2] { (byte)(Register % 256), (byte)(Register / 256) };
            // end of CRC16S Method
            return (uint)CRC;
        }
    }

}
