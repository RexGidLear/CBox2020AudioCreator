﻿namespace CBox_Low_Audio_Data_Config
{
    partial class FormInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormInfo));
            this.InfoPictBox1 = new System.Windows.Forms.PictureBox();
            this.InfoPictBox2 = new System.Windows.Forms.PictureBox();
            this.lblInfo1 = new System.Windows.Forms.Label();
            this.lblInfo2 = new System.Windows.Forms.Label();
            this.lblInfo3 = new System.Windows.Forms.Label();
            this.lblInfo4 = new System.Windows.Forms.Label();
            this.lblInfo5 = new System.Windows.Forms.Label();
            this.lblInfo6 = new System.Windows.Forms.Label();
            this.lblInfoL2 = new System.Windows.Forms.Label();
            this.lblInfoL3 = new System.Windows.Forms.Label();
            this.lblInfoL4 = new System.Windows.Forms.Label();
            this.lblInfoL5 = new System.Windows.Forms.Label();
            this.btnOK = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.InfoPictBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.InfoPictBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // InfoPictBox1
            // 
            this.InfoPictBox1.Image = ((System.Drawing.Image)(resources.GetObject("InfoPictBox1.Image")));
            this.InfoPictBox1.Location = new System.Drawing.Point(35, 12);
            this.InfoPictBox1.Name = "InfoPictBox1";
            this.InfoPictBox1.Size = new System.Drawing.Size(352, 74);
            this.InfoPictBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.InfoPictBox1.TabIndex = 0;
            this.InfoPictBox1.TabStop = false;
            this.InfoPictBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // InfoPictBox2
            // 
            this.InfoPictBox2.Image = ((System.Drawing.Image)(resources.GetObject("InfoPictBox2.Image")));
            this.InfoPictBox2.InitialImage = null;
            this.InfoPictBox2.Location = new System.Drawing.Point(283, 143);
            this.InfoPictBox2.Name = "InfoPictBox2";
            this.InfoPictBox2.Size = new System.Drawing.Size(104, 94);
            this.InfoPictBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.InfoPictBox2.TabIndex = 0;
            this.InfoPictBox2.TabStop = false;
            this.InfoPictBox2.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // lblInfo1
            // 
            this.lblInfo1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInfo1.Location = new System.Drawing.Point(35, 107);
            this.lblInfo1.Name = "lblInfo1";
            this.lblInfo1.Size = new System.Drawing.Size(352, 23);
            this.lblInfo1.TabIndex = 1;
            this.lblInfo1.Text = "label1";
            this.lblInfo1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblInfo1.Visible = false;
            // 
            // lblInfo2
            // 
            this.lblInfo2.Location = new System.Drawing.Point(88, 146);
            this.lblInfo2.Name = "lblInfo2";
            this.lblInfo2.Size = new System.Drawing.Size(189, 23);
            this.lblInfo2.TabIndex = 1;
            this.lblInfo2.Text = "label2";
            this.lblInfo2.Visible = false;
            // 
            // lblInfo3
            // 
            this.lblInfo3.Location = new System.Drawing.Point(88, 169);
            this.lblInfo3.Name = "lblInfo3";
            this.lblInfo3.Size = new System.Drawing.Size(189, 23);
            this.lblInfo3.TabIndex = 1;
            this.lblInfo3.Text = "label3";
            this.lblInfo3.Visible = false;
            // 
            // lblInfo4
            // 
            this.lblInfo4.Location = new System.Drawing.Point(88, 192);
            this.lblInfo4.Name = "lblInfo4";
            this.lblInfo4.Size = new System.Drawing.Size(189, 23);
            this.lblInfo4.TabIndex = 1;
            this.lblInfo4.Text = "label4";
            this.lblInfo4.Visible = false;
            // 
            // lblInfo5
            // 
            this.lblInfo5.Location = new System.Drawing.Point(88, 215);
            this.lblInfo5.Name = "lblInfo5";
            this.lblInfo5.Size = new System.Drawing.Size(189, 70);
            this.lblInfo5.TabIndex = 1;
            this.lblInfo5.Text = "label5";
            this.lblInfo5.Visible = false;
            // 
            // lblInfo6
            // 
            this.lblInfo6.Location = new System.Drawing.Point(32, 298);
            this.lblInfo6.Name = "lblInfo6";
            this.lblInfo6.Size = new System.Drawing.Size(235, 23);
            this.lblInfo6.TabIndex = 1;
            this.lblInfo6.Text = "label6";
            this.lblInfo6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblInfo6.Visible = false;
            // 
            // lblInfoL2
            // 
            this.lblInfoL2.Location = new System.Drawing.Point(32, 146);
            this.lblInfoL2.Name = "lblInfoL2";
            this.lblInfoL2.Size = new System.Drawing.Size(49, 23);
            this.lblInfoL2.TabIndex = 1;
            this.lblInfoL2.Text = "label1";
            this.lblInfoL2.Visible = false;
            // 
            // lblInfoL3
            // 
            this.lblInfoL3.Location = new System.Drawing.Point(32, 169);
            this.lblInfoL3.Name = "lblInfoL3";
            this.lblInfoL3.Size = new System.Drawing.Size(49, 23);
            this.lblInfoL3.TabIndex = 1;
            this.lblInfoL3.Text = "label1";
            this.lblInfoL3.Visible = false;
            // 
            // lblInfoL4
            // 
            this.lblInfoL4.Location = new System.Drawing.Point(32, 192);
            this.lblInfoL4.Name = "lblInfoL4";
            this.lblInfoL4.Size = new System.Drawing.Size(49, 23);
            this.lblInfoL4.TabIndex = 1;
            this.lblInfoL4.Text = "label1";
            this.lblInfoL4.Visible = false;
            // 
            // lblInfoL5
            // 
            this.lblInfoL5.Location = new System.Drawing.Point(32, 215);
            this.lblInfoL5.Name = "lblInfoL5";
            this.lblInfoL5.Size = new System.Drawing.Size(49, 70);
            this.lblInfoL5.TabIndex = 1;
            this.lblInfoL5.Text = "label1";
            this.lblInfoL5.Visible = false;
            this.lblInfoL5.Click += new System.EventHandler(this.label5_Click);
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(300, 298);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(86, 25);
            this.btnOK.TabIndex = 2;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // FormInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(430, 340);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.lblInfo4);
            this.Controls.Add(this.lblInfo6);
            this.Controls.Add(this.lblInfo5);
            this.Controls.Add(this.lblInfo3);
            this.Controls.Add(this.lblInfo2);
            this.Controls.Add(this.lblInfoL5);
            this.Controls.Add(this.lblInfoL4);
            this.Controls.Add(this.lblInfoL3);
            this.Controls.Add(this.lblInfoL2);
            this.Controls.Add(this.lblInfo1);
            this.Controls.Add(this.InfoPictBox2);
            this.Controls.Add(this.InfoPictBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormInfo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Info";
            ((System.ComponentModel.ISupportInitialize)(this.InfoPictBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.InfoPictBox2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox InfoPictBox1;
        private System.Windows.Forms.PictureBox InfoPictBox2;
        private System.Windows.Forms.Label lblInfo1;
        private System.Windows.Forms.Label lblInfo2;
        private System.Windows.Forms.Label lblInfo3;
        private System.Windows.Forms.Label lblInfo4;
        private System.Windows.Forms.Label lblInfo5;
        private System.Windows.Forms.Label lblInfo6;
        private System.Windows.Forms.Label lblInfoL2;
        private System.Windows.Forms.Label lblInfoL3;
        private System.Windows.Forms.Label lblInfoL4;
        private System.Windows.Forms.Label lblInfoL5;
        private System.Windows.Forms.Button btnOK;
    }
}