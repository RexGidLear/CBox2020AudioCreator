﻿using System;
using System.IO;
using System.Windows.Forms;
using RegawMOD.Android;
using Public_Const;
using System.Collections.Generic;

namespace CBoxADBcontroll
{
    public partial class FormCBoxConfig : Form
    {
        //ADB Thread variables
        int adbDEVcount = 0;   //Anzahl der gefundenen ADB Devices
        List<string> adbDeviceNumbers = new List<string>();  //Liste mit gefundenen Devise-Nummern
        bool adbRUN = true;  // definiert, ob das ADB Interface genutzt werden soll
        bool adbAutoCheck = true;  // automatik mode aktiv (standard)
        bool vADBreconnect = false; //Status reconnect aktiv

 
        // allgemeine Variablen
        string search_OutConfigParam = "eCall_Audio_Sink_MEC";
        string search_OutConfig_Spker = "Speaker";
        string search_OutConfig_Lineout = "Lineout";
        
        
        public FormCBoxConfig()
        {
            InitializeComponent();
            this.ControlBox = false;
        }

        AndroidController android;
        Device device;

        private void Form1_Load(object sender, EventArgs e)
        {
            thread_ADB_Control();
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            adbRUN = false;
            //ALWAYS remember to call this when you're done with AndroidController.  It removes used resources
            android.Dispose(); 
        }

        private void btnPullFile_Click_1(object sender, EventArgs e)
        {
            if (adbDEVcount == 0)
            {
                setStatusText("Error! No ADB-Device", System.Drawing.Color.Blue);
            }
            else
            {
                this.liBo_PhnoeNums.Items.Clear();
                string sourceOnDevice = "/data/ecall/comm_params.json";
                string destinationDir = System.IO.Path.Combine(Application.StartupPath, "Temp", "temp.txt");

                if (pullFile(sourceOnDevice, destinationDir))
                {
                    string[] telNumLines = System.IO.File.ReadAllLines(@destinationDir);
                    foreach (string line in telNumLines)
                    {
                        this.liBo_PhnoeNums.Items.Add(line);
                    }
                }
            }
        }

        private bool pullFile(string sourceOnDevice, string destinationDir)
        {
            bool k = false;
            if (adbDEVcount == 0)
            {
                setStatusText("Error! No ADB-Device", System.Drawing.Color.Blue);
            }
            else
            {
                setStatusText("Please wait", System.Drawing.Color.Red);
                try
                {
                    k = device.PullFile(sourceOnDevice, destinationDir, -1);
                }
                catch
                {
                    k = false;
                    meassNoADBdevice("");
                }
                if (k)
                {
                    setStatusText("OK", System.Drawing.Color.Green);
                }
                else
                {
                    setStatusText("Error!", System.Drawing.Color.Red);
                }
            }
            return (k);
        }

        private void pushfileToNAD(string sourcePath, string destinationPath)
        {
            bool k = false;
            if (adbDEVcount == 0)
            {
                setStatusText("Error! No ADB-Device", System.Drawing.Color.Blue);
            }
            else
            {
                try
                {
                    //AndroidController android = AndroidController.Instance;
                    k = device.PushFile(sourcePath, destinationPath, -1);
                }
                catch
                {
                    meassNoADBdevice("");
                }
                if (k) { setStatusText("OK", System.Drawing.Color.Green); } else { setStatusText("Error", System.Drawing.Color.Red); };
            }
        }

        private void btnWriteTelNum_Click(object sender, EventArgs e)
        {
            if (adbDEVcount == 0)
            {
                setStatusText("Error! No ADB-Device", System.Drawing.Color.Blue);
            }
            else
            {
                string DestinationOnDevice = "/data/ecall/comm_params.json";
                string TelNumFile = System.IO.Path.Combine(Application.StartupPath, "Temp", "temp.txt");

                setStatusText("Please wait!", System.Drawing.Color.Green);

                string json_TelNum = create_TelNumFile(teBo_NewTelNum.Text);
                if (json_TelNum == "")
                {
                    return;
                }

                try
                {
                    using (var writer = new System.IO.StreamWriter(TelNumFile))
                    {
                        writer.Write(json_TelNum);
                    }
                    pushfileToNAD(TelNumFile, DestinationOnDevice);
                }
                catch (Exception ex)
                {
                    setStatusText("Error while create file!", System.Drawing.Color.Red);
                    MessageBox.Show(ex.Message + ", Error while create file");
                }
            }
        }

        string create_TelNumFile(string NewTelNumber)
        {
            string json_formated = "";
            if (NewTelNumber == "")
            {
                MessageBox.Show("No Phone-Number selected!");
                return(json_formated);
            }

            else
            {
                string NL = Environment.NewLine;
                //string NL = ;
                    json_formated = "{" + NL;
                    json_formated = json_formated + "    " + '"' + "eCall_Dataset_Comm_Name" + '"' + ": " + '"' + "AUMLB0COMECE" + '"' + "," + NL;
                    json_formated = json_formated + "    " + '"' + "eCall_MEC_Number_TSP" + '"' + ": " + '"' + NewTelNumber + '"' + "," + NL;
                    json_formated = json_formated + "    " + '"' + "eCall_MEC_Number_ECC" + '"' + ": " + '"' + NewTelNumber + '"' + "," + NL;
                    json_formated = json_formated + "    " + '"' + "eCall_ACN_Number_TSP" + '"' + ": " + '"' + NewTelNumber + '"' + "," + NL;
                    json_formated = json_formated + "    " + '"' + "eCall_ACN_Number_ECC" + '"' + ": " + '"' + NewTelNumber + '"' + "," + NL;
                    json_formated = json_formated + "    " + '"' + "eCall_MSD_SMS_Number" + '"' + ": " + '"' + NewTelNumber + '"' + "," + NL;
                    json_formated = json_formated + "    " + '"' + "eCall_Test_Number" + '"' + ": " + '"' + "" + '"' + NL;
                    json_formated = json_formated + "}" + NL;
            }
            return(json_formated);
        }

        private void btnWriteOrgTelNum_Click(object sender, EventArgs e)
        {
            if (adbDEVcount == 0)
            {
                setStatusText("Error! No ADB-Device", System.Drawing.Color.Blue);
            }
            else
            {
                string DestinationOnDevice = "/data/ecall/comm_params.json";
                string TelNumFile = System.IO.Path.Combine(Application.StartupPath, "Temp", "temp.txt");

                setStatusText("Please wait!", System.Drawing.Color.Green);

                string json_TelNum = create_OrgTelNumFile();
                if (json_TelNum == "")
                {
                    return;
                }

                try
                {
                    using (var writer = new System.IO.StreamWriter(TelNumFile))
                    {
                        writer.Write(json_TelNum);
                    }
                    pushfileToNAD(TelNumFile, DestinationOnDevice);
                }
                catch (Exception ex)
                {
                    setStatusText("Error while create file!", System.Drawing.Color.Red);
                    MessageBox.Show(ex.Message + ", Error while create file");
                }
            }

        }

        string create_OrgTelNumFile()
        {
            string json_formated = "";
            {
                string NL = Environment.NewLine;
                //string NL = ;
                json_formated = "{" + NL;
                json_formated = json_formated + "    " + '"' + "eCall_Dataset_Comm_Name" + '"' + ": " + '"' + "AUMLB0COMECE" + '"' + "," + NL;
                json_formated = json_formated + "    " + '"' + "eCall_MEC_Number_TSP" + '"' + ": " + '"' + "+4984113300663" + '"' + "," + NL;
                json_formated = json_formated + "    " + '"' + "eCall_MEC_Number_ECC" + '"' + ": " + '"' + "+4953170222603" + '"' + "," + NL;
                json_formated = json_formated + "    " + '"' + "eCall_ACN_Number_TSP" + '"' + ": " + '"' + "+4984113300663" + '"' + "," + NL;
                json_formated = json_formated + "    " + '"' + "eCall_ACN_Number_ECC" + '"' + ": " + '"' + "+4953170222603" + '"' + "," + NL;
                json_formated = json_formated + "    " + '"' + "eCall_MSD_SMS_Number" + '"' + ": " + '"' + "310003202" + '"' + "," + NL;
                json_formated = json_formated + "    " + '"' + "eCall_Test_Number" + '"' + ": " + '"' + "" + '"' + NL;
                json_formated = json_formated + "}" + NL;
            }
            return (json_formated);
        }

        private void btn_NAD_Reboot_Click(object sender, EventArgs e)
        {
            if (adbDEVcount == 0)
            {
                setStatusText("Error! No ADB-Device", System.Drawing.Color.Blue);
            }
            else
            {
                if (getStatusText() != "Reboot is active!")
                {
                    setStatusText("Reboot is active!", System.Drawing.Color.Red);
                    bgw_Thread_Reboot.RunWorkerAsync();
                }
                cbADBautoCon.Checked = false;
            }
        }

        private void bgw_Thread_Reboot_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            try
            {
                MessageBox.Show("Is the SOS-Call disconnected?", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                AndroidController android = AndroidController.Instance;
                device.Reboot();
                System.Threading.Thread.Sleep(1000);

                AndroidController.Instance.WaitForDevice();

            }
            catch { }
        }

        private void bgw_Thread_Reboot_ProgressChanged(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {
            string text = getStatusText() + e.ProgressPercentage;
            setStatusText(text, System.Drawing.Color.Green);
        }

        private void bgw_Thread_Reboot_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            setStatusText("Device running", System.Drawing.Color.Green);
        }

        private void btnADBreconnect_Click(object sender, EventArgs e)
        {
            setStatusText("Please wait, reconnect ADB-Interface!", System.Drawing.Color.Red);
            bgw_Thread_Reconnect.RunWorkerAsync();
            //adbIFreopen();  //ohne bgw_hread
        }

        private void adbIFreopen()
        {
            //schließt die aktuelle Instanz vom ADB-Interface und öffnet es neu
            setStatusText("Please wait, reconnect ADB-Interface!", System.Drawing.Color.Red);
            android.Dispose();
            System.Threading.Thread.Sleep(100);
            vADBreconnect = true;

            android = AndroidController.Instance;

            // AdbCommand adbCmd = Adb.FormAdbCommand(device, "reconnect");
            // Adb.ExecuteAdbCommandNoReturn(adbCmd);
        }
        private void bgw_Thread_Reconnect_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            try
            {
                AndroidController android = AndroidController.Instance;
                android.Dispose();
                System.Threading.Thread.Sleep(100);
                vADBreconnect = true;

                android = AndroidController.Instance;

                AndroidController.Instance.WaitForDevice();
            }
            catch { }
        }

        private void bgw_Thread_Reconnect_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            setStatusText("Device running", System.Drawing.Color.Green);
        }


        private void setStatusText( string text, System.Drawing.Color color)
        {
                            lblAppStatus.ForeColor = color;
                            lblAppStatus.Text = text;
        }

        string getStatusText()
        {
            return (lblAppStatus.Text);
        }

        private void btn_readOutConf_Click(object sender, EventArgs e)
        {
            if (adbDEVcount == 0)
            {
                setStatusText("Error! No ADB-Device", System.Drawing.Color.Blue);
            }
            else
            {
                string sourceOnDevice = "/data/ecall/general_params.json";
                string destinationDir = System.IO.Path.Combine(Application.StartupPath, "Temp", "temp.txt");
                string thelp;

                if (pullFile(sourceOnDevice, destinationDir))
                {
                    string[] textLines = System.IO.File.ReadAllLines(@destinationDir);
                    foreach (string line in textLines)
                    {
                        thelp = line;
                        if (thelp.IndexOf(search_OutConfigParam, 0) > 0)
                        {
                            if (thelp.IndexOf(search_OutConfig_Spker, 0) > 0)
                            { raBu_eCallSpk.Checked = true; }
                            if (thelp.IndexOf(search_OutConfig_Lineout, 0) > 0)
                            { raBu_LineOut.Checked = true; }
                        }
                    }
                }
            }
        }

        private void btn_writeOutConf_Click(object sender, EventArgs e)
        {
            //this.liBo_PhnoeNums.Items.Clear();
            string sourceOnDevice = "/data/ecall/general_params.json";
            string destinationDir = System.IO.Path.Combine(Application.StartupPath, "Temp", "temp.txt");
            string paramsFile = System.IO.Path.Combine(Application.StartupPath, "Temp", "temp2.txt");
            string thelp;
            string tresult = "";
            string NL = Environment.NewLine;

            if (pullFile(sourceOnDevice, destinationDir))
            {
                if (adbDEVcount == 0)
                {
                    setStatusText("Error! No ADB-Device", System.Drawing.Color.Blue);
                }
                else
                {
                    try
                    {
                        string[] textLines = System.IO.File.ReadAllLines(@destinationDir);
                        foreach (string line in textLines)
                        {
                            thelp = line;
                            if (thelp.IndexOf(search_OutConfigParam, 0) > 0)
                            {
                                if (raBu_eCallSpk.Checked == true)
                                { thelp = thelp.Replace(search_OutConfig_Lineout, search_OutConfig_Spker); }
                                if (raBu_LineOut.Checked == true)
                                { thelp = thelp.Replace(search_OutConfig_Spker, search_OutConfig_Lineout); }
                            }
                            //this.liBo_PhnoeNums.Items.Add(line);
                            tresult = tresult + thelp + NL;
                        }
                        try
                        {
                            using (var writer = new System.IO.StreamWriter(paramsFile))
                            {
                                writer.Write(tresult);
                            }
                        }
                        catch (Exception ex)
                        {
                            setStatusText("Error while create file!", System.Drawing.Color.Red);
                            MessageBox.Show(ex.Message + ", Error while create file");
                        }
                        pushfileToNAD(paramsFile, sourceOnDevice);
                    }
                    catch (Exception ex)
                    {
                        setStatusText("Error while read/write file!", System.Drawing.Color.Red);
                        MessageBox.Show(ex.Message + ", Error while read/write file");
                    }
                }
            }
        }


        private void btn_Close_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void FormCBoxConfig_Activated(object sender, EventArgs e)
        {
            lblPackDownFile.Text = PublicConst.package_name + ".tar.gz";
            lblPackDownDate.Text = (System.IO.File.GetLastWriteTime(System.IO.Path.Combine(PublicConst.Project_Dir, PublicConst.Package_subdir, PublicConst.package_name + ".tar.gz"))).ToString();
        }

        private void btnPackDownload_Click(object sender, EventArgs e)
        {
            if (adbDEVcount == 0)
            {
                setStatusText("Error! No ADB-Device", System.Drawing.Color.Blue);
            }
            else
            {
                setStatusText("Please wait", System.Drawing.Color.Blue);

                try
                {
                    //Files auf NAD löschen
                    //bool k = device
                    //AdbCommand adbCmd = Adb.FormAdbCommand("devices");
                    //AdbCommand adbCmd = Adb.FormAdbCommand(device, "pull", "/system/app", @"C:\");

                    AdbCommand adbCmd = Adb.FormAdbCommand(device, "shell", "rm", PublicConst.dirNADaudioFiles + "*.xml*");
                    Adb.ExecuteAdbCommandNoReturn(adbCmd);
                    adbCmd = Adb.FormAdbCommand(device, "shell", "rm", PublicConst.dirNADaudioFiles + "*.lua*");
                    Adb.ExecuteAdbCommandNoReturn(adbCmd);
                    adbCmd = Adb.FormAdbCommand(device, "shell", "rm", PublicConst.dirNADaudioFiles + "*.tar.gz*");
                    Adb.ExecuteAdbCommandNoReturn(adbCmd);
                    adbCmd = Adb.FormAdbCommand(device, "shell", "rm", PublicConst.dirNADaudioFiles + "*.json*");
                    Adb.ExecuteAdbCommandNoReturn(adbCmd);
                    adbCmd = Adb.FormAdbCommand(device, "shell", "rm", PublicConst.dirNADaudioFiles + "*.txt*");
                    Adb.ExecuteAdbCommandNoReturn(adbCmd);

                    System.Threading.Thread.Sleep(100);


                    string[] filenames = new string[10] { "", "", "", "", "", "", "", "", "", "" };

                    filenames[0] = System.IO.Path.Combine(PublicConst.Project_Dir, PublicConst.Package_subdir, PublicConst.package_name + ".tar.gz");  //Package
                    filenames[1] = System.IO.Path.Combine(PublicConst.Project_Dir, PublicConst.Lua_fileName);  //lua_File
                    filenames[2] = System.IO.Path.Combine(PublicConst.Project_Dir, PublicConst.EQ_xml_intern_NewName);    //EQ eCall Speaker
                    filenames[3] = System.IO.Path.Combine(PublicConst.Project_Dir, PublicConst.EQ_xml_extern_NewName);   //EQ-Lineout 
                    filenames[4] = System.IO.Path.Combine(PublicConst.Project_Dir, PublicConst.Project_File);    //Audio config file
                    filenames[5] = System.IO.Path.Combine(PublicConst.Project_Dir, PublicConst.Project_textFile);    //Audio config text file

                    int k = 6; // Anzahl der Files in filenames[]
                    for (int i = 0; i < k; i++)
                    {
                        pushfileToNAD(filenames[i], PublicConst.dirNADaudioFiles);
                    }
                }
                catch
                {
                    meassNoADBdevice("");
                }
            }
        }

        private void btnPullAudioDir_Click(object sender, EventArgs e)
        {
            if (adbDEVcount == 0)
            {
                setStatusText("Error! No ADB-Device", System.Drawing.Color.Blue);
            }
            else
            {
                this.liBo_PhnoeNums.Items.Clear();
                string sourceOnDevice = PublicConst.dirNADaudioFiles;
                string destinationDir = System.IO.Path.Combine(Application.StartupPath, "TempA");

                try
                {
                    if (System.IO.Directory.Exists(destinationDir)) { System.IO.Directory.Delete(destinationDir, true); } //Zielverzeichnis löschen

                    if (pull_DirNAD(sourceOnDevice, destinationDir))
                    {
                        //if (System.IO.Directory.GetFiles(destinationDir).Length < 1)
                        if (System.IO.Directory.Exists(destinationDir))
                        {
                            //var files = from file in System.IO.Directory.EnumerateFiles(destinationDir, "*.*", SearchOption.AllDirectories)
                            foreach (var fn in System.IO.Directory.GetFiles(@destinationDir, "*.*", System.IO.SearchOption.TopDirectoryOnly))
                            {
                                this.liBo_PhnoeNums.Items.Add(System.IO.Path.GetFileName(fn));
                                //Console.WriteLine(fn);
                            }
                        }
                    }
                }
                catch
                {
                    meassNoADBdevice("");
                }
            }
        }

        private void meassNoADBdevice(string message)
        {
            MessageBox.Show(message + "Command not possible. No ADB-Device?");
        }

        private bool pull_DirNAD(string sourceDir, string destDir)
        {
            bool k = false;
            if (adbDEVcount == 0)
            {
                setStatusText("Error! No ADB-Device", System.Drawing.Color.Blue);
            }
            else
            {

                setStatusText("Please wait", System.Drawing.Color.Blue);

                try
                {
                    k = device.PullDirectory(sourceDir, destDir, -1);

                }
                catch
                {
                    k = false;
                    meassNoADBdevice("");
                }
                if (k)
                {
                    setStatusText("OK", System.Drawing.Color.Green);
                }
                else
                {
                    setStatusText("Error!", System.Drawing.Color.Red);
                }
            }
            return (k);
        }

       
        
        //-------------------------------------------------------------------------------------------
        // ADB connect handling
        
        private void thread_ADB_Control()
        {
            bgw_AndoidUpdateDevice.RunWorkerAsync();
        }

        private void bgw_AndoidUpdateDevice_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            try
            {
                int foundDevices = 0;
                // erstellt eine gültige Instanz von Androidcontroller
                android = AndroidController.Instance;
                
                 while (adbRUN == true)
                {
                    if (adbAutoCheck)
                    {
                        // prüfen der aktuell verbundenen Devices
                        foundDevices = updateADB_IF();
                        adbDEVcount = foundDevices;  // aktualisieren des globalen Zustandes adbDEVcount
                        bgw_AndoidUpdateDevice.ReportProgress(foundDevices); // Starten des Progress ereignisses mit anzahl devs als Progress
                    }
                    System.Threading.Thread.Sleep(100);
                 }
                android.Dispose();  // entfernt das ADB objekt
            }
            catch 
            {
                android.Dispose();  // entfernt das ADB objekt
            }

        }

        private int updateADB_IF()
        {
            string serial;
            int k = 0; //Anzahl der ADB Devices
            adbDeviceNumbers.Clear(); // löscht die Device liste

            try
            {

                //Always call UpdateDeviceList() before using AndroidController on devices to get the most updated list
                android.UpdateDeviceList();

                if (android.HasConnectedDevices)
                {
                    k = android.ConnectedDevices.Count;
                    // alle gefundenen Devices anzeigen
                    //for (int i = 0; i < k; i++)
                    //{
                    //    serial = android.ConnectedDevices[i];
                    //    device = android.GetConnectedDevice(serial);
                    //    adbDeviceNumbers.Add(device.SerialNumber);
                    //}
                    adbDEVcount = k;
                    //Legt erstes Device fest
                    serial = android.ConnectedDevices[0];
                    device = android.GetConnectedDevice(serial);
                    adbDeviceNumbers.Add(device.SerialNumber);
                }
            }
            catch
            {
                k = 0; // im Fehlerfall
            }
            return (k);
        }

        private void bgw_AndoidUpdateDevice_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {

        }

        private void bgw_AndoidUpdateDevice_ProgressChanged(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {
            int Devices = e.ProgressPercentage;
            lblADBstatus.Text = Devices.ToString();
            if ( Devices ==0)
            {
                if (lblADBstatus.BackColor != System.Drawing.Color.LightSalmon)
                { lblADBstatus.BackColor = System.Drawing.Color.LightSalmon; }
                else { lblADBstatus.BackColor = System.Drawing.Color.Salmon; }
                setADBstatusText("No Device found!");
            }
            if (Devices > 0)
            {
                if (lblADBstatus.BackColor != System.Drawing.Color.LightGreen)
                { lblADBstatus.BackColor = System.Drawing.Color.LightGreen; }
                else { lblADBstatus.BackColor = System.Drawing.Color.Aquamarine; }
                setADBstatusText("Device found!");
            }
            showADBdeviceNumber();
            if (vADBreconnect)
            {
                setStatusText("Reconnect done!", System.Drawing.Color.Green);
                vADBreconnect = false;
            }
        }

        private void tim_checkADBconnection_Tick(object sender, EventArgs e)
        {
            updateADB_IF();
        }

        private void setADBstatusText(string stat)
        {
            lblADBstatus.Text = stat;
        }
        
        private void showADBdeviceNumber()
        {
            int k = adbDeviceNumbers.Count;
            liBoNADserNo.Items.Clear();

            for (int i = 0; i < k; i++)
            {
                liBoNADserNo.Items.Add(adbDeviceNumbers[i]);
            }
        }

        private void cbADBautoCon_CheckedChanged(object sender, EventArgs e)
        {
            adbAutoCheck = cbADBautoCon.Checked;
        }

        private void btnPhoneBook_Click(object sender, EventArgs e)
        {
            string destx =Path.Combine(Application.StartupPath, PublicConst.PhoneBook_name);
            if (System.IO.File.Exists(destx) == false)
            {
                File.WriteAllText(destx, "");
                //FileStream Create(destx); }
            }
            string Programmname = "notepad.exe";
            string Parameter = destx;
            System.Diagnostics.Process.Start(Programmname, Parameter);
        }

        // Kopiert die markierte(n) Zeile(n) einer Listbox in die Zwischenablage.
        public void kopieren_listbox(ListBox listbox)
        {
            if (listbox.SelectedItem != null)
            {
                string str = "";
                for (int i = 0; i < listbox.SelectedItems.Count; i++)
                {
                    str += listbox.SelectedItems[i].ToString() + "\n";
                }
                Clipboard.SetDataObject(str);
            }
        }

        private void btnCopyList_Click(object sender, EventArgs e)
        {
            kopieren_listbox(liBo_PhnoeNums);
        }

    }
}
