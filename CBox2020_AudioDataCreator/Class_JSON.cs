﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Public_Const;
using System.Runtime.Serialization;
using System.Web.Script.Serialization;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;


namespace JSON_Interface
{
    public class JsonMurks
    {
        public string IN1 { set; get; }
        public string IN2 { set; get; }
    }

    public class JsonData
    {
        //Definition aller Keys
        public string eCall_Disarm_Trigger { set; get; }
        public string eCall_Standard { set; get; }
        //public List<JsonMurks> Murks { set; get; }
    }

    //"Class JsonKeysCBox" in PubkicConst erstellt !!!
    class Class_JSON
    {

        public int read_jsonConfig(string DPath)
        {
            int vret = 1;
            //Test
            if (System.IO.File.Exists(DPath))
            {
                string jsonText = System.IO.File.ReadAllText(DPath);

                JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
                //JsonData result = jsSerializer.Deserialize<JsonData>(jsonText);
                JsonKeysCBox result = jsSerializer.Deserialize<JsonKeysCBox>(jsonText);

//                var obj = new JsonKeysCBox
                //PublicConst.package_name = result.package_name;
                PublicConst.Car_type = result.car_type; //;JsonValueCBox.json_val_car_type,
                PublicConst.Car_version = result.car_version;
                PublicConst.Car_revision = Convert.ToInt32(result.car_revision);
                PublicConst.Car_AmpList = Convert.ToInt32(result.car_AmpList);
                PublicConst.Tuning_version_int = result.tuning_version_int;
                PublicConst.Tuning_version_ext = result.tuning_version_ext;
                PublicConst.Date_create = result.created;
                PublicConst.Author_int = result.author_intern;
                PublicConst.Author_ext = result.author_extern;
                PublicConst.Tuning_comment_ext = result.comment_extern;
                PublicConst.Tuning_comment_int = result.comment_intern;
                PublicConst.Lua_fileName = result.lua_file;
                PublicConst.Lua_CRC = result.lua_crc;
                PublicConst.EQ_xml_intern_NewName = result.audio_EQ_int;
                PublicConst.EQ_xml_intern_Hash = result.audio_EQ_int_crc;
                PublicConst.EQ_xml_extern_NewName = result.audio_EQ_ext;
                PublicConst.EQ_xml_extern_Hash = result.audio_EQ_ext_crc;
                PublicConst.NAD_CMIC = result.NAD_CMIC.ToString();
                PublicConst.NAD_CLVL = result.NAD_CLVL.ToString();
                PublicConst.NAD_CMICext = result.NAD_CMICext.ToString();
                PublicConst.NAD_CLVLext = result.NAD_CLVLext.ToString();
                PublicConst.NAD_ByPassExtActive = result.NAD_ByPass.ToString();
                PublicConst.CBoxType = result.CBoxType.ToString();

                vret = 0;
            }

            return vret;

        }

        public int write_jsonConfig(string DPath)
        {
            int vret = 0;
             var obj = new JsonKeysCBox
            {
                car_DataSetName = PublicConst.Car_Datensatzname,
                car_version = PublicConst.Car_version,
                package_name = PublicConst.package_name,
                car_type = PublicConst.Car_type, //;JsonValueCBox.json_val_car_type,
                car_revision = PublicConst.Car_revision.ToString(),
                car_AmpList = PublicConst.Car_AmpList.ToString(),
                tuning_version_int = PublicConst.Tuning_version_int,
                tuning_version_ext = PublicConst.Tuning_version_ext,
                created = PublicConst.Date_create,
                author_intern = PublicConst.Author_int,
                author_extern = PublicConst.Author_ext,
                comment_extern = PublicConst.Tuning_comment_ext,
                comment_intern = PublicConst.Tuning_comment_int,
                lua_file = PublicConst.Lua_fileName,
                lua_crc = PublicConst.Lua_CRC,
                audio_EQ_int = PublicConst.EQ_xml_intern_NewName,
                audio_EQ_int_crc = PublicConst.EQ_xml_intern_Hash,
                audio_EQ_ext = PublicConst.EQ_xml_extern_NewName,
                audio_EQ_ext_crc = PublicConst.EQ_xml_extern_Hash,
                NAD_CMIC = Convert.ToInt32(PublicConst.NAD_CMIC),
                NAD_CLVL = Convert.ToInt32(PublicConst.NAD_CLVL),
                NAD_CMICext = Convert.ToInt32(PublicConst.NAD_CMICext),
                NAD_CLVLext = Convert.ToInt32(PublicConst.NAD_CLVLext),
                NAD_ByPass = Convert.ToInt32(PublicConst.NAD_ByPassExtActive),
                CBoxType = Convert.ToInt32(PublicConst.CBoxType)
             };
            
            // write JSON file
            var json_wdata = new JavaScriptSerializer().Serialize(obj);

            string jsonw1 = "";
            jsonw1 = json_wdata;
            string json_formated = "";
            string NL=Environment.NewLine;
            int i=0;
            int atribute = 0;
            for (i=0; i<jsonw1.Length; i++) 
            {
                if (jsonw1[i] == '"') { atribute = atribute + 1;}
                if (atribute == 2) { atribute = 0; }
                if (atribute == 0)
                {
                    if (jsonw1[i].ToString() == "}") { json_formated = json_formated + NL; }
                }
                json_formated = json_formated + jsonw1[i];
                if (atribute == 0)
                {
                    if (jsonw1[i].ToString() == "{") { json_formated = json_formated + NL + "  "; }
                    if (jsonw1[i].ToString() == ",") { json_formated = json_formated + NL + "  "; }
                    if (jsonw1[i].ToString() == ":") { json_formated = json_formated + " "; }
                }
            }

            using (var writer = new System.IO.StreamWriter(DPath))
            {
                writer.Write(json_formated);
            }

            // write content file
            jsonw1 = json_formated;
            json_formated = "";
            atribute = 0;
            for (i = 0; i < jsonw1.Length-3; i++)
            {
                if (jsonw1[i] == '"') { atribute = atribute + 1; }
                if (atribute == 2) { atribute = 0; }
                if (atribute == 0)
                {
                    if ((jsonw1[i].ToString() == ":") | (jsonw1[i].ToString() == ","))
                    {
                        json_formated = json_formated + ";";
                    }
                    else
                    {
                        if ((jsonw1[i] != '"') & (jsonw1[i] != ',') & (jsonw1[i] != '{') & (jsonw1[i] != '}'))
                        {
                            json_formated = json_formated + jsonw1[i];
                        }
                    }
                }
                else
                {
                    if ((jsonw1[i] != '"'))
                    {
                        json_formated = json_formated + jsonw1[i];
                    }
                }
            }
            json_formated = json_formated + ";";
            json_formated = json_formated + NL;
            json_formated = "Content of package:;" +json_formated;

            string thelp = Path.GetDirectoryName(DPath);
            thelp = Path.Combine(thelp, PublicConst.package_name + ".txt");
            PublicConst.Project_textFile = thelp;

            using (var writer = new System.IO.StreamWriter(thelp))
            {
                writer.Write(json_formated);
            }

            return vret;
        }

    }

}
